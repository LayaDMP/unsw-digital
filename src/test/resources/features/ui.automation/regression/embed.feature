@Embed
Feature: Embed Component
  @Tweet
  Scenario Template: Tracy adds Tweet Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Tweet option
    And she publishes the page
    And she navigates to the published site
    Then she should see the tweet in the published page
    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @TweetTimeline
  Scenario Template: Tracy adds Twitter Timeline Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Twitter Timeline option
    And she publishes the page
    And she navigates to the published site
    Then she should see the twitter timeline in the published page
    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @Flickr
  Scenario Template: Tracy adds Flickr Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Flickr option
    And she publishes the page
    And she navigates to the published site
    Then she should see the flickr image in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @FeeCalculator
  Scenario Template: Tracy adds Fee Calculator Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Fee Calculator option
    And she publishes the page
    And she navigates to the published site
    Then she should see the Fee Calculator in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @Whooshkaa
  Scenario Template: Tracy adds Whooshkaa Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Whooshkaa option
    And she publishes the page
    And she navigates to the published site
    Then she should see the Whooshkaa in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @FindAnAgent @iFrame
  Scenario Template: Tracy adds Find an Agent/iFrame Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with iFrame option
    And she publishes the page
    And she navigates to the published site
    Then she should see the iFrame in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @Qualtrics
  Scenario Template: Tracy adds Qualtrics Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Qualtrics option
    And she publishes the page
    And she navigates to the published site
    Then she should see the Qualtrics in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @EventBrite
  Scenario Template: Tracy adds Eventbrite Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Eventbrite option
    And she publishes the page
    And she navigates to the published site
    Then she should see the Eventbrite in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

  @Soundcloud
  Scenario Template: Tracy adds Soundcloud Option in Embed Component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Embed" component
    * she adds a "Embed (v2)" component in "<columnLayout>" layout
    And she configures the "Embed (v2)" component with Soundcloud option
    And she publishes the page
    And she navigates to the published site
    Then she should see the Soundcloud in the published page

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |