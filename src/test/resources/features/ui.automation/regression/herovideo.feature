@HeroVideo @Story-1340
Feature: Hero video component

  @DFDR-1480 @rerun
  Scenario: Tracy adds Hero Video component in Standard Page Template
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she creates a page with template "standardPage" for "Hero Video" component
    When she adds a "Hero Video" component in "STANDARDFULLWIDTHCOLUMN" layout
    And she configures hero video component with default setting
    And she publishes the page
    And she navigates to the published site
    Then she validates the Title,subtitle,author name,published date and youtube video has been published
