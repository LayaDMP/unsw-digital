@HeroGeneralComponent
Feature: Hero General Component

  Scenario Template: Tracy adds Hero General component - Standard page, with illustration option
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    When she creates a page with template "<templateType>" for "Hero General" component
    And she adds a "Hero General" component in "<column_layout>" layout
    And she configures text and content with date "<dateEnabled>" and description "<desc>"
    And she configures hero general background with "<background>"
    And she publishes the page
    And she navigates to the published site
    Then she validates all configured fields for hero general are displayed

    Scenarios:
      | templateType | column_layout           | background   | dateEnabled | desc |
      | standardPage | STANDARDFULLWIDTHCOLUMN | image        | no          | no   |
      | standardPage | STANDARDFULLWIDTHCOLUMN | illustration | no          | no   |
      | standardPage | STANDARDFULLWIDTHCOLUMN | default      | no          | no   |
      | oneColumn    | ONECOLUMNFULLWIDTH      | default      | no          | no   |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | default      | no          | no   |
