@Story_HorizontalScroll-1166 @HorizontalScrollComponent
Feature:Horizontal scroll component

  @DFDR-686
  Scenario Template: Tracy adds 4 General cards in horizontal scroll component - Standard page template
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Content Fragment" component
    And she adds a "Content Fragment" component in "ONECOLUMNFULLWIDTH" layout
    And she publishes the page
    And she navigates back to Test Automation folder
    And she creates a page with template "<templateType>" for "Horizontal scroll" component
    And she adds a "Horizontal scroll" component in "<column_layout>" layout
    When she configures horizontal scroll component with "<column_layout>" "<NumberOfComponents>" "<HorizontalScrollComponent>" "<ContentFragmentPath>"
    And she publishes the page
    And she navigates to the published site
    And she should see the next slider arrow and slider dotted on the published site

    Scenarios:
      | templateType | NumberOfComponents | HorizontalScrollComponent | column_layout           | ContentFragmentPath |
      | standardPage | 4                  | General Cards (v2)        | STANDARDFULLWIDTHCOLUMN | generalcardtemplate |

  @DFDR-1275 @DFDR-1276 @rerun
  Scenario Template: Tracy adds 4 Image components in horizontal scroll component - Standard page template
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Horizontal scroll" component
    And she adds a "Horizontal scroll" component in "<column_layout>" layout
    When she configures horizontal scroll component with "<column_layout>" "<NumberOfComponents>" "<Component>" "<ContentFragmentPath>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same number of "<Component>" on the published site <NumberOfComponents>
    And she should see the next slider arrow and slider dotted on the published site

    Scenarios:
      | templateType | NumberOfComponents | Component  | column_layout           | ContentFragmentPath |
      | standardPage | 4                  | Image (v2) | STANDARDFULLWIDTHCOLUMN | generalcardtemplate |
      | standardPage | 4                  | Text       | STANDARDFULLWIDTHCOLUMN | generalcardtemplate |

