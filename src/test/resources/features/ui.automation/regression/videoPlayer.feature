@ParentStory_Video-176 @Story_Video-177 @VideoComponent
Feature: Video Component

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages

  @DFDR-322 @DFDR-280 @DFDR-281 @DFDR-323  @DFDR-1467
  Scenario Template: Tracy wants to save a Video in Portrait mode - Single column template and column layout
    Given she creates a page with template "<templateType>" for "Video Player" component
    And she adds a "Video Player" component in "<column_layout>" layout
    When she crops the video in Portrait mode "<CropMode>" "<videoLink>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the video on the published site "<videoLink>"

    Scenarios:
      | templateType | videoLink           | CropMode | column_layout      |
      | oneColumn    | watch?v=V7vjxhqMPng | portrait | ONECOLUMNFULLWIDTH |
      | oneColumn    | watch?v=V7vjxhqMPng | portrait | ONECOLUMNLEFT     |
      | twoColumn    | watch?v=V7vjxhqMPng | portrait | TWOCOLUMNFULLWIDTH     |
      | twoColumn    | watch?v=V7vjxhqMPng | portrait | TWOCOLUMNLEFT     |
      | standardPage | watch?v=V7vjxhqMPng | portrait | STANDARDFULLWIDTHCOLUMN  |

