Feature: List component
  @List
  Scenario Template: Tracy adds text from h2 to h6,paragraph or quote size for left, centre and right alignment - Two column template (paragraph,right)
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "List" component
    And she adds a "List" component in "<columnLayout>" layout
    And she configures the "List" component <no of items>
    And she publishes the page
    And she navigates to the published site
    Then she should see the same text on the published page "text"
    Scenarios:
      | templateType | no of items          | columnLayout            |
      | standardPage | 3 | STANDARDFULLWIDTHCOLUMN |
