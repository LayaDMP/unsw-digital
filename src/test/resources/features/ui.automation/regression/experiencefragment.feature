## new feature
## Tags: optional
#
#Feature: Create Experience Fragment
#
#  Scenario Template: Tracy wants to add a new Experience Fragment
#    Given Tracy logs in to the AEM site
#    And she navigates to the Test Automation folder in Experience Fragment
#    And she creates an Experience Fragment "<templateType>" "<templateTitle>" "<templateName>" "<templateDescription>"
#
#    Scenarios:
#      | templateType               | templateTitle                   | templateName                    | templateDescription                                           |
#      | common experience fragment | ExperienceFragment_GlobalHeader0 | experiencefragment_globalheader0 | create experience fragment with 3 sections and 3 sub sections |