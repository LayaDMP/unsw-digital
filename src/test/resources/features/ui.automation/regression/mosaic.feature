@Mosaic
Feature: Mosaic component

  @DRDR-869,@DFDR-441 @demo
  Scenario Template: Tracy adds Mosaic component - Standard page, with transcript
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she creates a page with template "<standardPage>" for "<keyword>" component
    When she adds a "<keyword>" component in "<column_layout>" layout
    And she configures the Mosaic component with default configuration
    And she publishes the page
    And she navigates to the published site
    Then she validates the mosaic details displayed in published page
    Scenarios:
      | column_layout      | keyword | standardPage |
      | ONECOLUMNFULLWIDTH | Mosaic  | standardPage |
