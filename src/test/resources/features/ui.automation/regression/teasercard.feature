@TeaserCard

  @GeneralTeaserCard
Feature: TeaserCard Component

  Scenario Template: Tracy adds General Teaser Component in Standard Full width Page
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Teaser Card" component
    * she adds a "Teaser (Card) v3" component in "<columnLayout>" layout
    And she configures the "Teaser (Card) v3" component with its properties
    And she publishes the page
    And she navigates to the published site
    Then she validates the teaser card with its properties

    Scenarios:
      | template type | columnLayout |
      | standardPage  | STANDARDFULLWIDTHCOLUMN |

@VideoTeaserCard
Scenario Template: Tracy adds Video Teaser Component in Standard Full width Page
  Given Tracy logs in to the AEM site
  And she navigates to the Test Automation
  * she deletes all the previous Pages
  * she creates a page with template "<template type>" for "Teaser Card" component
  * she adds a "Teaser (Card) v3" component in "<columnLayout>" layout
  And she configures the "Teaser (Card) v3" component with its Video properties
  And she publishes the page
  And she navigates to the published site
  Then she validates the teaser card with its Video properties

  Scenarios:
    | template type | columnLayout |
    | standardPage  | STANDARDFULLWIDTHCOLUMN |

@ProfileTeaserCard
Scenario Template: Tracy adds Profile Teaser Component in Standard Full width Page
  Given Tracy logs in to the AEM site
  And she navigates to the Test Automation
  * she deletes all the previous Pages
  * she creates a page with template "<template type>" for "Teaser Card" component
  * she adds a "Teaser (Card) v3" component in "<columnLayout>" layout
  And she configures the "Teaser (Card) v3" component with its Profile properties
  And she publishes the page
  And she navigates to the published site
  Then she validates the teaser card with its Profile properties

  Scenarios:
    | template type | columnLayout |
    | standardPage  | STANDARDFULLWIDTHCOLUMN |