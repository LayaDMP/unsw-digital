@sidebarCTA
Feature: sidebar CTA
  Scenario Template: Tracy adds a sidebarCTA component
    Given Tracy logs in to the AEM site
    And she deletes all the previous Pages
    When she navigates to the Test Automation
    And she creates a page with template "<templateType>" for "SidedBarCTA" component
    And she adds a "Sidebar CTA (v2)" component in "<column_layout>" layout
    And she configures the "Sidebar CTA (v2)" component with "<title>" "<subtitle>" "<buttonLabel>" "<linkURL>"
    And she publishes the page
    And she navigates to the published site
    Then she validates the sidebar CTA button with "<title>" "<subtitle>" "<buttonLabel>" "<linkURL>"

    Scenarios:
    |templateType|column_layout|title|subtitle|buttonLabel|linkURL|
    |standardPage|STANDARDRIGHTCOLUMN|SideBarCTATitle|SideBarSubTitle|SideCTA|https://www.unsw.edu.au/study|

