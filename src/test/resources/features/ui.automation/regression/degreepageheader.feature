@DegreePageHeader @Story_PageHeader-1398 @ParentStory_PageHeader-1073
@DFDR-1399  @DFDR-1401 @DFDR-1403 @DFDR-1402 @DFDR-1396 @DFDR-1397 @DFDR-1404 @DFDR-1405

Feature: Add Degree Page Header component

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages

  Scenario Template: Tracy adds Degree Page Header component - Image Component, Shape, Standard template

    Given she creates a page with template "<templateType>" for "Page Header" component
    And she adds a "Page Header" component in "<column_layout>" layout
    When she configures the Degree page header component "<Type>" "<Background_shape>" "<Asset>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Degree page header component on the published page "<Background_shape>"
    And she should see the header logo added "<Header_logo>"

      Scenarios:
      | templateType | Type            | column_layout           | Background_shape | Asset | Header_logo |
      | standardPage | Manual Settings | STANDARDFULLWIDTHCOLUMN | Shape            | Image | unique2     |
      | oneColumn    | Manual Settings | STANDARDFULLWIDTHCOLUMN | Shape            | Image | unique2     |


  @DFDR-1403
  Scenario Template: Tracy adds Degree Page Header component - Video Component, Standard template

    Given she creates a page with template "<templateType>" for "Page Header" component
    And she adds a "Page Header" component in "<column_layout>" layout
    And she configures the Degree page header component "<Type>" "<Background_shape>" "<Asset>"
    When she crops the video in Portrait mode "<CropMode>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Degree page header component on the published page "<Background_shape>"
    And she should see the default video on the published site

    Scenarios:
      | templateType | Type            | column_layout           | Background_shape | Asset | CropMode |
      | standardPage | Manual Settings | STANDARDFULLWIDTHCOLUMN | Shape            | Video | portrait |
      | oneColumn    | Manual Settings | STANDARDFULLWIDTHCOLUMN | Shape            | Video | portrait |

  @DFDR-1396
  Scenario Template: Tracy adds Degree Page Header component - Degree Page template

    Given she creates a page with template "<templateType>" for "Page Header" component
    And she adds a "Page Header" component in "<column_layout>" layout
    And she configures the Degree page header component "<Type>" "<Background_shape>" "<Asset>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Degree page header component on the published page "<Background_shape>"
    Scenarios:
      | templateType | Type            | column_layout           | Background_shape | Asset |
      | oneColumn    | Manual Settings | STANDARDFULLWIDTHCOLUMN | None             | None  |
     #| degreePage   | Manual Settings | STANDARDFULLWIDTHCOLUMN | None             | None  |


  @DFDR-1404 @degreeIssue
  Scenario Template: Tracy adds Degree Page Header component - Standard Page template, dropdown filter

    Given she creates a page with template "<templateType>" for "Page Header" component
    And she adds a "Page Header" component in "<column_layout>" layout
    When she configures the Degree page header component "<Type>" "<Background_shape>" "<Asset>"
    And she configures degree dropdown filter with default values
    And she publishes the page
    And she navigates to the published site
    Then she should see the Degree page header component on the published page "<Background_shape>"
    And she should also see the options under dropdown filter component
    Scenarios:
      | templateType | Type            | column_layout           | Background_shape | Asset |
      | standardPage | Manual Settings | STANDARDFULLWIDTHCOLUMN | None             | None  |

  @DFDR-1405 @degreeIssue
  Scenario Template: Tracy adds Degree Page Header component - OneColumn Page template, dropdown filter

    Given she creates a page with template "<templateType>" for "Page Header" component
    And she adds a "Page Header" component in "<column_layout>" layout
    When she configures the Degree page header component "<Type>" "<Background_shape>" "<Asset>"
    And she configures degree dropdown filter with default values
    And she also adds "Text" component in "<column_layout>" layout
    And she configures the "Text" component with paragraph "<Heading_level>" "<Heading_alignment>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Degree page header component on the published page "<Background_shape>"
    And she should also see the options under dropdown filter component

    Scenarios:
      | templateType | Type            | column_layout           | Background_shape | Asset  | Heading_level | Heading_alignment |
      | oneColumn    | Manual Settings | STANDARDFULLWIDTHCOLUMN | None             | None   | paragraph     | right             |
