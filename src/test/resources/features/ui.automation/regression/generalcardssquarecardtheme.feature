@ParentStory_GeneralCard-258 @Story_GeneralCard-149 @GeneralCardscomponent
Feature:General Cards component

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages

  @DFDR-430 @DAEMCC-387 @DAEMCC-385 @DAEMCC-393  @DFDR-429 @DAEMCC-386 @DAEMCC-384 @DAEMCC-388
  Scenario Template: Tracy adds a general card with square card theme - Two column template

    Given she creates a page with template "<templateType>" for "Content Fragment" component
    And she adds a "Content Fragment" component in "<column_layout>" layout
    When she configures the content fragment with "<ContentFragmentPath>" "<DisplayMode>"
    And she publishes the page
    And she navigates back to Test Automation folder
    And she creates a page with template "<templateType>" for "General" component
    And she adds a "General" component in "<column_layout>" layout
    And she adds a square card theme to the general card in "<column_layout>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same general card in square shaped alignment on the published site
    Scenarios:
      | templateType | column_layout      | ContentFragmentPath | DisplayMode |
      | twoColumn    | TWOCOLUMNFULLWIDTH | generalcardtemplate | Multiple    |
      | oneColumn    | ONECOLUMNFULLWIDTH | generalcardtemplate | Multiple    |


  @DFDR-300
  Scenario Template: Tracy adds a general card with Manual settings, 1 card and external link One column template

    Given she creates a page with template "<templateType>" for "General" component
    And she adds a "General" component in "<column_layout>" layout
    When she configures the card with manual settings with "<Link>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the general card on the published page
    And she should be navigated to the linked page/site on clicking the general card "<Link>"
    Scenarios:
      | templateType | Link     | column_layout |
      | oneColumn    | External | ONECOLUMNFULLWIDTH |

  @DFDR-308
  Scenario Template: Tracy adds a general card with Manual settings, 1 card and internal link One column template

    Given she creates a page with template "<templateType>" for "Content Fragment" component
    When she adds a "Content Fragment" component in "<column_layout>" layout
    And she publishes the page
    * she navigates back to Test Automation folder
    * she creates a page with template "<templateType>" for "General" component
    * she adds a "General" component in "<column_layout>" layout
    * she configures the card with manual settings with "<Link>"
    * she publishes the page
    * she navigates to the published site
    Then she should see the general card on the published page
    And she clicks on the general card
    And she should be navigated to the published page "<Link>"
    Scenarios:
      | templateType | Link     | column_layout      |
      | twoColumn    | External | TWOCOLUMNFULLWIDTH |

  @DFDR-309
  Scenario Template: Tracy adds a general card and verifies the mandatory fields in manual settings
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "General" component
    And she adds a "General" component in "<column_layout>" layout
    When she tries to save the General Card without entering any field values
    Then she should get an alert for all the mandatory fields

    Scenarios:
     | templateType | column_layout |
      | oneColumn    | ONECOLUMNFULLWIDTH |
