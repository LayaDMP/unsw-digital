@Story_CTAButton-159 @ParentStory_CTAButton-158
@CTAComponent
Feature: CTA Component

  @DFDR-1406 @DFDR-556 @DFDR-557 @DFDR-557 @DFDR-558 @DFDR-559 @DFDR-560
  Scenario Template: Tracy adds a CTA Button component - standard column template
    Given Tracy logs in to the AEM site
    And she deletes all the previous Pages
    When she navigates to the Test Automation
    And she creates a page with template "<templateType>" for "CTA" component
    And she adds a "CTA" component in "<column_layout>" layout
    And she publishes CTA button with "<Alignment>" "<Link>" "<Label style>" "<Button color>" "<Button style>" "<Button size>"
    And she publishes the page
    And she navigates to the published site
    Then she validates CTA button with "<Alignment>" "<Link>" "<Label style>" "<Button color>" "<Button style>" "<Button size>"
    Scenarios:
      | templateType | Alignment | Link     | column_layout           | Label style               | Button color | Button style | Button size |
      | standardPage | left      | Internal | STANDARDFULLWIDTHCOLUMN | Married with icon on left | dark         | fill border  | xlarge      |
      | oneColumn    | left      | Internal | ONECOLUMNLEFT           | Text Only                 | tertiary     | none         | xlarge      |
      | oneColumn    | left      | Internal | ONECOLUMNLEFT           | Text Only                 | secondary    | none         | xlarge      |
      | oneColumn    | center    | Internal | ONECOLUMNFULLWIDTH      | Married with icon on left | primary      | fill border  | large       |
      | twoColumn    | right     | External | TWOCOLUMNLEFT           | Divorced                  | light        | fill         | medium      |
      | twoColumn    | left      | External | TWOCOLUMNFULLWIDTH      | Married                   | dark         | border       | small       |