@Story-1325 @VideoStandFirstComponent
Feature: Video Standfirst Player component

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages

  @DAEMCC-389 @DAEMCC-720 @DAEMCC-390  @DAEMCC-721
  Scenario Template: Tracy adds Video standfirst component - Standard page, with transcript
    Given she creates a page with template "<templateType>" for "Video player Standfirst" component
    And she adds a "Video player Standfirst" component in "<column_layout>" layout
    When she configures the video player standfirst component with "<ShowTranscript>"
    And she publishes the page
    And she navigates to the published site
    Then she should validate the video player standfirst component with "<ShowTranscript>"

    Scenarios:
      | templateType | column_layout           | ShowTranscript |
      | standardPage | STANDARDFULLWIDTHCOLUMN | Yes            |
      | standardPage | STANDARDFULLWIDTHCOLUMN | No             |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | Yes            |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | No             |
