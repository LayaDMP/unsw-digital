@Story_KeyDates-192 @ParentStory_KeyDates-191 @Regression @KeyDatesComponent
Feature:Key Dates component

  @DFDR-613 @DFDR-612 @DFDR-611
  Scenario Template: Tracy adds a Key Dates component - two column template full width, external link and ReadMoreTarget = New tab
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Key" component
    And she adds a "Key" component in "<column_layout>" layout
    And she configures 4 set of parameters to key dates with "<ReadMoreTarget>"
    And she publishes the page
    And she navigates to the published site
    Then she validates all the configured fields are displayed
    Scenarios:
      | templateType | column_layout           | ReadMoreTarget |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | New Tab        |
      | oneColumn    | ONECOLUMNFULLWIDTH      | Same Tab       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | Same Tab       |
