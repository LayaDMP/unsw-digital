Feature: Tab component
  @Tab
  Scenario Template: Tracy adds tab component in Standard template
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    When she creates a page with template "<templateType>" for "Tabs" component
    And she adds a "tabs" component in "<columnLayout>" layout
    And she configures the tab component with below components
    |Text|
    |List|
    |Quote|
    And she configures the "Text" component "UniqueText_p_right" "paragraph" "right"
    And she configures the "List" component 2
    And she configures the "Quote" component with default image "All is well" "Tester" "Test Manager"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same text on the published page "UniqueText_p_right" "paragraph" "right"
    And she navigates to "List" tab
    Then she should see the same text on the published page "text"
    And she navigates to "Quote" tab
    Then she should see the same image with "All is well" "Tester" "Test Manager"

    Scenarios:
       | templateType | columnLayout       |
       | standardPage | STANDARDLEFTCOLUMN |