@SiteAnnouncement
Feature: Site Announcement

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages


  Scenario Template: Tracy adds a Short Page Announcement component - Two column template

    Given she creates a page with template "<templateType>" for "Page Announcement" component
    And she adds a "Page Announcement" component in "<columnLayout>" layout
    When she configures the short page announcement component with
      | type | <type>      |
      | tab  | <tab>       |
      | icon | <closeIcon> |
      | link | <link>      |
    And she publishes the page
    And she navigates to the published site
    Then she validates published short component are working as configured

    Scenarios:
      | templateType | columnLayout            | type  | tab      | closeIcon | link     |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | short | new tab  | Enabled   | External |
      | twoColumn    | TWOCOLUMNFULLWIDTH      | short | same tab | disabled  | Internal |
      | standardPage | STANDARDFULLWIDTHCOLUMN | short | new tab  | Enabled   | External |

  Scenario Template: Tracy adds a long Page Announcement component - Two column template

    Given she creates a page with template "<templateType>" for "Page Announcement" component
    And she adds a "Page Announcement" component in "<columnLayout>" layout
    When she configures the long page announcement component with
      | type | <type>      |
      | icon | <closeIcon> |
      | link | <link>      |
      | tab  | <tab>       |
    And she configures the page announcement buttons with below attributes
      | button colour | button style | button link | button icon | label style | button size |
      | dark          | fill border  | <link>      | Phone       | Married     | small       |
      | light         | fill         | <link>      | Envelope    | Divorced    | medium      |

    And she publishes the page
    And she navigates to the published site
    Then she validates published long component as configured

    Scenarios:
      | templateType | columnLayout       | type | closeIcon | link     | tab     |
      | twoColumn    | TWOCOLUMNFULLWIDTH | long | Enabled   | External | new tab |