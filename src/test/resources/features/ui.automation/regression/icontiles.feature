@Story_IconTiles-1236 @IconTilesComponent
Feature:Icon Tiles component

  @DFDR-844  @DFDR-848 @DFDR-851 @rerun
  Scenario Template: Tracy adds 3 tiles within an Icon Tile component - Standard page template, full width column, No background
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Icon tile" component
    And she adds a "Icon tile" component in "<column_layout>" layout
    And she configures the Icon Tile with default values and "<TitleStyle>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Icon tile configured with "<TitleStyle>" on published page

    Scenarios:
      | templateType | TitleStyle        | column_layout           |
      | standardPage | No Background     | STANDARDFULLWIDTHCOLUMN |
      | standardPage | Shadow Background | STANDARDLEFTCOLUMN      |
      | standardPage | White Background  | STANDARDLEFTCOLUMN      |

