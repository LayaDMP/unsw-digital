@HeroStandardComponent_Story_1169 @HeroStandard
Feature: Hero standard component

  @DFDR-1476 @DFDR-856
  Scenario Template: Tracy adds Hero standard component - Standard page, CTA button left width, style lawbuilding
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Hero Standard" component
    And she adds a "Hero Standard" component in "<column_layout>" layout
    And she configures the hero standard component with "Fixed" type
    And she chooses the the style for the component "<style>" "Hero Standard"
    And she publishes CTA button with "<Alignment>" "<Link>" "<Label style>" "<Button color>" "<Button style>" "<Button size>"
    And she publishes the page
    And she navigates to the published site
    Then she validates CTA button with "<Alignment>" "<Link>" "<Label style>" "<Button color>" "<Button style>" "<Button size>"
    And she should see the hero standard component as configured
    Scenarios:
      | templateType | column_layout           | style               | Alignment | Link     | Label style | Button color | Button style | Button size |
      | standardPage | STANDARDFULLWIDTHCOLUMN | lawbuilding         | left      | External | Married     | dark         | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | roundhouse          | right     | External | Married     | dark         | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | sirf                | center    | External | Married     | dark         | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | morvenbrown         | left      | External | Married     | secondary    | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | tyree               | left      | External | Married     | tertiary     | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | sculpture           | left      | External | Married     | dark         | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | chemicalscience     | left      | External | Married     | dark         | border       | small       |
      | standardPage | STANDARDFULLWIDTHCOLUMN | roundhouse 90degree | left      | External | Married     | dark         | border       | small       |