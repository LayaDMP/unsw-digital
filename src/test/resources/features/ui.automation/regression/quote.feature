@Quote
Feature: Quote Component

  Scenario Template: Tracy adds Quote Component - Standard Template, Fullwidth layout
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Quote" component
    * she adds a "Quote" component in "<columnLayout>" layout
    And she configures the "Quote" component with default image "<quoteText>" "<quoteAuthorName>" "<quotePositionTitle>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same image with "<quoteText>" "<quoteAuthorName>" "<quotePositionTitle>"

    Scenarios:
      | template type | columnLayout            | quoteText   | quoteAuthorName | quotePositionTitle |
      | standardPage  | STANDARDFULLWIDTHCOLUMN | All is well | Tester  | Test Manager       |