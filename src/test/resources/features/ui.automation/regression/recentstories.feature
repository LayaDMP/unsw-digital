@Story-1168  @DFDR-Test @RecentStoriescomponent
Feature: Recent Stories component

  @DFDR-1360 @Rerun
  Scenario Template: Tracy adds Recent stories component - Manual Selection
    Given Tracy logs in to the AEM site
    When she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Recent Stories" component
    And she adds a "Recent Stories" component in "<column_layout>" layout
    And she configures the "Recent Stories" component with "<Selection>" "<JsonFeed>"
    And she publishes the page
    And she navigates to the published site
    Then she should validate the recent story component with "<Selection>" "<JsonFeed>"
    Scenarios:
      | templateType | Selection        | JsonFeed      | column_layout      |
      | standardPage | Manual Selection | Alumini feed  | ONECOLUMNFULLWIDTH |
      | standardPage | Default          | Newsroom feed | ONECOLUMNFULLWIDTH |
      | standardPage | Default          | Alumni feed   | ONECOLUMNFULLWIDTH |
