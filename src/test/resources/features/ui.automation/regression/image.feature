# have sep page for Sites, Assets - maybe name it home navigation
#  alt text test case is added, need to automate that
@Regression @stage @Story_Image-138 @ParentStory_Image-130 @CropImageComponent
Feature: Image Component

  Background:
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages

  @DFDR-310 @DFDR-1439 @image
  Scenario Template: Tracy wants to crop Image in Portrait mode - Single column template and column layout
    Given she creates a page with template "<templateType>" for "Image" component
    And she adds a "Image (v2)" component in "<column_layout>" layout
    And she configures image component with default image and "<Decorative>" "<Link>"
    When she crops the image in Portrait mode "<CropMode>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the Image in the Image Component "<imageName>"
    And she should see the Metadata on the published page "<Decorative>" "<Link>"
    Scenarios:
      | templateType | imageName | CropMode | Decorative | Link          | column_layout           |
      | oneColumn    | unique2   | Portrait | No         | External      | ONECOLUMNFULLWIDTH      |
      | oneColumn    | unique2   | Portrait | Yes        | Dex Reference | ONECOLUMNLEFT           |
      | twoColumn    | unique2   | Portrait | Yes        | Dex Reference | TWOCOLUMNLEFT           |
      | twoColumn    | unique2   | Portrait | No         | External      | TWOCOLUMNFULLWIDTH      |
   #   | degreePage   | unique2   | Portrait | Yes        | Dex Reference | DEGREEFULLWIDTH         |
 #   | degreePage   | unique2   | Portrait | No         | Dex Reference | DEGREEFULLWIDTH         |
      | standardPage | unique2   | Portrait | Yes        | Dex Reference | STANDARDFULLWIDTHCOLUMN |
      | standardPage | unique2   | Portrait | No         | Dex Reference | STANDARDFULLWIDTHCOLUMN |
