@testimonial
Ability: To validate Authoring and publishing Testimonial component

  @Sanity
  Scenario Template: Tracy adds Testimonial component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "<Testimonial>" component
    And she adds a "<Testimonial>" component in "<column_layout>" layout
    And she configures the "<Testimonial>" testimonial component "<image>" image
    And she publishes the page
    And she navigates to the published site
    Then she validates the "<Testimonial>" testimonial "<image>" image configured values
    @DFDR-1432 @longtestimonial
    Scenarios:
      | Testimonial      | templateType | column_layout           | image |
      | long testimonial | standardPage | STANDARDFULLWIDTHCOLUMN | with  |
    @DFDR-1495  @longtestimonial
    Scenarios:
      | Testimonial      | templateType | column_layout           | image   |
      | long testimonial | standardPage | STANDARDFULLWIDTHCOLUMN | without |
    @DFDR-1429 @DFDR-1430 @shorttestimonial
    Scenarios:
      | Testimonial       | templateType | column_layout       | image |
      | short testimonial | standardPage | STANDARDLEFTCOLUMN  | with  |
      | short testimonial | standardPage | STANDARDRIGHTCOLUMN | with  |
    @DFDR-1495 @DFDR-887 @shorttestimonial
    Scenarios:
      | Testimonial       | templateType | column_layout      | image   |
      | short testimonial | standardPage | STANDARDLEFTCOLUMN | without |

