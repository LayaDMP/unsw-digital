@ParentStory_Text-140 @Story_Text-143 @Text
Feature: Text component

  @DFDR-363 @DAEMCC-352
  Scenario Template: Tracy adds text from h2 to h6,paragraph or quote size for left, centre and right alignment - Two column template (paragraph,right)
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Text" component
    And she adds a "Text" component in "<columnLayout>" layout
    And she configures the "Text" component "<HeadingText>" "<HeadingLevel>" "<HeadingAlignment>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same text on the published page "<HeadingText>" "<HeadingLevel>" "<HeadingAlignment>"
    Scenarios:
      | templateType | HeadingText          | HeadingLevel | HeadingAlignment | columnLayout       |
      | twoColumn    | UniqueText_p_right   | paragraph    | right            | TWOCOLUMNLEFT      |
      | twoColumn    | UniqueText_q_center  | quote        | center           | TWOCOLUMNFULLWIDTH |
      | oneColumn    | UniqueText_h5_left   | h5           | left             | ONECOLUMNFULLWIDTH |
      | oneColumn    | UniqueText_h6_center | h6           | center           | ONECOLUMNLEFT      |
