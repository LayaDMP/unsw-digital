Feature: Standout
  @Standout
  Scenario Template: Tracy adds Standout Component - Standard Template, Fullwidth layout
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Standout" component
    * she adds a "Standout (v2)" component in "<columnLayout>" layout
    And she configures the "Standout (v2)" component with "<standoutText>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the standout component with "<standoutText>"

    Scenarios:
      | template type | columnLayout            | standoutText     |
      | standardPage  | STANDARDFULLWIDTHCOLUMN | Standout Testing |

