Feature: Card Minimal Divider
  @CardMinimalDivider
  Scenario Template: Tracy adds Card Minimal Divider component
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    * she deletes all the previous Pages
    * she creates a page with template "<template type>" for "Card Minimal Divider" component
    * she adds a "Card Minimal Divider (v2)" component in "<columnLayout>" layout
    And she configures the "Card Minimal Divider (v2)" component with "<heading>" "<desc>" "<url>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the card minimal divider component with "<heading>" "<desc>" "<url>"
    Scenarios:
      | template type | columnLayout            | heading              | desc         | url |
      | standardPage  | STANDARDFULLWIDTHCOLUMN | Card Minimal Divider | Card Testing |https://www.library.unsw.edu.au|
