@Story_Heading-146 @ParentStory_Heading-145 @HeadingComponent
Feature: Heading Component

  @DFDR-357 @DAEMCC-628  @DFDR-359  @DFDR-356 @DAEMCC-627 @DFDR-358
  Scenario Template: Tracy adds headings from h2 to h5 size for left, centre and right alignment - Two column template (h3,right)
    Given Tracy logs in to the AEM site
    And she navigates to the Test Automation
    And she deletes all the previous Pages
    And she creates a page with template "<templateType>" for "Heading" component
    And she adds a "Heading" component in "<column_layout>" layout
    And she configures the Heading component "<Heading_text>" "<Heading_level>" "<Heading_alignment>"
    And she publishes the page
    And she navigates to the published site
    Then she should see the same heading on the published page "<Heading_text>" "<Heading_alignment>" "<Heading_level>"
    Scenarios:
      | templateType | Heading_text      | Heading_level | Heading_alignment | column_layout      |
      | twoColumn    | Unique1_h4_right  | h4            | right             | TWOCOLUMNFULLWIDTH |
      | twoColumn    | Unique1_h5_center | h5            | center            | TWOCOLUMNLEFT      |
      | oneColumn    | Unique1_h2_left   | h2            | left              | ONECOLUMNFULLWIDTH |
      | oneColumn    | Unique1_h3_right  | h3            | right             | ONECOLUMNLEFT      |
