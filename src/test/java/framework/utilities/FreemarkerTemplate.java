package framework.utilities;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerTemplate {

    private Configuration configuration = null;

    private Configuration getConfiguration() {
        if (configuration == null) {
            configuration = new Configuration(Configuration.VERSION_2_3_29);
            configuration.setDefaultEncoding("UTF-8");
            configuration.setClassForTemplateLoading(FreemarkerTemplate.class, "/");
            configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        }
        return configuration;
    }

        Template getTemplate(String templateFile){
        try{
            return getConfiguration().getTemplate(templateFile);
        }catch(Exception e){
            throw new IllegalStateException("Unable to find template "+ templateFile,e);
                }
        }
}
