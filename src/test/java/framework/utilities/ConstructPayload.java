package framework.utilities;

import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

public class ConstructPayload {

    private final String templateFile;

    private static final FreemarkerTemplate freemarkerTemplate=new FreemarkerTemplate();


    public ConstructPayload(String templateFile) {
        this.templateFile = templateFile;
    }

    public static ConstructPayload usingTemplate(String template){
        return new ConstructPayload(template);
    }
    public String withFieldsFrom(Map<String,String> fieldValues){
        Template template=freemarkerTemplate.getTemplate(templateFile);

        Writer writer= new StringWriter();

        try {
            template.process(fieldValues,writer);
        }catch (TemplateException | IOException exception){
            throw new IllegalStateException("Failed to merge test data template");
        }
        return writer.toString();
    }


}
