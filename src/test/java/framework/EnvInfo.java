package framework;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvInfo {

    static final Logger LOGGER = LoggerFactory.getLogger(EnvInfo.class);
    static EnvironmentVariables environmentVariables= SystemEnvironmentVariables.createEnvironmentVariables();

    public static EnvironmentSpecificConfiguration getEnvConfig(){
        return  EnvironmentSpecificConfiguration.from(environmentVariables);
    }
}
