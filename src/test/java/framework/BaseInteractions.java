package framework;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.util.EnvironmentVariables;
import org.assertj.core.api.SoftAssertions;
import org.awaitility.core.ConditionTimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static org.awaitility.Awaitility.await;


public class BaseInteractions extends UIInteractionSteps {

    protected static WebDriver driver;
    public BaseInteractions() {
        driver=getDriver();
    }

    protected static final Logger LOGGER= LoggerFactory.getLogger(BaseInteractions.class);
    private EnvironmentVariables environmentVariables;

    public void dragAndDrop(WebElement fromElement,WebElement toElement){
        Actions actions=new Actions(getDriver());
        waitforPageTodisplayWith(fromElement);
        waitforPageTodisplayWith(toElement);
        actions.dragAndDrop(fromElement,toElement)
                .build().perform();
    }

    public void waitforPageTodisplayWith(WebElement element){
        int defaultWait= Integer.parseInt(environmentVariables.getProperty("wait.pageload"));
        wait(element,defaultWait);
    }
    public void waitForPageToLoadWith(WebElementFacade element){
        int defaultWait= Integer.parseInt(environmentVariables.getProperty("wait.pageload"));

        await(String.format("Waiting for '%s' to be loaded",nameOf(element)))
                .atMost(defaultWait, TimeUnit.SECONDS)
                .pollDelay(1,TimeUnit.MILLISECONDS)
                .pollInterval(300,TimeUnit.MILLISECONDS)
                .ignoreExceptions()
                .until(()-> isElementPresent.test(element));
        LOGGER.debug("Page Loaded with element - " +nameOf(element));

    }

    public void waitForJSPageLoadComplete(){
        evaluateJavascript("return document.readyState").toString().equals("complete");
    }

    public boolean optionalWaitForElement(WebElement element,int time){
                try{
                    wait(element,time);
                    return true;
                }catch (ConditionTimeoutException e){
                    return false;
                }
    }
    public boolean isOptionalElementDisplayed(WebElementFacade element){
        int time= 5;
        try{
            wait(element,time);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    public boolean optionalDefaultWaitFor(WebElement element){
        int defaultWait= Integer.parseInt(environmentVariables.getProperty("wait.pageload"));
        try{
            wait(element,defaultWait);
            return true;
        }catch (ConditionTimeoutException e){
            return false;
        }
    }

    private void wait(WebElement e,int time){
        await(String.format("Waiting for '%s' to be displayed",nameOf(e)))
                .atMost(time, TimeUnit.SECONDS)
                .pollDelay(1,TimeUnit.MILLISECONDS)
                .pollInterval(300,TimeUnit.MILLISECONDS)
                .ignoreExceptions()
                .until(()-> isElementDisplayed.test(e));
        LOGGER.debug("Page displayed with element - " +nameOf(e));
    }
    private String nameOf(WebElement element){
        try{
            return String.format("[%s]",id(element.toString()));
        }catch (Exception e){
            return "Invalid webelement";
        }
    }
    private static String id(String name){
        name=name.replace("Proxy element for: ","");
        if(name.contains("[[")){
            String sub=name.substring(name.lastIndexOf("[["),name.lastIndexOf("}] -> ")+6);
            name=name.replace(sub,"").replace("]","").trim();
        }
        return name;
    }
    public Predicate<WebElement> isElementDisplayed = WebElement::isDisplayed;

    public Predicate<WebElementFacade> isElementPresent =WebElementFacade::isPresent;


    public WebElementFacade getDynamicElement(WebElement element,String value){
        try{
            return  $(DynamicElement.forElement(element).withValue(value));
        }catch (Exception e){
           throw new RuntimeException("Failed to get dynamic element for "+nameOf(element));
        }
    }

    public void switchToiFrame(WebElementFacade elementFacade){
        driver.switchTo().frame(elementFacade);
    }

    public void clickWithJS(WebElementFacade webElementFacade){
        evaluateJavascript("arguments[0].click();",webElementFacade);
    }

    public void switchToTab(String currentHandle,Set<String> existingHandles){
        Set<String> handles=getDriver().getWindowHandles();
        handles.removeAll(existingHandles);
        for (String actual : handles) {
            if (!actual.equalsIgnoreCase(currentHandle))
            {
                getDriver().switchTo().window(actual);
            }
        }
    }
}
