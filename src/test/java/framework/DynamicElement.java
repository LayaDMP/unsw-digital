package framework;


import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;

import java.lang.reflect.Method;
import java.util.Arrays;

import static org.slf4j.LoggerFactory.getLogger;

public class DynamicElement {
    private static final Logger LOGGER=getLogger(DynamicElement.class);
    private static final Method[] byMethods= By.class.getMethods();
    private final WebElement element;


    public DynamicElement(WebElement element) {
        this.element = element;
    }

    public static DynamicElement forElement(WebElement element){
        return new DynamicElement(element);
    }

    public By withValue(String value){
        try{
            String locator=getLocator(element).toString();
            locator=locator.replace("By.","");
            String[] params=locator.split(":",2);

            String strategy=params[0];
            String dynamicLocator=params[1].trim();

            if(dynamicLocator.contains("${value}")){
                dynamicLocator=dynamicLocator.replace("${value}",value);
            }else{
                LOGGER.error(String.format("Locator %s does not have a dynamic value defined ",locator));
            }

            Method m= Arrays.stream(byMethods).filter(i->i.getName().equalsIgnoreCase(strategy)).findFirst().get();
            return (By) m.invoke(null,dynamicLocator);

        }catch(Exception e){
            throw new RuntimeException("Unable to get locator for dynamic element- "+ element.toString()+ "More details: "+e.getMessage());
        }
    }

    private By getLocator(WebElement element){
        try{
            Object proxy= FieldUtils.readField(element,"h",true);
            Object locator= FieldUtils.readField(proxy,"locator",true);
            Object findBy= FieldUtils.readField(locator,"by",true);

            if(findBy !=null){
                return (By) findBy;
            }
        }catch (IllegalAccessException accessException){
           throw new RuntimeException("Unable to get locator for dynamic element- "+element.toString()+
                    "More details: "+accessException.getMessage());
        }
    return null;
    }
}
