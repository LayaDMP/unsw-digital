package testautomation.api.automation.apidefinitions;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import static framework.EnvInfo.getEnvConfig;

public class Eclips {

    public Response getInternationalDouble(){

        SerenityRest.useRelaxedHTTPSValidation();

        return SerenityRest.given()
                .baseUri(getEnvConfig().getProperty("api.eclips.url"))
                .auth()
                .basic(getEnvConfig().getProperty("eclips.username"),getEnvConfig().getProperty("eclips.password"))
                .queryParam("cl_query","master_cl_id=6c513201db4e9090eb6b147a3a961966")
                .get();
    }
}
