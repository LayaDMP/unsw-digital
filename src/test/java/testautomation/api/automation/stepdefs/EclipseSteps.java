package testautomation.api.automation.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.core.Is;
import testautomation.api.automation.apidefinitions.Eclips;

import static org.hamcrest.MatcherAssert.assertThat;


public class EclipseSteps {

    Response iDoubleDegree;

    @Steps
    Eclips eclips;

    @Then("validates the response code is {int}")
    public void validatesTheResponseCodeIs(int code) {

        assertThat("Validate response",SerenityRest.lastResponse().getStatusCode(), Is.is(200));
    }

    @When("user gets International Double degrees combined courses")
    public void userGetsInternationalDoubleDegreesCombinedCourses() {
        iDoubleDegree= eclips.getInternationalDouble();
    }
}
