package testautomation.api.automation.endpoints;

public enum dmpEndPoints {


    DegreeInternationalDouble("/api/x_f5sl_cl/v4/combined_course/dependent"),
    DegreeInternationalSingle("/api/x_f5sl_cl/v3/course")





    ;






    private final String path;

    dmpEndPoints(String path) {
        this.path=path;
    }

    public String path() {
        return path;
    }
}
