package testautomation.ui.automation;


import framework.BaseInteractions;
import io.cucumber.java.After;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class TestHooks {
    protected static final Logger LOGGER= LoggerFactory.getLogger(TestHooks.class);
    @After
    public void closeDriver()
    {
        getDriver().quit();
        LOGGER.info("Quit driver");
    }

}
