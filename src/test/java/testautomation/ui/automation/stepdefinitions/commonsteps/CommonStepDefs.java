package testautomation.ui.automation.stepdefinitions.commonsteps;

import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;

import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.HeroVideoActions;

import java.util.HashMap;
import java.util.Map;

public class CommonStepDefs {


    @Steps
    public ReferenceSiteActions referencePage;


    @And("^she publishes the page$")
    public void shePublishesThePage() {
        referencePage.clickonpageinformation();
        referencePage.clickOnPublishPage();
    }

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TemplatesActions templatesActions;
    @Steps
    public HeroVideoActions heroVideoActions;

    public static final Map<String,String> pageDetails=new HashMap<>();
    public static String fragmentPageName="";


    @And("she creates a page with template {string} for {string} component")
    public void sheCreatesAPageWithTemplateForComponent(String templateType, String templateName) {
        String prefix= RandomStringUtils.randomAlphabetic(2);
        pageDetails.put("templateTitle",templateName+prefix+"title");
        pageDetails.put("templatePage",templateName+prefix+"page");
        pageDetails.put("templateNavigation",templateName+prefix+"nav");
        pageDetails.put("templateType",templateType);
        pageDetails.put("templateName",(templateName+prefix).replace(" ",""));
        navigationPage.clickOnCreateButton();
        navigationPage.clickOnPageOption();
        templatesActions.createThePage(pageDetails);
    }
    @When("she adds a {string} component in {string} layout")
    public void sheAddsAComponentInTheSelectedTemplate(String componentName,String layout) {
        fragmentPageName=pageDetails.get("templateName");
        templatesActions.clickOnTemplateImage(pageDetails.get("templateName"));
        navigationPage.editThePage();
        navigationPage.switchToNewTab();
        referenceSiteActions.clickOnTheSuppliedColumnLayout(layout);
        referenceSiteActions.clickOnPlusButton();
        referenceSiteActions.enterKeyword(componentName);
        referenceSiteActions.addComponentFromDropdown();
    }

    @When("she also adds {string} component in {string} layout")
    public void sheAlsoAddsAComponent(String componentName,String layout) {
        referenceSiteActions.waitFor(2).seconds();
        referenceSiteActions.clickOnTheSuppliedColumnLayout(layout);
        referenceSiteActions.clickOnPlusButton();
        referenceSiteActions.enterKeyword(componentName);
        referenceSiteActions.addComponentFromDropdown();
    }

}