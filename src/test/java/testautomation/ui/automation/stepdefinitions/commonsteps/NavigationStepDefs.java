package testautomation.ui.automation.stepdefinitions.commonsteps;


import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;



public class NavigationStepDefs {

    @Steps
    public NavigationActions navigation;

    @Steps
    public TemplatesActions templatesActions;


    @And("^she deletes all the previous Pages$")
    public void sheDeletesAllThePreviousPages() {
        navigation.deletePagesUnderTestAutomationFolder();
      //navigation.deleteAllPagesUnderTestAutomationFolder();
    }

    @And("^she navigates to the Test Automation$")
    public void sheNavigatesToTheTestAutomation() {
        navigation.clickOnTestAutomationFolder();
    }
    @And("^she navigates to the published site$")
    public void sheNavigatesToThePublishedSite() {
        navigation.navigateToPublishedSite();
    }

    @And("^she navigates back to Test Automation folder$")
    public void sheNavigatesBackToTestAutomationFolder() {
        navigation.switchToFirstTab();
    }

}
