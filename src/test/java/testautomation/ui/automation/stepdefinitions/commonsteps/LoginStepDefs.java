package testautomation.ui.automation.stepdefinitions.commonsteps;

import testautomation.ui.automation.actions.commonactions.LoginActions;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;


public class LoginStepDefs {


    @Steps
    public LoginActions login;

    @Given("^Tracy logs in to the AEM site$")
    public void tracyLogsInToTheAEMSite() {
        login.tracyLogsInToTheAEMSite();
    }




}
