package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.QualtricsActions;

public class QualtricsStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public QualtricsActions qualtricsActions;

    public static final String qURL= "https://unsw.au1.qualtrics.com/jfe/form/SV_afbCZH5O1oN86fY";
    public static final Integer qHeight = 800;

    @And("she configures the {string} component with Qualtrics option")
    public void sheConfiguresTheComponentWithQualtricsOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        qualtricsActions.selectQualtricsDropDown();
        qualtricsActions.editQualtricsOption(qURL,qHeight);
        referenceSiteActions.clickOnDoneButton();
    }
    @Then("she should see the Qualtrics in the published page")
    public void sheShouldSeeTheQualtricsInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(qualtricsActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(qualtricsActions.publishedQualtricsFrame()).isTrue();

        softly.assertAll();
    }
}
