package testautomation.ui.automation.stepdefinitions.componentsteps;

import org.apache.commons.lang3.RandomStringUtils;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.CTAButtonActions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class CTAButtonStepDefs {

;
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public CTAButtonActions ctaButtonActions;

    String randomName= RandomStringUtils.randomNumeric(2);
    String ctaButtonName="UniqueButton1";
    String text="";



    @And("she publishes CTA button with {string} {string} {string} {string} {string} {string}")
    public void shePublishesCTAButtonWith(String Alignment, String Link, String labelStyle, String buttonColor, String buttonStyle, String buttonSize) {
        ctaButtonName="CTAbutton"+randomName;
        ctaButtonActions.clickOnCTAButtonAdded();
        referenceSiteActions.clickOnConfigureButton();
        referenceSiteActions.gotoManualSettingsTab();
        ctaButtonActions.clickOnAddButtonCTAButton();
        ctaButtonActions.enterInTextField(ctaButtonName);
        ctaButtonActions.selectButtonSize(buttonSize);
        ctaButtonActions.selectButtonColor(buttonColor);
        ctaButtonActions.selectButtonStyle(buttonStyle);
        ctaButtonActions.selectLabelStyle(labelStyle);
        ctaButtonActions.enterValueInLinkField(Link);
        ctaButtonActions.selectButtonAlignment(Alignment);
        referenceSiteActions.clickOnDoneButton();

    }
    @And("she validates CTA button with {string} {string} {string} {string} {string} {string}")
    public void sheValidatesCTAButtonWith(String Alignment, String Link, String labelStyle, String buttonColor, String buttonStyle, String buttonSize) {
        assertThat(ctaButtonActions.isCTAButtonAlignmentPublishedSite(Alignment)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonTextPublishedSite(ctaButtonName)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonLinkPublishedSite(Link)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonLabelStylePublishedSite(labelStyle)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonColorPublishedSite(buttonColor)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonStyle(buttonStyle)).isTrue();
        assertThat(ctaButtonActions.isCTAButtonSize(buttonSize)).isTrue();
    }

}
