package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.When;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.GeneralCardActions;
import testautomation.ui.automation.actions.componentactions.HorizontalScrollActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;
import static testautomation.ui.automation.stepdefinitions.commonsteps.CommonStepDefs.fragmentPageName;


public class HorizontalScrollStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public HorizontalScrollActions horizontalScrollActions;
    @Steps
    GeneralCardActions generalCardActions;

    String text="HorizontalScrollText";


    @And("^she should see the next slider arrow and slider dotted on the published site$")
    public void sheShouldSeeTheNextSliderArrowAndSliderDottedOnThePublishedSite() {
        assertThat(horizontalScrollActions.verifySiderDottedPublishedSite()).isTrue();
        assertThat(horizontalScrollActions.verifySliderArrowHorizontalScrollPublishedSite()).isTrue();

    }

    @When("she configures horizontal scroll component with {string} {string} {string} {string}")
    public void sheConfiguresHorizontalScrollComponentWith(String columnLayout, String numberOfComponents, String componentToAdd, String fragmentPath) {
        horizontalScrollActions.addHorizontalScrollComponent(Integer.valueOf(numberOfComponents), columnLayout, componentToAdd);
        horizontalScrollActions.configureEachComponent(Integer.valueOf(numberOfComponents), componentToAdd, fragmentPath,
                text, IMAGENAMETOUSE,fragmentPageName );
    }

    @Then("she should see the same number of {string} on the published site {int}")
    public void sheShouldSeeTheSameNumberOfOnThePublishedSite(String component, int ComponentNumber) {
        if(component.toLowerCase().contains("image")){
            assertThat(horizontalScrollActions.verifyImageComponent(ComponentNumber,IMAGENAMETOUSE)).doesNotContain(false);

        }else if(component.toLowerCase().contains("text")){
            assertThat(horizontalScrollActions.verifyTextComponent(text,ComponentNumber)).doesNotContain(false);
        }
    }

}
