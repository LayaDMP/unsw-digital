package testautomation.ui.automation.stepdefinitions.componentsteps;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.DegreePageHeaderActions;
import testautomation.ui.automation.actions.componentactions.TextActions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.core.Serenity.getDriver;
import static org.assertj.core.api.Java6Assertions.assertThat;
import java.time.format.DateTimeFormatter;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class DegreePageHeaderStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public DegreePageHeaderActions degreePageHeaderActions;
    @Steps
    public TextActions textActions;

    private final String heading="Heading_PageHeader";
    private final String summary="Summary_PageHeader";
    private final String date=today.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));
    private final String authorDetails="William";




    @And("^she should see the header logo added \"([^\"]*)\"$")
    public void sheShouldSeeTheHeaderLogoAdded(String headerLogo)  {
        degreePageHeaderActions.verifyLogoPageHeaderOnPublishedPage(headerLogo);
    }


    @And("she configures the Degree page header component {string} {string} {string}")
    public void sheConfiguresTheDegreePageHeaderComponent(String type, String backgroundShape, String asset) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        degreePageHeaderActions.clickOnPageHeaderComponent();
        referenceSiteActions.clickOnConfigureButton();
        degreePageHeaderActions.selectType(type);
        if (type.contains("Manual")) {
            degreePageHeaderActions.enterInHeadingField(heading);
            degreePageHeaderActions.clickGeneralTab();
            degreePageHeaderActions.enterInSummaryField(summary);
            degreePageHeaderActions.enterInDateField(date);
            referenceSiteActions.dragAndDropImageToComponent();
            degreePageHeaderActions.enterInAuthorDetails(authorDetails);
            degreePageHeaderActions.chooseBackgroundShape(backgroundShape);
            degreePageHeaderActions.chooseAsset(asset);
            referenceSiteActions.clickOnDoneButton();
        }
    }

    @Then("she should see the Degree page header component on the published page {string}")
    public void validatePageHeaderComponentOnThePublishedPage(String backgroundShape) {

        assertThat(degreePageHeaderActions.verifyHeadingPageHeaderOnPublishedPage(heading)).isTrue();
        assertThat(degreePageHeaderActions.verifySummaryPageHeaderOnPublishedPage(summary)).isTrue();
        //assertThat(degreePageHeaderActions.verifyDatePageHeaderOnPublishePage(date)).isTrue();
        assertThat(degreePageHeaderActions.verifyLogoPageHeaderOnPublishedPage(IMAGENAMETOUSE)).isTrue();
        assertThat(degreePageHeaderActions.verifyAuthorPageHeaderOnPublishedPage(authorDetails)).isTrue();
        if (backgroundShape.toLowerCase().contains("shape")){
            assertThat(degreePageHeaderActions.verifyPageHeaderComponentShapePublishedSite()).isTrue();
        }
        else{
            assertThat(degreePageHeaderActions.verifyPageHeaderShape()).isFalse();
        }
    }

    @And("she configures degree dropdown filter with default values")
    public void sheConfiguresDegreeDropdownFilter() {
        degreePageHeaderActions.clickOnDropdownFilter();
        referenceSiteActions.clickOnConfigureButton();
        referenceSiteActions.gotoManualSettingsTab();
        degreePageHeaderActions.selectInternationalOption();
        degreePageHeaderActions.selectDomesticOption();
        referenceSiteActions.clickOnDoneButton();
    }

    @And("she should also see the options under dropdown filter component")
    public void sheShouldAlsoSeeTheOptionsUnderDropdownFilterComponent() {

    assertThat(degreePageHeaderActions.selectOptionFromDropdown("Domestic")).contains("selected");
    assertThat(degreePageHeaderActions.selectOptionFromDropdown("International")).contains("selected");

    }
}
