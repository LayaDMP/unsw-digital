package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.KeyDatesActions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class KeyDatesStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public KeyDatesActions keyDatesActions;

    @And("she configures {int} set of parameters to key dates with {string}")
    public void sheConfiguresSetOfParamerters(int num, String tab) {
        keyDatesActions.addParametersToKeyDates(num,tab);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she validates all the configured fields are displayed")
    public void sheValidatesAllTheConfiguredFieldsAreDisplayed() {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(keyDatesActions.validateShowmoreBtn()).isTrue();
        softly.assertThat(keyDatesActions.validateShowlessBtn()).isTrue();
        softly.assertThat(keyDatesActions.validateDate()).containsOnly(true);
        softly.assertThat(keyDatesActions.validateTitle()).containsOnly(true);
        softly.assertThat(keyDatesActions.validateSubtitle()).containsOnly(true);
        softly.assertThat(keyDatesActions.validateReadmoreLink()).containsOnly(true);
        softly.assertAll();
    }
}