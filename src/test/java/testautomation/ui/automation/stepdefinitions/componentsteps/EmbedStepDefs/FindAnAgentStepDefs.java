package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.PageObject;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.FindAnAgentActions;

public class FindAnAgentStepDefs extends PageObject {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public FindAnAgentActions findAnAgentActions;

    public static final String iFrameId = "https://eap.ascentone.com/unsw?type=E";

    @And("she configures the {string} component with iFrame option")
    public void sheConfiguresTheComponentWithIFrameOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        findAnAgentActions.selectFindAnAgentPageDropDown();
        findAnAgentActions.editFindAnAgentPageOption(iFrameId);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the iFrame in the published page")
    public void sheShouldSeeTheIFrameInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(findAnAgentActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(findAnAgentActions.publishedFindAnAgentPageFrame()).isTrue();

        softly.assertAll();
    }
}
