package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.EventBriteActions;

public class EventBriteStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public EventBriteActions eventBriteActions;

    public static final long eventID = 215366175027L;

    @And("she configures the {string} component with Eventbrite option")
    public void sheConfiguresTheComponentWithEventbriteOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        eventBriteActions.selectEventBriteDropDown();
        eventBriteActions.editEventBriteOption(eventID);
        referenceSiteActions.clickOnDoneButton();
    }
    @Then("she should see the Eventbrite in the published page")
    public void sheShouldSeeTheEventbriteInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(eventBriteActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(eventBriteActions.publishedEventBriteFrame()).isTrue();

        softly.assertAll();

    }



}
