package testautomation.ui.automation.stepdefinitions.componentsteps;


import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.HeroActions;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;

public class HeroStandardStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public HeroActions heroActions;

    String heading="unique2";
    String subHeading="This is the sub heading";


    @And("she configures the hero standard component with {string} type")
    public void sheConfiguresTheHeroStandardComponent(String type) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        heroActions.clickOnHerostandardComponent();
        referenceSiteActions.clickOnConfigureButton();
        heroActions.chooseType(type);
        referenceSiteActions.dragAndDropImageToComponent();
        heroActions.enterInHeadingfieldHeroStandard(heading);
        heroActions.enterInSubheadingfieldHeroStandard(subHeading);
        referenceSiteActions.clickOnDoneButton();
    }
    @And("^she chooses the the style for the component \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheChoosesTheTheStyleForTheComponent(String style, String keyword) {
        heroActions.clickOnTheConfiguredComponent(keyword);
        heroActions.clickOnStyleOption();
        heroActions.selectStyeFromDropdown(style);

    }

    @And("she should see the hero standard component as configured")
    public void sheShouldSeeTheHeroStandardComponents() {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(heroActions.verifyComponentStyleHeroStandard()).isTrue();
        softly.assertThat(heroActions.verifyBackgroundImageHeroStandard(IMAGENAMETOUSE)).isTrue();
        softly.assertThat(heroActions.verifyHeadingHeroStandard(heading)).isTrue();
        softly.assertThat(heroActions.verifySubheadingHeroStandard(subHeading)).isTrue();
        softly.assertAll();
    }

}
