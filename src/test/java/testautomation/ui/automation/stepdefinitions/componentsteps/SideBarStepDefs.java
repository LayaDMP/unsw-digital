package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.SideBarCTAActions;

public class SideBarStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public SideBarCTAActions sideBarCTAActions;


    @And("she configures the {string} component with {string} {string} {string} {string}")
    public void sheConfiguresTheComponentWith(String componentName, String sideTitle, String sideSub, String sideButton, String sideLinkURL) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        sideBarCTAActions.editSideBarCTAComponent(sideTitle,sideSub,sideButton,sideLinkURL);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she validates the sidebar CTA button with {string} {string} {string} {string}")
    public void sheValidatesTheSidebarCTAButtonWith(String sideTitle, String sideSub, String sideButton, String sideLinkURL) {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(sideBarCTAActions.publishedSideCTAComponent()).isTrue();
        softly.assertThat(sideBarCTAActions.publishedSideBarTitleText()).isTrue();
        softly.assertThat(sideBarCTAActions.publishedSideBarSubTitleText()).isTrue();
        softly.assertThat(sideBarCTAActions.publishedSideBarButtonText()).isTrue();
        softly.assertThat(sideBarCTAActions.publishedSideBarNewPageOpen()).isTrue();

        softly.assertAll();

    }
}
