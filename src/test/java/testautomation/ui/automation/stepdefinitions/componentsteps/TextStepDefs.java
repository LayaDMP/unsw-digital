package testautomation.ui.automation.stepdefinitions.componentsteps;


import org.apache.commons.lang3.RandomStringUtils;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.TextActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class TextStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TextActions textActions;

    String headingTitle="";



    @Then("she should see the same text on the published page {string} {string} {string}")
    public void sheShouldSeeTheSameTextOnThePublishPage(String headingText, String headingLevel, String headingAlignment) {
        assertThat(textActions.validateSize(headingLevel)).isTrue();
        assertThat(headingText).isEqualTo(textActions.validateText(headingLevel));
        assertThat(textActions.validateAlignment(headingLevel)).contains(headingAlignment);
    }

    @And("she configures the {string} component with paragraph {string} {string}")

    public void sheConfiguresTheTextComponentWithParagraph(String componentName,String headingLevel, String headingAlignment) {
        headingTitle= RandomStringUtils.randomAlphabetic(200);
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        referenceSiteActions.gotoManualSettingsTab();
        textActions.editTextComponent(headingTitle, headingLevel, headingAlignment);
        referenceSiteActions.clickOnDoneButton();
    }

    @And("she configures the {string} component {string} {string} {string}")
    public void sheConfiguresTheComponent(String componentName,String title,String headingLevel, String headingAlignment) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        referenceSiteActions.gotoManualSettingsTab();
        textActions.editTextComponent(title, headingLevel, headingAlignment);
        referenceSiteActions.clickOnDoneButton();
    }


}

