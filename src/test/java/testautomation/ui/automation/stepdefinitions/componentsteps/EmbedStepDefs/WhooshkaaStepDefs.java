package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.WhooshkaaActions;

public class WhooshkaaStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public WhooshkaaActions whooshkaaActions;

    public static final int whooshkaaId = 912605;

    @And("she configures the {string} component with Whooshkaa option")
    public void sheConfiguresTheComponentWithWhooshkaaOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        whooshkaaActions.selectWhooshkaaDropDown();
        whooshkaaActions.editWhooshkaaOption(whooshkaaId);
        referenceSiteActions.clickOnDoneButton();
    }
    @Then("she should see the Whooshkaa in the published page")
    public void sheShouldSeeTheWhooshkaaInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(whooshkaaActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(whooshkaaActions.publishedWhooshkaaFrame()).isTrue();

        softly.assertAll();
    }



}
