package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ListActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ListStepdefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public ListActions listActions;

    String headingTitle="";
    List<String> namesOflist=new ArrayList<>();

         @And("she configures the {string} component {int}")
         public void sheConfiguresTheComponent(String componentName,int items) {
             referenceSiteActions.clickOnAddedComponent(componentName);
             referenceSiteActions.clickOnConfigureButton();
             referenceSiteActions.gotoManualSettingsTab();
             namesOflist=listActions.editListComponent(items);
             referenceSiteActions.clickOnDoneButton();
         }

    @Then("she should see the same text on the published page {string}")
    public void sheShouldSeeTheSameTextOnThePublishedPage(String text) {
        assertThat(listActions.checkTheListItems()).isEqualTo(listActions.getListNames());
    }

}

