package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.StandoutActions;

public class StandoutStepdefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public StandoutActions standoutActions;



    @And("she configures the {string} component with {string}")
    public void sheConfiguresTheComponentWith(String componentName, String sText) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        standoutActions.editStandoutComponent(sText);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the standout component with {string}")
    public void sheShouldSeeTheStandoutComponentWith(String sText) {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(standoutActions.publishedStandoutComponent()).isTrue();
        softly.assertThat(standoutActions.publishedStandoutText()).isTrue();
        softly.assertAll();
    }



}
