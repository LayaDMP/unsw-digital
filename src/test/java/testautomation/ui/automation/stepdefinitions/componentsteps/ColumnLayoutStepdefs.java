package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ColumnLayoutActions;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

public class ColumnLayoutStepdefs {


    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;


    @Steps
    public ReferenceSiteActions referenceSiteActions;

    @Steps
    public ColumnLayoutActions columnLayoutActions;


}
