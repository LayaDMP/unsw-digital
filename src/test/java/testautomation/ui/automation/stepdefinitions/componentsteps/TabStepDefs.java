package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.datatable.DataTable;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.TabActions;

import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class TabStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TabActions tabActions;


    @And("she configures the {string} component")
    public void sheConfiguresTheComponent(String componentName) {
        tabActions.clickTabComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        tabActions.editTabComponent();
        referenceSiteActions.clickOnDoneButton();
        tabActions.editTabTitles();

    }


    @And("she configures the tab component with {string} and {string}")
    public void sheConfiguresTheComponentWithAnd(String component1, String component2) {
        tabActions.clickTabComponent("Tabs (v2)");
        referenceSiteActions.clickOnConfigureButton();
        tabActions.editTabComponent(component1,component2);
    }

    @And("she configures the tab component with below components")
    public void sheConfiguresTheTabComponentWithBelowComponents(DataTable table) {

        List<String> componentList = table.asList();
        tabActions.clickTabComponent("Tabs (v2)");
        referenceSiteActions.clickOnConfigureButton();
        tabActions.editTabComponent(componentList);
        referenceSiteActions.clickOnDoneButton();
    }

    @And("she navigates to {string} tab")
    public void sheNavigatesToTab(String tabName) {

        tabActions.navigateToTab(tabName);
    }
}

