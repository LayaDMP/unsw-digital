package testautomation.ui.automation.stepdefinitions.componentsteps;

import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.GeneralCardActions;
import testautomation.ui.automation.actions.componentactions.IconTilesActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import static org.assertj.core.api.Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.PATH_TO_TEST_AUTOMATION;

public class IconTilesStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public IconTilesActions iconTilesActions;
    @Steps
    GeneralCardActions generalCardActions;


    String heading="Heading_Icon";
    String subTitle="Subtitle";
    String titleCopy="TitleCopy";
    String buttonLabel="ButtonLabel";

    @And("she configures the Icon Tile with default values and {string}")
    public void sheConfiguresTheIconTileWithDefaultValuesAnd(String titleStyle) {
        iconTilesActions.clickOnIconTileAdded();
        referenceSiteActions.clickOnConfigureButton();
        iconTilesActions.selectTitleStyle(titleStyle);
        iconTilesActions.clickOnAddButtonIcontile();
        iconTilesActions.addIconForThisTile();
        iconTilesActions.iconTileAddHeading(heading);
        iconTilesActions.iconTileAddSubtitle(subTitle);
        iconTilesActions.selectButtonURLIconTile(PATH_TO_TEST_AUTOMATION);
        iconTilesActions.iconTileAddTitleCopy(titleCopy);
        iconTilesActions.iconTileAddButtonLabel(buttonLabel);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the Icon tile configured with {string} on published page")
    public void sheShouldSeeTheIconTileConfiguredWithOnPublishedPage(String titleStyle) {

        assertThat(iconTilesActions.verifyIconForThisTilePublishedPage()).isTrue();
        assertThat(iconTilesActions.verifyTitleStylePublishedPage(titleStyle)).isTrue();
        assertThat(iconTilesActions.verifyHeadingPublisedPage()).isEqualToIgnoringWhitespace(heading);
        assertThat(iconTilesActions.verifySubtitlePublishedPage()).isEqualToIgnoringWhitespace(subTitle);
        assertThat(iconTilesActions.verifyTitleCopyPublishedPage()).isEqualToIgnoringWhitespace(titleCopy);
        assertThat(iconTilesActions.verifyButtonLabelPubishedPage(buttonLabel)).isTrue();


    }
}
