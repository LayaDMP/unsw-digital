package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.HeroGeneralActions;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class HeroGeneralStepDefs {


    @Steps
    NavigationActions navigationPage;
    @Steps
    TemplatesActions templateActions;
    @Steps
    ReferenceSiteActions referenceSiteActions;
    @Steps
    HeroGeneralActions heroGeneralActions;

    String dateOnlabel= today.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    String authorName= "Tracy";
    Map<String,String> config=new HashMap<>();


    @And("she configures hero general background with {string}")
    public void sheConfiguresHeroGeneralComponentWith(String background) {

        heroGeneralActions.configureImageprop(background);
        referenceSiteActions.clickOnDoneButton();


    }

    @And("she configures text and content with date {string} and description {string}")
    public void sheConfiguresTextAndContentWithDateAndDescription(String dateEnabled, String description) {
        referenceSiteActions.goToConfigOfComponent("Hero General");
        config.put("date",dateOnlabel);
        config.put("authorName",authorName);
        config.put("enableDate",dateEnabled);
        config.put("enableDescription",description);
        heroGeneralActions.configureTextAndContent(config);
    }

    @Then("she validates all configured fields for hero general are displayed")
    public void sheValidatesAllConfiguredFieldsForHeroGeneralAreDisplayed() {

        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(heroGeneralActions.publishedAuthorImage()).isTrue();
        softly.assertThat(heroGeneralActions.publishedBackgroundImage()).isTrue();
        softly.assertThat(heroGeneralActions.publishedFeatureImage()).isTrue();
        softly.assertThat(heroGeneralActions.publishedPageHeading()).isTrue();
        if(config.get("enableDate").equalsIgnoreCase("yes")){
        softly.assertThat(heroGeneralActions.publishedDate()).contains(dateOnlabel);}
        softly.assertThat(heroGeneralActions.publishedAuthorName()).contains(authorName);
        softly.assertAll();
    }
}
