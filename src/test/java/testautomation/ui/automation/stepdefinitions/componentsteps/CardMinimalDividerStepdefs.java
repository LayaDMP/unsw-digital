package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;

import io.cucumber.java.en.And;
import testautomation.ui.automation.actions.componentactions.CardMinimalDividerActions;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class CardMinimalDividerStepdefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public CardMinimalDividerActions cardMinimalDividerActions;


    @And("she configures the {string} component with {string} {string} {string}")
    public void sheConfiguresTheComponentWith(String componentName, String heading, String desc, String url) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        cardMinimalDividerActions.clickOnAddButtonCardMinimalDivider();
        cardMinimalDividerActions.editCardMinimalDividerComponent(heading,desc,url);
        referenceSiteActions.clickOnDoneButton();

    }

    @Then("she should see the card minimal divider component with {string} {string} {string}")
    public void sheShouldSeeTheCardMinimalDividerComponentWith(String heading, String desc, String url) {

        assertThat(cardMinimalDividerActions.verifyPublishedCardMinimalComponent()).isTrue();
       assertThat(cardMinimalDividerActions.verifyPublishedCardMinimalHeader()).isTrue();
       assertThat(cardMinimalDividerActions.verifyPublishedCardMinimalDesc()).isTrue();
      assertThat(cardMinimalDividerActions.verifyPublishedCardMinimalUrl()).isTrue();

    }
}
