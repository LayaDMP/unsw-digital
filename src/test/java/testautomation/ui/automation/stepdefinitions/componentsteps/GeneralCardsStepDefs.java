package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ContentFragmentActions;
import testautomation.ui.automation.actions.componentactions.ExperienceFragmentActions;
import testautomation.ui.automation.actions.componentactions.GeneralCardActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.*;
import static testautomation.ui.automation.stepdefinitions.commonsteps.CommonStepDefs.fragmentPageName;


public class GeneralCardsStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public ContentFragmentActions contentFragmentActions;
    @Steps
    GeneralCardActions generalCardActions;
    @Steps
    ExperienceFragmentActions experienceFragment;


    String headingText="HeadngText";
    String altText="Alt_Unique1";
    String standFirst="Standfirst_example";
    String category="category";
    String pageName="";


    @And("she configures the content fragment with {string} {string}")
    public void sheConfiguresTheContentFragmentWith(String ContentFragmentPath, String DisplayMode) {
        referenceSiteActions.clickOnAddedComponent("Content Fragment");
        referenceSiteActions.clickOnConfigureButton();
        contentFragmentActions.addContentFragmentPath(ContentFragmentPath);
        generalCardActions.selectDisplayMode(DisplayMode);
        contentFragmentActions.addArticleContentElement();
        referenceSiteActions.clickOnDoneButton();
        pageName=fragmentPageName;
    }


    @Then("^she should see the same general card in square shaped alignment on the published site$")
    public void sheShouldSeeTheSameGeneralCardInSquareShapedAlignmentOnThePublishedSite() {
        generalCardActions.verifyGeneralCardOnPublishedSite();
        generalCardActions.verifySquareCardAlignmentOnPblishedSite();
    }

    @And("she adds a square card theme to the general card in {string}")
    public void sheAddsASquareCardThemeToTheGeneralCardIn(String columnLayout) {
        generalCardActions.clickOnGeneralComponent();
        referenceSiteActions.clickOnConfigureButton();
        generalCardActions.clickOnSquareCardTheme();
        generalCardActions.clickOnContentFragmentCardSettingsTab();
        referenceSiteActions.selectFixedListDropdownOption();
        generalCardActions.addPathToContentFragmentPage(pageName);
        referenceSiteActions.clickOnDoneButton();
    }

    @And("^she should be navigated to the linked page/site on clicking the general card \"([^\"]*)\"$")
    public void sheShouldBeNavigatedToTheLinkedPageSiteOnClickingTheGeneralCard(String Link) {
        generalCardActions.click_on_generalCard_on_publishedSite();
        generalCardActions.verifyNavigatedPageFromGeneralCard(Link);
    }

    @Then("^she should see the general card on the published page$")
    public void sheShouldSeeTheGeneralCardOnThePublishedPage() {
        assertThat(generalCardActions.verifyGeneralCardOnPublishedSite()).isTrue();
    }

    @And("^she clicks on the general card$")
    public void sheClicksOnTheGeneralCard() {
        generalCardActions.click_on_generalCard_on_publishedSite();
    }

    @And("^she should be navigated to the published page \"([^\"]*)\"$")
    public void sheShouldBeNavigatedToThePublishedPage(String link) {
        assertThat(generalCardActions.
                verifyNavigatedPageFromGeneralCard(link)).isTrue();
    }

    @When("^she tries to save the General Card without entering any field values$")
    public void sheTriesToSaveTheGeneralCardWithoutEnteringAnyFieldValues() {
        generalCardActions.clickOnGeneralCardAddedOnPage();
        referenceSiteActions.clickOnConfigureButton();
        generalCardActions.clickOnManualSettingsDropdown();
        generalCardActions.navigateToManualSettingsTab();
        generalCardActions.clickOnAddButton();
    }

    @Then("^she should get an alert for all the mandatory fields$")
    public void sheShouldGetAnAlertForAllTheMandatoryFields() {
        referenceSiteActions.clickOnDoneButton();
        assertThat(generalCardActions.verifyAlertsAreDisplayed()).doesNotContain(false);
    }

    @And("she configures the card with manual settings with {string}")
    public void sheConfiguresTheCardWithManualSettingsWith(String link) {
        navigationPage.refreshPageURL();
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        generalCardActions.clickOnGeneralCardAddedOnPage();
        referenceSiteActions.clickOnConfigureButton();
        generalCardActions.clickOnManualSettingsDropdown();
        generalCardActions.navigateToManualSettingsTab();
        generalCardActions.fillFields(headingText, altText, standFirst, category,
                today.format(DateTimeFormatter.ofPattern("MMMM dd,yyyy")) , link, IMAGENAMETOUSE);

    }
}
