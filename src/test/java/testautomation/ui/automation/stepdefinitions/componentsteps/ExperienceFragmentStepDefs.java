package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ExperienceFragmentActions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;


import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

public class ExperienceFragmentStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;

    @Steps
    public ReferenceSiteActions refereceSiteActions;

    @Steps
    public ExperienceFragmentActions experienceFragment;

    @And("^she navigates to the Test Automation folder in Experience Fragment$")
    public void sheNavigatesToTheTestAutomationFolderInExperienceFragment() {
        experienceFragment.navigateToTestAutomationFolderInExperienceFragment();
    }

    @And("^she creates an Experience Fragment \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheCreatesAnExperienceFragment(String templateType, String templateTitle, String templateName, String templateDescription) {
        experienceFragment.clickOnCreateButtonExFragment();
        experienceFragment.selectExperienceFragmentFromDropdown();
        experienceFragment.selectTemplateType(templateType);
        experienceFragment.enterTemplateTitleExFragment(templateTitle);
        experienceFragment.enterTemplateNameExFragment(templateName);
        experienceFragment.enterTemplateDescriptionExFragment(templateDescription);
        experienceFragment.clickOnCreateButtonToCreateExFragment();
        experienceFragment.clickOnDoneButtonExFragment();
        navigationPage.refreshPageURL();
    }

    @And("^she navigates to the Experience Fragment created \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheNavigatesToTheExperienceFragmentCreated(String templateTitle, String templateName) {
        navigationPage.refreshPageURL();
        experienceFragment.clickOnTemplateTitleFolderUnderTestAutomation(templateTitle);
        experienceFragment.clickOnExperienceFragment(templateName);
        navigationPage.editThePage();
    }

    @And("^adds UNSW Common Global Header component to the following layout \"([^\"]*)\"$")
    public void addsUNSWCommonGlobalHeaderComponentToTheFollowingLayout(String column_layout) {
        navigationPage.switchToNewTab();
        refereceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);
        refereceSiteActions.clickOnPlusButton();
        experienceFragment.addUnswCommonGlobalHeaderComponent();
    }

    @And("^she configures the Global Header component with sections \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheConfiguresTheGlobalHeaderComponentWithSections(String imageName, String LogoLink, String SearchPageLink, String SearchLabel, int NumberOfSections) {
        experienceFragment.clickOnSideToggleBar();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
        refereceSiteActions.searchForImage(imageName);
        experienceFragment.clickOnContentTree();
        experienceFragment.clickOnUNSWCommonGlobalHeaderUnderContentTreeSideToggleBar();
        experienceFragment.clickOnConfigureButton();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
//          Drag and drop the searched image to the component chosen before
        refereceSiteActions.dragAndDropImageToComponent();
        experienceFragment.enterInLogoLinkCommonGlobalHeaderComponent(LogoLink);
        experienceFragment.enter_in_SearchPageLink_CommonGlobalHeaderComponent(SearchPageLink);
        experienceFragment.enter_in_SearchLabel(SearchLabel);
        experienceFragment.click_on_SectionsTab_CommonGlobalHeaderComponent();
        experienceFragment.add_number_of_GlobalHeaderSections_CommonGlobalHeaderComponent(NumberOfSections);
        refereceSiteActions.clickOnDoneButton();
    }


    @And("^she configures the individual Global Header sections and creates sub-sections \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheConfiguresTheIndividualGlobalHeaderSectionsAndCreatesSubSections(int NumberOfSubSections, String SectionTitile1, String SectionTitile2, String SectionTitile3, String SectionTitileLink1, String SectionTitileLink2, String SectionTitileLink3) {

        experienceFragment.click_on_contentTree_SideToggleBar();
        experienceFragment.configure_firs_GlobalHeaderSection(SectionTitile1, NumberOfSubSections, SectionTitileLink1);
        refereceSiteActions.clickOnDoneButton();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();
        experienceFragment.configure_second_GlobalHeaderSection(SectionTitile2, NumberOfSubSections, SectionTitileLink2);
        refereceSiteActions.clickOnDoneButton();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();
        experienceFragment.configure_third_GlobalHeaderSection(SectionTitile3, NumberOfSubSections, SectionTitileLink3);
        refereceSiteActions.clickOnDoneButton();

    }

    @And("^she configures the individual Global Header sub section \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"  \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheConfiguresTheIndividualGlobalHeaderSubSection(String SubSection_SectionTitile1, String SubSection_SectionTitile2, String SubSection_SectionTitile3, String SubSection_SectionTitile4, String SubSection_SectionTitile5, String SubSection_SectionTitile6, String SubSection_SectionTitile7, String SubSection_SectionTitile8, String SubSection_SectionTitile9, String SubSection_SectionTitileLink1, String SubSection_SectionTitileLink2, String SubSection_SectionTitileLink3, String SubSection_SectionTitileLink4, String SubSection_SectionTitileLink5, String SubSection_SectionTitileLink6, String SubSection_SectionTitileLink7, String SubSection_SectionTitileLink8, String SubSection_SectionTitileLink9, int NumberOfSections, int NumberOfSubSections) {
        int numberOfSubSections = NumberOfSubSections * NumberOfSections;
        experienceFragment.click_on_contentTree_SideToggleBar();
        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();

        experienceFragment.configureFirstSubSection(SubSection_SectionTitile1, SubSection_SectionTitileLink1);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();


        experienceFragment.configuresecondSubSection(SubSection_SectionTitile2, SubSection_SectionTitileLink2);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();


        experienceFragment.configuresthirdSubSection(SubSection_SectionTitile3, SubSection_SectionTitileLink3);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();


        experienceFragment.configuresfourthSubSection(SubSection_SectionTitile4, SubSection_SectionTitileLink4);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();

        experienceFragment.configuresfifthSubSection(SubSection_SectionTitile5, SubSection_SectionTitileLink5);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();

        experienceFragment.configuressixthSubSection(SubSection_SectionTitile6, SubSection_SectionTitileLink6);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();

        experienceFragment.configuresseventhSubSection(SubSection_SectionTitile7, SubSection_SectionTitileLink7);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();

        experienceFragment.configureseigthSubSection(SubSection_SectionTitile8, SubSection_SectionTitileLink8);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();


        experienceFragment.configuresninthSubSection(SubSection_SectionTitile9, SubSection_SectionTitileLink9);
        refereceSiteActions.clickOnDoneButton();

        experienceFragment.clickOnAssetsFolderSideToggleBar();
        experienceFragment.click_on_contentTree_SideToggleBar();


    }
}
