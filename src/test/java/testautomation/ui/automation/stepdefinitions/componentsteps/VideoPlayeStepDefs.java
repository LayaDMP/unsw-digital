package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.VideoActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.YOUTUBE_ID;

public class VideoPlayeStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public VideoActions videoActions;


    @When("^she crops the video in Portrait mode \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheCropsTheVideoInPortraitMode(String cropMode, String videoLink) {
        videoActions.clickOnConfigureVideoComponent(cropMode, videoLink);

    }

    @Then("^she should see the video on the published site \"([^\"]*)\"$")
    public void sheShouldSeeTheVideoOnThePublishedSite(String videoLink) {
        assertThat(videoActions.verifyVideoOnPublishedSite(videoLink)).isTrue();
    }


    @And("she should see the default video on the published site")
    public void sheShouldSeeTheDefaultVideoOnThePublishedSite() {
        assertThat(videoActions.verifyVideoOnPublishedSite(YOUTUBE_ID)).isTrue();
    }

    @When("she crops the video in Portrait mode {string}")
    public void sheCropsTheVideoInPortraitMode(String cropmode) {

        videoActions.clickOnConfigureVideoComponent(cropmode,YOUTUBE_ID);
    }
}
