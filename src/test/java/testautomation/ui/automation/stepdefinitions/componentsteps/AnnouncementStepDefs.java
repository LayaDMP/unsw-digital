package testautomation.ui.automation.stepdefinitions.componentsteps;


import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.componentactions.AnnouncementActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.*;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.PATH_TO_TEST_AUTOMATION;

public class AnnouncementStepDefs {

    @Steps
    ReferenceSiteActions referenceSiteActions;

    @Steps
    AnnouncementActions announcementActions;

    Map<String,String> componentDetails=new HashMap<>();




    @Then("she validates published short component are working as configured")
    public void publishedComponentAreWorkingAsConfigured() {
        assertThat(announcementActions.isPublishedTextPresent()).isNotNull();
        assertThat(announcementActions.isPublishedLinkPresent()).isTrue();
        assertThat(announcementActions.validateTab()).isTrue();
        if(componentDetails.get("icon").toLowerCase().contains("enabled")) {
            assertThat(announcementActions.validateCloseIcon()).isFalse();
        }
    }

    @When("she configures the short page announcement component with")
    public void shortThePageAnnouncementComponentWith(DataTable table) {
        componentDetails=table.asMap(String.class,String.class);
        referenceSiteActions.clickOnAddedComponent("Page Announcement");
        referenceSiteActions.clickOnConfigureButton();
        announcementActions.configureTypeWith(componentDetails);
        referenceSiteActions.clickOnDoneButton();
    }

    @When("she configures the long page announcement component with")
    public void longPageAnnouncementComponentWith(DataTable table) {
        componentDetails=table.asMap(String.class,String.class);
        referenceSiteActions.clickOnAddedComponent("Page Announcement");
        referenceSiteActions.clickOnConfigureButton();
        announcementActions.configureTypeWith(componentDetails);

    }

   @And("she configures the page announcement buttons with below attributes")
    public void sheConfiguresThePageAnnouncementButtonsWithBelowAttributes(DataTable table) {
             List<Map<String,String>> buttonConfigs= table.asMaps();
             announcementActions.configureLongComponent(buttonConfigs);
             referenceSiteActions.clickOnDoneButton();
    }

   @Then("she validates published long component as configured")
    public void sheValidatesPublishedLongComponentAsConfigured() {

        assertThat(announcementActions.validateIcons()).isNotNull();
        assertThat(announcementActions.isPublishedTextPresent()).isNotNull();
        assertThat(announcementActions.isPublishedLinkPresent()).isTrue();

        assertThat(announcementActions.validateButtonSize()).isTrue();
        assertThat(announcementActions.validateTab()).isTrue();
        if(componentDetails.get("icon").toLowerCase().contains("enabled")) {
            assertThat(announcementActions.validateCloseIcon()).isFalse();
        }
    }
}
