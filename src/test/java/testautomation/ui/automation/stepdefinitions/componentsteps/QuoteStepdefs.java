package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.QuoteActions;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;

public class QuoteStepdefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public QuoteActions quoteActions;



    @And("she configures the {string} component with default image {string} {string} {string}")
    public void sheConfiguresTheComponentWithDefaultImage(String componentName, String quoteText, String quoteAuthorName, String quotePositionTitle) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        quoteActions.editQuoteComponent(quoteText,quoteAuthorName,quotePositionTitle);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the same image with {string} {string} {string}")
    public void sheShouldSeeTheSameImageWith(String quoteText, String quoteAuthorName, String quotePositionTitle) {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(quoteActions.publishedQuoteText()).isTrue();
        softly.assertThat(quoteActions.publishedQuoteAuthorName()).isTrue();
        softly.assertThat(quoteActions.publishedQuotePositionTitle()).isTrue();
        softly.assertThat(quoteActions.publishedQuoteIcon()).isTrue();
        softly.assertThat(quoteActions.publishedQuoteImage()).isTrue();
        softly.assertAll();

    }


}
