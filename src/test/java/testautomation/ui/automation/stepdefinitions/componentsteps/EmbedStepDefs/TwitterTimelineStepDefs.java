package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.TwitterTimelineActions;

public class TwitterTimelineStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TwitterTimelineActions twitterTimelineActions;

    public static final String htmlTwitterTimeline = "<a class=\"twitter-timeline\" href=\"https://twitter.com/UNSW?ref_src=twsrc%5Etfw\">" +
            "Tweets by UNSW</a> <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>";

    @And("she configures the {string} component with Twitter Timeline option")
    public void sheConfiguresTheComponentWithTwitterTimelineOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        twitterTimelineActions.selectTwittertimelineDropdown();
        twitterTimelineActions.editTwitterTimelineOption(htmlTwitterTimeline);
        referenceSiteActions.clickOnDoneButton();

    }
    @Then("she should see the twitter timeline in the published page")
    public void sheShouldSeeTheTwitterTimelineInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(twitterTimelineActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(twitterTimelineActions.publishedTwitterTimelineOption()).isTrue();

        softly.assertAll();
    }
}
