package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.SoundcloudActions;

public class SoundcloudStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public SoundcloudActions soundcloudActions;

    public static final String soundURL = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/351335497&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true";
    public static final String bandLinkTitle ="Baby Shark";
    public static final String bandLink ="https://soundcloud.com/user-128019510/baby-shark";
    public static final String songLink = "Baby shark";
    public static final String songType = "Children Rhymes";

    @And("she configures the {string} component with Soundcloud option")
    public void sheConfiguresTheComponentWithSoundcloudOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        soundcloudActions.selectSoundCloudDropDown();
        soundcloudActions.editSoundCloudOption(soundURL,bandLinkTitle,bandLink,songLink,songType);
        referenceSiteActions.clickOnDoneButton();
    }
    @Then("she should see the Soundcloud in the published page")
    public void sheShouldSeeTheSoundcloudInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(soundcloudActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(soundcloudActions.publishedSoundCloudFrame()).isTrue();

        softly.assertAll();
    }
}
