package testautomation.ui.automation.stepdefinitions.componentsteps.TeaserCardSTepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.TeaserCard.ProfileTeaserActions;

import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class ProfileTeaserCardStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public ProfileTeaserActions profileTeaserActions;

    Map<String,String> profileTeaserCardInput = Map.of("profilePage","TesterProfilePage",
            "profileFirstName","TestFirstName ",
            "profileLastName","Overwrites",
            "profileTitle","Title Overriden",
            "profileDescription","This is the Profile Description",
            "profileLinkText","Know More",
            "profileLinkToNavigate","https://www.unsw.edu.au");

    //K1"profilePage",v1"content/unsw-sites/au/en/staff/abhirup-das


    @And("she configures the {string} component with its Profile properties")
    public void sheConfiguresTheComponentWithItsProfileProperties(String componentName) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        profileTeaserActions.editProfileTeaserCard(profileTeaserCardInput);
        profileTeaserActions.addProfileTeaserCardImage();
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she validates the teaser card with its Profile properties")
    public void sheValidatesTheTeaserCardWithItsProfileProperties() {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(profileTeaserActions.publishedProfileTeaserComponent()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfileImg()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfileTitle()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfileNameCard()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfileCardDesc()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfileLnk()).isTrue();
        softly.assertThat(profileTeaserActions.publishedProfilePgeLink()).isTrue();
    }
}
