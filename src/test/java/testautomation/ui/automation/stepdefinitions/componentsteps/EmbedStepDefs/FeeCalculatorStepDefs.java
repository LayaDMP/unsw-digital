package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.FeeCalculatorActions;

public class FeeCalculatorStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public FeeCalculatorActions feeCalculatorActions;

    public static final String feeURL = "https://staging.indicativefees.unsw.edu.au/embed-fees-calculator";
    public static final String feeTitle = "Fee Calculator Title";

    @And("she configures the {string} component with Fee Calculator option")
    public void sheConfiguresTheComponentWithFeeCalculatorOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        feeCalculatorActions.selectFeeCalculatorDropDown();
        feeCalculatorActions.editFeeCalculatorOption(feeURL,feeTitle);
        referenceSiteActions.clickOnDoneButton();
    }
    @Then("she should see the Fee Calculator in the published page")
    public void sheShouldSeeTheFeeCalculatorInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(feeCalculatorActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(feeCalculatorActions.publishedFeeCalculatorFrame()).isTrue();

        softly.assertAll();
    }



}
