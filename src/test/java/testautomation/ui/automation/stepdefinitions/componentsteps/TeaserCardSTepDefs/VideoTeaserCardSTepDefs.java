package testautomation.ui.automation.stepdefinitions.componentsteps.TeaserCardSTepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.TeaserCard.VideoTeaserActions;

import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class VideoTeaserCardSTepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public VideoTeaserActions videoTeaserActions;

    Map<String,String> videoTeaserCardInput = Map.of("videoID","HooIs8Ewc2k",
            "videoTitle","UNSW Life",
            "videoHeading","Student Life - UNSW",
            "videoDesc","Description - Automation Testing..",
            "videoImage","/content/dam/images/photos/automation/unique.jpg",
            "videoLink","https://www.unsw.edu.au",
            "videoTranscript","Transcript goes here",
            "videoLength","5:00",
            "vDate",today.toString())  ;

    @And("she configures the {string} component with its Video properties")
    public void sheConfiguresTheComponentWithItsVideoProperties(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        videoTeaserActions.editVideoTeaserCard(videoTeaserCardInput);
        videoTeaserActions.tagSelection();
        referenceSiteActions.clickOnDoneButton();

    }

    @Then("she validates the teaser card with its Video properties")
    public void sheValidatesTheTeaserCardWithItsVideoProperties() {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(videoTeaserActions.publishVideoTeaserCard()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserLink()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserHeading()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserDate()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserDesc()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserTopic()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserTranscript()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserTag()).isTrue();
        softly.assertThat(videoTeaserActions.publishVideoTeaserVideo()).isTrue();

    }
}
