package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.RecentStoriesActions;


import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class RecentStoriesStepDefs {


    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;

    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public RecentStoriesActions recentStoriesActions;

    String title="Title_RecentStories";
    String standFirst="Standfirst_RecentStories";
    String altText ="ALtText_RecentStories";
    String publishedDate = today.format(DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy"));



    @And("she configures the {string} component with {string} {string}")
    public void sheConfiguresTheComponentWith(String keyword, String selection, String jsonFeed) {

            recentStoriesActions.chooseSelection(selection, title, standFirst, IMAGENAMETOUSE, altText,  publishedDate, keyword, jsonFeed);
    }

    @Then("she should validate the recent story component with {string} {string}")
    public void sheShouldValidateTheRecentStoryComponentWith(String selection, String jsonFeed) {
        if (selection.equals("Default")) {
            if (jsonFeed.equals("Alumini feed")) {
                assertThat(recentStoriesActions.verifyAlumnifeedPublishedpage()).isTrue();
            } else if (jsonFeed.equals("Newsroom feed")) {
                assertThat(recentStoriesActions.verifyNewsroomfeedPublishedpage()).isTrue();
            }
        }

        else if (selection.equals("Manual Selection"))
        {
            assertThat(recentStoriesActions.validateTitle(title)).doesNotContain(false);
            assertThat(recentStoriesActions.validateDate(publishedDate)).doesNotContain(false);
            assertThat(recentStoriesActions.validateImage()).contains(true);
        }


    }
}




