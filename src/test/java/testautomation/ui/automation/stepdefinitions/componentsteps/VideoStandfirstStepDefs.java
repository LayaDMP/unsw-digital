package testautomation.ui.automation.stepdefinitions.componentsteps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.VideoPlayerStandfirstActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.*;


public class VideoStandfirstStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public VideoPlayerStandfirstActions videoPlayerActions;

    String title="VideoplayerStandfirst_Title";
    String standFirst="VideoplayerStandfirst__Standfirst";
    String buttonLabel="VideoplayerStandfirst_Buttonlabel";
    String minimumLinkURL="VideoplayerStandfirst_Minimumlinklabel";
    String transcriptTitle="VideoplayerStandfirst__Transripttitle";
    String transcriptOfVideo="It is a long established fact that ";




    @And("she configures the video player standfirst component with {string}")
    public void sheConfiguresTheVideoPlayerStandfirstComponent(String showTranscript) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        videoPlayerActions.clickOnVideoplayerStandfirstComponentAdded();
        referenceSiteActions.clickOnConfigureButton();
        videoPlayerActions.addVideoStandfirstTitle(title);
        videoPlayerActions.addVideoStandfirstStandfirst(standFirst);
        videoPlayerActions.addVideoStandfirstButtonlabel(buttonLabel);
        videoPlayerActions.addVideoStandfirstButtonURL(GENERIC_EXTERNAL_LINK);
        videoPlayerActions.addVideoStandfirstMinimumLinkLabel(minimumLinkURL);
        videoPlayerActions.addVideoStandfirstMinimumLinkURL(GENERIC_EXTERNAL_LINK);
        videoPlayerActions.addVideoplayerStandfirstYoutubeId(YOUTUBE_ID);
        videoPlayerActions.addVideoStandfirstShowVideoTranscript(showTranscript);
        videoPlayerActions.addVideoStandfirstTrasnscriptTitle(transcriptTitle);
        videoPlayerActions.addVideoStandfirstTrasnscriptOfTheVideo(transcriptOfVideo);
        referenceSiteActions.dragAndDropImageToComponent();
        referenceSiteActions.clickOnDoneButton();

    }

    @Then("she should validate the video player standfirst component with {string}")
    public void sheShouldValidateTheVideoPlayerStandfirstComponentWith(String showTranscript) {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstTitlePublishedPage()).isEqualToIgnoringCase(title);
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstStandfirstPublishedPage()).isEqualToIgnoringCase(standFirst);
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstButtonlabelPublishedPage()).isEqualToIgnoringCase(buttonLabel);
        softly.assertThat( videoPlayerActions.verifyVideoStandfirstButtonURLPublishedPage()).containsIgnoringCase(GENERIC_EXTERNAL_LINK);
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstMinimumLinkLabelPublishedPage()).containsIgnoringCase(minimumLinkURL);
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstMinimumLinkURLPublishedPage()).containsIgnoringCase(GENERIC_EXTERNAL_LINK);
        softly.assertThat(videoPlayerActions.verifyVideoStandfirstYoutubeIdPublishedPage()).containsIgnoringCase(YOUTUBE_ID);
        if(showTranscript.toLowerCase().contains("yes")) {
            softly.assertThat(videoPlayerActions.verifyVideoStandfirstShowVideoTranscriptPublished()).isEqualToIgnoringCase(transcriptTitle);
            softly.assertThat(videoPlayerActions.verifyVideoStandfirstTrasnscriptOfTheVideoPubpishedPage()).isEqualToIgnoringCase(transcriptOfVideo);
        }
        softly.assertAll();
    }
}
