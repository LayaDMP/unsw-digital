package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.commons.lang3.RandomStringUtils;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.MosaicActions;
import static org.assertj.core.api.Assertions.*;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MosaicStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TemplatesActions templatesActions;
    @Steps
    public MosaicActions mosaicActions;
    Map<String,String> pageDetails=new HashMap<>();
    Map<String,String> mosaicDetails=new HashMap<>();

    int defaultTileNo=6;
    String defaultTitle="Mosaic Title";
    String defaultLabel ="Mosaic label";
    String defaultSubTitle="Mosaic subTitle";
    String longTestimonialQuote;
    String longTestimonialAuthorName;
    String longTestimonialAuthorRole;


    @And("^she configures the Mosaic component with default configuration$")
    public void sheConfiguresTheMosaicComponentWithImage(){

        mosaicDetails.put("noOfTiles", String.valueOf(defaultTileNo));
        mosaicDetails.put("path", GENERIC_EXTERNAL_LINK);
        mosaicDetails.put("title",defaultTitle);
        mosaicDetails.put("linkLabelTxt", defaultLabel);
        mosaicDetails.put("subTitle",defaultSubTitle);
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        referenceSiteActions.clickOnAddedComponent("Mosaic");
        referenceSiteActions.clickOnConfigureButton();
        mosaicActions.setDefaultMosaicTile(mosaicDetails);
        referenceSiteActions.clickOnDoneButton();
    }

    @And("^she publishes mosaic component page$")
    public void shePublishesMosaicComponentPage() {
        mosaicActions.publishMosaicPage();
    }


    @When("^she adds the component with layout \"([^\"]*)\" and with \"([^\"]*)\"$")
    @And("^she adds the mosaic component with layout \"([^\"]*)\" and with \"([^\"]*)\"$")
    public void sheAddsTheComponentWithLayoutAndWith(String column_layout, String keyword) throws Throwable {
        String templateName = "Component" + RandomStringUtils.randomAlphanumeric(3);
        templatesActions.clickOnTemplateImage(templateName);
        navigationPage.editThePage();
        navigationPage.switchToNewTab();
        referenceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);
        referenceSiteActions.clickOnPlusButton();
        referenceSiteActions.enterKeyword(keyword);
        referenceSiteActions.addComponentFromDropdown();
    }



    @When("she adds the mosaic component with layout {string} {string} {string} {string}")
    public void sheAddsTheMosaicComponentWithLayout(String templateType, String templateName, String column_layout, String keyword) {

        templatesActions.clickOnTemplateImage(templateName);
        navigationPage.editThePage();
        navigationPage.switchToNewTab();
        referenceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);
        referenceSiteActions.clickOnPlusButton();
        referenceSiteActions.enterKeyword(keyword);
        referenceSiteActions.addComponentFromDropdown();
    }

    @And("^she sets mosaic title text with more than (\\d+) character$")
    public void sheSetsMosaicTitleTextWithMoreThanCharacter(int count) {
        defaultTitle=RandomStringUtils.randomAlphanumeric(count);
        mosaicDetails.put("title",defaultTitle);
    }

    @And("^she sets mosaic Link copy text with more than (\\d+) characters$")
    public void sheSetsMosaicLinkCopyTextWithMoreThanCharacters(int count) {
        defaultLabel=RandomStringUtils.randomAlphanumeric(count);
        mosaicDetails.put("linkLabelTxt",defaultLabel);
    }

    @And("^she sets mosaic with more than (\\d+) characters in subtitle$")
    public void sheSetsMosaicWithMoreThanCharactersInSubtitle(int count) {
        defaultSubTitle=RandomStringUtils.randomAlphanumeric(count);
        mosaicDetails.put("subTitle",defaultSubTitle);
    }

    @And("^she completes mosaic tile configuration$")
    public void sheCompletesMosaicTileConfiguration() {
        mosaicActions.setDefaultMosaicTile(mosaicDetails);
        referenceSiteActions.clickOnDoneButton();
    }

    @And("^she sets sets the sidebar with image to drag and drop$")
    public void sheSetsSetsTheSidebarWithImageToDragAndDrop() {
        mosaicActions.clickOnMosaicComponentAdded();
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        referenceSiteActions.clickOnConfigureButton();

    }

    @And("^she sets number of tiles as ([^\"]*)$")
    public void sheSetsNumberOfTilesAs(int count) {
        mosaicDetails.put("noOfTiles",String.valueOf(count));
    }

    @Then("^she validates the mosaic details displayed in published page$")
    public void sheValidatesTheMosaicDetailsDisplayedInPublishedPage() {
        int subtitle=mosaicActions.getNoOfMosaicSubTitles();
        List<String> mosaicUrl=mosaicActions.getMosaicURLs();
        assertThat(mosaicActions.getNoOfMosaicTile()).isEqualTo(defaultTileNo);
        mosaicActions.mosaicTitle().stream().forEach(s->s.startsWith(defaultTitle));
        assertThat(subtitle).isEqualTo(defaultTileNo-1);
        assertThat(GENERIC_EXTERNAL_LINK).isIn(mosaicUrl);
        assertThat(mosaicActions.mosaicAltText()).size().isNotNull();
    }

}
