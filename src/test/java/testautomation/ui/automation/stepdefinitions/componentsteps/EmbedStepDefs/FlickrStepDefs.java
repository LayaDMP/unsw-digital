package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.FlickrActions;

public class FlickrStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public FlickrActions flickrActions;

    public static final String flickrURL = "https://www.flickr.com/photos/125877475@N06/51260171020/in/explore-2021-06-20/";
    public static final String flickrTitle = "Flickr Title";
    public static final String flickrImage = "https://live.staticflickr.com/65535/51260171020_128f471a7b_6k.jpg";

    @And("she configures the {string} component with Flickr option")
    public void sheConfiguresTheComponentWithFlickrOption(String componentName ) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        flickrActions.selectFlickrDropDown();
        flickrActions.editFlickrOption(flickrURL,flickrTitle,flickrImage);
        referenceSiteActions.clickOnDoneButton();

    }
    @Then("she should see the flickr image in the published page")
    public void sheShouldSeeTheFlickrImageInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(flickrActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(flickrActions.publishedFlickrFrame()).isTrue();

        softly.assertAll();
    }



}
