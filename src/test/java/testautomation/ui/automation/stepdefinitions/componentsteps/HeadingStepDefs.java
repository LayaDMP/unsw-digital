package testautomation.ui.automation.stepdefinitions.componentsteps;

import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;

import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.HeadingActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;


public class HeadingStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;

    @Steps
    public ReferenceSiteActions refereceSiteActions;

    @Steps
    public HeadingActions headingActions;


    @And("she configures the Heading component {string} {string} {string}")
    public void sheConfiguresTheHeadingComponent(String headingTitle,String headingSize,String headingAlignment) {
        headingActions.clickOnAddedHeadingComponent();
        refereceSiteActions.clickOnConfigureButton();
        headingActions.editHeadingComponent(headingTitle, headingSize, headingAlignment);
        refereceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the same heading on the published page {string} {string} {string}")
    public void sheShouldSeeTheSameHeadingOnThePublishedPages(String headingText, String headingAlignment, String headingLevel) {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(headingActions.headingText(headingLevel)).isEqualTo(headingText);
        softly.assertThat(headingActions.headinglevel(headingLevel)).isTrue();
        softly.assertThat(headingActions.headingAlignment(headingLevel)).
                isEqualToIgnoringWhitespace(headingAlignment);
        softly.assertAll();
    }
}
