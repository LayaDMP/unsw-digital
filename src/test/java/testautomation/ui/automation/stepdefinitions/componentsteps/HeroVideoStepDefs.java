package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.HeroVideoActions;
import static org.assertj.core.api.Assertions.*;


import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.*;


public class HeroVideoStepDefs {

        @Steps
        public NavigationActions navigationPage;
        @Steps
        public ReferenceSiteActions referenceSiteActions;
        @Steps
        public TemplatesActions templatesActions;
        @Steps
        public HeroVideoActions heroVideoActions;

        Map<String,String> herovideoDetails=new HashMap<>();


        @And("she configures hero video component with default setting")
        public void sheConfiguresHeroVideoComponent() {
         String currentDate=today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
         herovideoDetails.put("title","HeroTitle");
         herovideoDetails.put("subTitle","HeroSubtitle");
         herovideoDetails.put("authorName","HeroAuthor");
         herovideoDetails.put("publishedDate",currentDate);
         herovideoDetails.put("youTubeId", YOUTUBE_ID);
         heroVideoActions.selectHeroVideoSettings();
         heroVideoActions.setHeroVideoDefaultContent(herovideoDetails);
         referenceSiteActions.clickOnDoneButton();
        }

        @Then("she validates the Title,subtitle,author name,published date and youtube video has been published")
        public void sheValidatesPublishedVideoComponent() {
                String publishedCurrentDate=today.format(DateTimeFormatter.ofPattern("d MMMM, yyyy"));
                assertThat(heroVideoActions.getPublishedTitle( herovideoDetails.get("title"))).isTrue();
                assertThat(herovideoDetails.get("subTitle")).isEqualToIgnoringCase(heroVideoActions.getPublishedSubTitle());
                assertThat(herovideoDetails.get("authorName")).isEqualToIgnoringCase(heroVideoActions.getPublishedAuthorName());
                assertThat(publishedCurrentDate).isEqualToIgnoringCase(heroVideoActions.getPublishedDate().trim());
                assertThat(heroVideoActions.getPublishedYouTubeId(YOUTUBE_ID)).isTrue();
        }
}

