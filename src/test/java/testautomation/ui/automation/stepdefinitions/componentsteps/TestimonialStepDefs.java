package testautomation.ui.automation.stepdefinitions.componentsteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.TestimonialActions;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;

public class TestimonialStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    TestimonialActions testimonialActions;

    String quotecopy="This is test quote";
    String name="Tracy";
    String role="Tester";

    @And("she configures the {string} testimonial component {string} image")
    public void sheConfiguresTestimonialComponentImage(String component, String image) {
        boolean imageuse=!image.toLowerCase().endsWith("out");
        if(imageuse) {
            navigationPage.refreshPageURL();
            referenceSiteActions.clickOnTogglePanel();
            referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        }
        testimonialActions.clickOnTestimonialAddedOnPage();
        referenceSiteActions.clickOnConfigureButton();
        testimonialActions.addQuotecopy(quotecopy);
        testimonialActions.addName(name);
        testimonialActions.addRole(role);
        if(imageuse) {
            referenceSiteActions.dragAndDropImageToComponent();
        }
        testimonialActions.clickOnDoneButton();
    }

    @Then("she validates the {string} testimonial {string} image configured values")
    public void sheValidatesTestimonialImageConfiguredValues(String component,String image) {
        SoftAssertions softly =new SoftAssertions();
        softly.assertThat(testimonialActions.publishedAuthorName()).isEqualToIgnoringWhitespace(name);
        softly.assertThat(testimonialActions.publishedQuote()).isEqualToIgnoringWhitespace(quotecopy);
        softly.assertThat(testimonialActions.publishedAuthorRole()).isEqualToIgnoringWhitespace(role);
        if(!image.toLowerCase().endsWith("out"))
            softly.assertThat(testimonialActions.publishedImage()).containsIgnoringCase(IMAGENAMETOUSE);
        softly.assertAll();
    }
}
