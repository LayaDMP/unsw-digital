package testautomation.ui.automation.stepdefinitions.componentsteps;

import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ImageActions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;

public class ImageStepDefs {

    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public ImageActions imageActions;

    String altText="Sand and surf description";
    String caption="Sand and surf";


    @And("^she crops the image in Portrait mode \"([^\"]*)\"$")
    public void sheCropsTheImageInPortraitMode(String CropMode) {
        imageActions.cropImage(CropMode);

    }

    @Then("she should see the Image in the Image Component {string}")
    public void sheShouldSeeTheImageInTheImageComponent(String Image) {

        assertThat(imageActions.verifyImageOnPublishedPage(Image)).isTrue();
    }

    @And("she configures image component with default image and {string} {string}")
    public void sheAddsAnImageToTheTemplateWith(String decorative, String link) {
        referenceSiteActions.clickOnTogglePanel();
        referenceSiteActions.searchForImage(IMAGENAMETOUSE);
        imageActions.clickOnConfigure();
        referenceSiteActions.dragAndDropImageToComponent();
        imageActions.clickOnMetadataTab();
        if (decorative.toLowerCase().startsWith("y")) {
            imageActions.clickOnImageIsDecorativeCheckbox();
            referenceSiteActions.clickOnDoneButton();
        } else if (decorative.toLowerCase().startsWith("n")) {
            imageActions.verifyAltTextImageComponent(altText);
           if (link.toLowerCase().startsWith("dex")){
                imageActions.clickOnOpenSelectionDialogImageComponent();
                referenceSiteActions.clickOnDexReferenceLink();
                referenceSiteActions.clickOnSelectButtonForPopupwindow();
            }else{
               imageActions.addExternalLinkImageComponent(GENERIC_EXTERNAL_LINK);
           }
            imageActions.verifyCaptionImageComponent(caption);
            referenceSiteActions.clickOnDoneButton();
        }

    }

    @And("she should see the Metadata on the published page {string} {string}")
    public void sheShouldSeeTheMetadataOnThePublishedPage(String decorative, String link) {


        if (decorative.toLowerCase().startsWith("n")) {
            assertThat(imageActions.verfyAltTextPublishedPage(altText)).isTrue();
            assertThat(imageActions.verifyCaptionPublishedPage(caption)).isTrue();
            assertThat(imageActions.clickOnImagePublishedSite(caption)).isTrue();
            assertThat(imageActions.verifyLinkPublishedPage(link)).isTrue();
        }
        else if (decorative.toLowerCase().startsWith("y")){
            assertThat(imageActions.verifyCaptionPublishedPage(caption)).isTrue();
        }
    }
}

