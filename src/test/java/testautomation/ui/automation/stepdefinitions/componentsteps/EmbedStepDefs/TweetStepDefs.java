package testautomation.ui.automation.stepdefinitions.componentsteps.EmbedStepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.EmbedActions.TweetActions;

public class TweetStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public TweetActions tweetActions;
    public static final String htmlTweet = "<blockquote class=\"twitter-tweet\"><p lang=\"en\" dir=\"ltr\">" +
            "For nearly four decades, researchers from <a href=\"https://twitter.com/UNSWScience?ref_src=twsrc%5Etfw\">@UNSWScience</a>" +
            ", with scientists from the NSW Department of Planning, Industry and Environment (<a href=\"https://twitter.com/NSWDPIE?ref_src=twsrc%5Etfw\">@nswdpie</a>)" +
            " and other state agencies, have been surveying Australia’s waterbirds once a year.<a href=\"https://t.co/iNiCuCIrUy\">https://t.co/iNiCuCIrUy</a>" +
            " <a href=\"https://t.co/ZoT6tecSWa\">pic.twitter.com/ZoT6tecSWa</a></p>&mdash; UNSW (@UNSW)" +
            " <a href=\"https://twitter.com/UNSW/status/1451043065288818691?ref_src=twsrc%5Etfw\">October 21, 2021</a></blockquote>" +
            " <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>";


    @And("she configures the {string} component with Tweet option")
    public void sheConfiguresTheComponentWithTweetOption(String componentName) {
        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        tweetActions.selectTweetDropdown();
        tweetActions.editTweetOption(htmlTweet);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she should see the tweet in the published page")
    public void sheShouldSeeTheTweetInThePublishedPage() {
        SoftAssertions softly=new SoftAssertions();

        softly.assertThat(tweetActions.publishedEmbedGrid()).isTrue();
        softly.assertThat(tweetActions.publishedTweetOption()).isTrue();

        softly.assertAll();
    }



}
