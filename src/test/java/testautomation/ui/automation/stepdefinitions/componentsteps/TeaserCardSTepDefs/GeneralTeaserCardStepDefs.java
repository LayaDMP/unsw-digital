package testautomation.ui.automation.stepdefinitions.componentsteps.TeaserCardSTepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.GeneralCardActions;
import testautomation.ui.automation.actions.componentactions.TeaserCard.GeneralTeaserActions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;

public class GeneralTeaserCardStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public GeneralTeaserActions generalTeaserActions;

    Map<String,String> generalTeaserCardInput = Map.of("generalTeaserHeading","General Teaser Card Heading",
            "generalTeaserImage","/content/dam/images/photos/automation/unique3.jpg",
            "generalTeaserStandfirstText","Standfirst Description",
           "generalTeaserCategoryText","Category Text",
            "generalTeaserLinkText","https://www.education.unsw.edu.au/",
            "generalNewsDate",today.toString())  ;

    @And("she configures the {string} component with its properties")
    public void sheConfiguresTheComponentWithItsProperties(String componentName) {

        referenceSiteActions.clickOnAddedComponent(componentName);
        referenceSiteActions.clickOnConfigureButton();
        generalTeaserActions.editGeneralTeaserCard(generalTeaserCardInput);
        referenceSiteActions.clickOnDoneButton();
    }

    @Then("she validates the teaser card with its properties")
    public void sheValidatesTheTeaserCardWithItsProperties() {
        SoftAssertions softly=new SoftAssertions();
        softly.assertThat(generalTeaserActions.publishedGeneralTeaserComponent()).isTrue();
        softly.assertThat(generalTeaserActions.publishedGeneralTeaserImage()).isTrue();
        softly.assertThat(generalTeaserActions.publishedGeneralTeaserCategory()).isTrue();
        softly.assertThat(generalTeaserActions.publishedGeneralTeaserStandfirst()).isTrue();
        softly.assertThat(generalTeaserActions.publishedGeneralTeaserDate()).isTrue();
    }



}
