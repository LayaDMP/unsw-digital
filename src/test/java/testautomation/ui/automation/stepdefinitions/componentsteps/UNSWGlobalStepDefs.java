package testautomation.ui.automation.stepdefinitions.componentsteps;

import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.actions.commonactions.TemplatesActions;
import testautomation.ui.automation.actions.componentactions.ExperienceFragmentActions;

import testautomation.ui.automation.actions.componentactions.UNSWGlobalHeaderActions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class UNSWGlobalStepDefs {
    @Steps
    public NavigationActions navigationPage;
    @Steps
    public TemplatesActions templateActions;
    @Steps
    public ReferenceSiteActions referenceSiteActions;
    @Steps
    public ExperienceFragmentActions experienceFragmentActions;
    @Steps
    public UNSWGlobalHeaderActions unswGlobalHeaderActions;

    @When("^she adds an Experience Fragment to the template \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheAddsAnExperienceFragmentToTheTemplate(String templateName, String column_layout) {
        //        Click on the Page under Test Automation folder
       templateActions.clickOnTemplateImage(templateName);

//        Click on Edit button to add components
       navigationPage.editThePage();

//        Since the page gets opened in a new tab, focus the browser to the new tab
        navigationPage.switchToNewTab();

//        select the column layout - eg fullwidth / left / right
        referenceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);

//        Click on the "+" button
        referenceSiteActions.clickOnPlusButton();


    }


    @When("^she configures an Experience Fragment to the template \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheConfiguresAnExperienceFragmentToTheTemplate(String templateName, String column_layout, String experienceFragmentName, String experienceFragmentTitle) {
        //        Click on the Page under Test Automation folder
       templateActions.clickOnTemplateImage(templateName);

//        Click on Edit button to add components
       navigationPage.editThePage();

//        Since the page gets opened in a new tab, focus the browser to the new tab
        navigationPage.switchToNewTab();

//        select the column layout - eg fullwidth / left / right
        referenceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);

        referenceSiteActions.clickOnConfigureButton();

        experienceFragmentActions.add_link_to_ExperienceFragmentPage(experienceFragmentName, experienceFragmentTitle);

        referenceSiteActions.clickOnDoneButton();


    }

    @And("^she verifies the Global Header sections and sub sections \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void sheVerifiesTheGlobalHeaderSectionsAndSubSections(String SectionTitile1, String SectionTitile2, String SectionTitile3, String SectionTitileLink, String SubSection_SectionTitile1, String SubSection_SectionTitile2, String SubSection_SectionTitile3, String SubSection_SectionTitile4, String SubSection_SectionTitile5, String SubSection_SectionTitile6, String SubSection_SectionTitile7, String SubSection_SectionTitile8, String SubSection_SectionTitile9) {
        unswGlobalHeaderActions.verify_GlobalHeaderSection_onPublishedPage(SectionTitile1, SectionTitileLink);
        unswGlobalHeaderActions.verify_GlobalHeaderSubSections_onPublishedPage(SubSection_SectionTitile1, SubSection_SectionTitile2, SubSection_SectionTitile3, SectionTitileLink);

        unswGlobalHeaderActions.verify_GlobalHeaderSection_onPublishedPage(SectionTitile2, SectionTitileLink);
        unswGlobalHeaderActions.verify_GlobalHeaderSubSections_onPublishedPage(SubSection_SectionTitile4, SubSection_SectionTitile5, SubSection_SectionTitile6, SectionTitileLink);

        unswGlobalHeaderActions.verify_GlobalHeaderSection_onPublishedPage(SectionTitile3, SectionTitileLink);
        unswGlobalHeaderActions.verify_GlobalHeaderSubSections_onPublishedPage(SubSection_SectionTitile7, SubSection_SectionTitile8, SubSection_SectionTitile9, SectionTitileLink);

    }

    @And("^she clicks on subsection \"([^\"]*)\" of section\"([^\"]*)\" and gets navigated to \"([^\"]*)\"$")
    public void sheClicksOnSubsectionOfSectionAndGetsNavigatedTo(String SubSection_SectionTitile1, String SectionTitile1, String SectionTitileLink) {
        unswGlobalHeaderActions.verify_GlobalHeaderSection_onPublishedPage(SectionTitile1, SectionTitileLink);
        unswGlobalHeaderActions.click_on_GlobalHeaderSubSection_onPublishedPage(SubSection_SectionTitile1, SectionTitileLink);
        referenceSiteActions.verifyNavigatedURLFor(SectionTitileLink);

    }
}
