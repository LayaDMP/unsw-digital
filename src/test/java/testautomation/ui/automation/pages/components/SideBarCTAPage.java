package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class SideBarCTAPage extends PageObject {

    @FindBy(css = "input[name='./title']")
    public WebElementFacade sidebarTitle;

    @FindBy(css = "input[name='./subtitle']")
    public WebElementFacade sidebarSubTitle;

    @FindBy(css = "input[name='./buttonLabel']")
    public WebElementFacade sidebarButtonLabel;

    @FindBy(xpath = "//foundation-autocomplete[@name='./linkUrl']//input[@role='combobox']")
    public WebElementFacade sidebarLinkURL;

    @FindBy(xpath = "//coral-select[@name='./target']//button[@type='button']")
    public WebElementFacade sidebarOpenLinkDropDown;

    @FindBy(css = "coral-selectlist-item[value='_blank']")
    public WebElementFacade sidebarSelectDropDownNewPage;

    @FindBy(css=".cta.minimal")
    public WebElementFacade publishedSideBarCTA;

    @FindBy(css="div[class='content'] h3")
    public WebElementFacade publishedSideBarTitle;

    @FindBy(css="div[class='content'] p")
    public WebElementFacade publishedSideBarSubTitle;

    @FindBy(css=".faux-link")
    public WebElementFacade publishedSideBarButton;

    @FindBy(css="a[aria-label='opens in a new window']")
    public WebElementFacade publishedSideBarOpensNewPage;


}
