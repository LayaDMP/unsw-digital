package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;


public class HorizontalScrollPage extends PageObject {

    @FindBy(xpath = "//*[@value ='/apps/unsw-common/components/content/horizontal-scroll-container']")
    public WebElementFacade horizontalScrollComponentDropdown;

    @FindBy(css = "#OverlayWrapper > div:nth-child(6)")
    public WebElementFacade horizontalScrollComponentAdded;

    @FindBy(xpath = "//span[text()='Horizontal Scroll Container']//ancestor::*[contains(@data-path,'/content/unsw-dex-reference/en/test-automation/') and contains(@data-path,'horizontal_scroll')  and @title = 'Horizontal Scroll Container']//div[@data-text='Drag components here']")
    public WebElementFacade AddComponent_HorizontalScroll;

    @FindBy(xpath = "//*[@role='option' and @value='/apps/unsw-common/components/content/cfcomponents/general-cards/v2/general-cards']")
    public WebElementFacade GeneralCardsDropdown_horizontalscroll;

    @FindBy(xpath = "//*[@data-cmp-hook-childreneditor='add' and @type='button']")
    public WebElementFacade addButtonHorizontalScroll;

    @FindBy(xpath = "//button[@class = 'next slick-arrow']")
    public WebElementFacade sliderArrowHorizontalScrollPublishedSite;

    @FindBy(xpath = "//div[@class='slider slick-initialized slick-slider slick-dotted']")
    public WebElementFacade sliderDottedHorizontalScrollPublishedSite;

    @FindBy(xpath = "//span[text() ='Horizontal Scroll Container']//following::div[@data-text = '${value}")
    public WebElementFacade componentUnderHorzontalScrollComponent;

    @FindBy(xpath = "//div[@id='slick-slide0${value}']//p")
    public WebElementFacade publishedTextComponent;

    @FindBy(xpath = "//div[@id='slick-slide0${value}']//img")
    public WebElementFacade publishedImageComponent;

    @FindBy(xpath = "//input[@placeholder='Enter Keyword']//following::coral-selectlist//coral-selectlist-item")
    public WebElementFacade addDropdownComponentHorizontalScroll;





}
