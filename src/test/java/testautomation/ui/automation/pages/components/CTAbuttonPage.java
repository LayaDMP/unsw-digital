package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class CTAbuttonPage extends PageObject {

    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Link *']//following::span[1]")
    public WebElementFacade linkFieldChevron;

    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Link *']//following::input[1]")
    public WebElementFacade linkFieldButtonComponent;

    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Text *']//following::input[1]")
    public WebElementFacade textFieldButtonComponent;

    @FindBy(xpath = "//coral-select[@name='./buttonAlignment']//button")
    public WebElementFacade alignmentDropdownButtonComponent;

    @FindBy(xpath = "//label[contains(text(),'Buttons')]/following-sibling::*//button")
    public WebElementFacade addButtonCTAComponent;

    @FindBy(xpath = "//div[contains(@title,'CTA')]")
    public WebElementFacade CTAbuttonAdded;

    @FindBy(xpath = "//coral-selectlist-item[@value='/apps/unsw-common/components/content/button']")
    public WebElementFacade CTAButtonDropdown;

    @FindBy(xpath = "//label[text()='Button color']//following::coral-select[1]")
    public WebElementFacade buttonColorDropdown;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonColour;

    @FindBy(xpath = "//label[text()='Label style']//following::coral-select[1]")
    public WebElementFacade labelStyleDropdown;

    @FindBy(xpath = "//label[text()='Button Size']/following::coral-select[1]")
    public WebElementFacade buttonSizeDropdown;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonSize;

    @FindBy(xpath = "//coral-select[@name='./buttonAlignment']//coral-selectlist-item[@value='${value}']")
    public WebElementFacade selectButtonAlignment;

    @FindBy(xpath = "//a[@data-click_name]//ancestor::div[contains(@class,'unsw-brand-button-container ${value}')]")
    public WebElementFacade buttonAlignment;

    @FindBy(xpath = "//a[@data-click_name='${value}']//ancestor::div[contains(@class,'unsw-brand-button-container')]")
    public WebElementFacade buttonText;

    @FindBy(xpath = "//div[contains(@class,'unsw-brand-button-container')]//following::a[contains(@class,'${value}')]")
    public WebElementFacade ctaButtonAttribute;


    @FindBy(xpath = "//div[contains(@class,'unsw-brand-button-container')]//following::a[@href='${value}']")
    public WebElementFacade externalLink;

    @FindBy(xpath = "//div[@class='icon']//following::div[@class='text']")
    public WebElementFacade buttonPublishedSite;

    @FindBy(xpath = "//label[text()='Button style']/following::coral-select[1]")
    public WebElementFacade buttonStyleDropdown;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonStyle;

    @FindBy(xpath = "//coral-selectlist-item[@value='text-married']")
    public WebElementFacade layoutMarried;

    @FindBy(xpath = "//coral-selectlist-item[@value='text-married text-left']")
    public WebElementFacade layoutMarriedWithIcon;

    @FindBy(xpath = "//coral-selectlist-item[text()='Divorced']")
    public WebElementFacade layoutDivorced;

    @FindBy(xpath = "//coral-selectlist-item[@value='text-only']")
    public WebElementFacade layoutTextOnly;

    @FindBy(xpath = "//div[contains(@class,'unsw-brand-button-container')]//following::a[contains(@class,'text-married')]")
    public WebElementFacade labelStyleMarried;
    @FindBy(xpath = "//div[contains(@class,'unsw-brand-button-container')]//following::a[contains(@class,'text-left')]")
    public WebElementFacade labelStyleLeft;
    @FindBy(xpath = "//div[contains(@class,'unsw-brand-button-container')]//following::a[contains(@class,'text-only')]")
    public WebElementFacade labelStyleText;

    @FindBy(xpath = "//img[contains(@src,'/content/unsw-dex-reference.thumb.48.48.png')]")
    public WebElementFacade dexReferenceSitePopupButtonComponent;
}
