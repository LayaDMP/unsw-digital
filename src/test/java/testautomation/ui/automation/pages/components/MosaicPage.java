package testautomation.ui.automation.pages.components;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MosaicPage extends PageObject {
    @FindBy(xpath = "//div[@title='Mosaic']")
    public WebElementFacade mosaicTitle;
    @FindBy(xpath = "//input[contains(@name,'tile${value}/tiletitle')]")
    public WebElementFacade mosaicTileTitle;
    @FindBy(xpath = "//input[contains(@name,'tile${value}/tilelinklabel')]")
    public WebElementFacade mosaicTileLinkLabel;
    @FindBy(xpath = "//textarea[contains(@name,'/tile${value}/tilesubtitle')]")
    public WebElementFacade mosaicTileSubTitle;
    @FindBy(xpath = "//input[contains(@name,'tile${value}/fileName')]/preceding-sibling::div")
    public WebElementFacade mosaicTileImageDrop;
    @FindBy(xpath = "//coral-tab-label[contains(.,'Tile ${value}')]")
    public WebElementFacade mosaicTileConfiguration;
    @FindBy(xpath = "//section[contains(@class,'mosaic')]//a")
    public List<WebElementFacade> noOfMosaicTiles;

    @FindBy(xpath = "//section[contains(@class,'mosaic')]//a//h3")
    public List<WebElementFacade> mosaicTitles;

    @FindBy(xpath = "//section[contains(@class,'mosaic')]//a//p")
    public List<WebElementFacade> subTitles;

    @FindBy(className= "replace-background-image hidden")
    public List<WebElement> mosaicImage;

    @FindBy(xpath= "//*[contains(@name,'tile${value}/tilelink')]/div/div/input")
    public WebElementFacade mosaicLinkTile;

    public static final String MosaicComponentAdded_xpath ="//div[@title='Mosaic']";
    public static final String MosaicTileConfiguration_xpath1 ="//coral-tab-label[contains(text(),'Tile ";
    public static final String MosaicTileConfiguration_xpath2="')]";
    public static final String MosaicTitleTile_xpath1 ="//input[contains(@name,'tile";
    public static final String MosaicTitleTile_xpath2 = "/tiletitle')]";
    public static final String MosaicSubTitleTile_xpath1 ="//textarea[contains(@name,'/tile";
    public static final String MosaicSubTitleTile_xpath2 = "/tilesubtitle')]";
    public static final String MosaicDragNDropTile_xpath1 ="//input[contains(@name,'tile";
    public static final String MosaicDragNDropTile_xpath2 = "/fileName')]/preceding-sibling::div";
    public static final String MosaicLinkLabelTile_xpath1 = "//input[contains(@name,'tile";
    public static final String MosaicLinkLabelTile_xpath2 ="/tilelinklabel')]";

    public static final String MosaicTile2Configuration ="//coral-tab-label[contains(text(),'Tile 2')]";
    public static final String MosaicTile3Configuration ="//coral-tab-label[contains(text(),'Tile 3')]";
    public static final String MosaicTile4Configuration = "//coral-tab-label[contains(text(),'Tile 4')]";
    public static final String MosaicTile5Configuration ="//coral-tab-label[contains(text(),'Tile 5')]";
    public static final String MosaicTile6Configuration ="//coral-tab-label[contains(text(),'Tile 6')]";

    public static final String MosaicTitleTile1_xpath ="//input[contains(@name,'tile1/tiletitle')]";
    public static final String MosaicTitleTile2_xpath ="//input[contains(@name,'tile2/tiletitle')]";
    public static final String MosaicTitleTile3_xpath ="//input[contains(@name,'tile3/tiletitle')]";
    public static final String MosaicTitleTile4_xpath ="//input[contains(@name,'tile4/tiletitle')]";
    public static final String MosaicTitleTile5_xpath ="//input[contains(@name,'tile5/tiletitle')]";
    public static final String MosaicTitleTile6_xpath ="//input[contains(@name,'tile6/tiletitle')]";
    public static final String MosaicLinkTile_xpath1 ="//foundation-autocomplete[@name='./tiles/tile";
    public static final String MosaicLinkTile_xpath2 ="/tilelink']/div/div/input";
    public static final String mosaicLinkDialog_xpath1="//button[contains(@value,'";
    public static final String mosaicLinkDialog_xpath2="')]";
    public static final String MosaicDoneButton_css = "button[title='Done']";

}
