package testautomation.ui.automation.pages.components.TeaserCard;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProfileTeaserCardPage extends PageObject {

    @FindBy(xpath = "//coral-select[@name='./variance']//button[@type='button']")
    public WebElementFacade VarianceDropDown;

    @FindBy(xpath = "//coral-select[@placeholder='Select']//button[@type='button']")
    public WebElementFacade profileTeaserTypeDropDown;

    @FindBy(css = "coral-selectlist-item[value='profile']")
    public WebElementFacade profileSelectionDropDown;

    @FindBy(xpath = "//coral-tab-label[normalize-space()='Profile Settings']")
    public WebElementFacade profileSettingTab;

    @FindBy(xpath = "//coral-multifield[@data-granite-coral-multifield-name='./profileCards']//button[@type='button']")
    public WebElementFacade profileAddButton;

    @FindBy(css="a[href='https://www.unsw.edu.au']")
    public WebElementFacade publishedProfileLink;

    @FindBy(css=".card-profile__image-container")
    public WebElementFacade publishedProfileImage;

    @FindBy(css=".card-profile")
    public WebElementFacade publishedProfileCard;

    @FindBy(css=".card-profile__title")
    public WebElementFacade publishedProfileCardTitle;

    @FindBy(css="div[class='card-profile__name'] h3")
    public WebElementFacade publishedProfileCardName;

    @FindBy(css="div[class='card-profile__content'] p")
    public WebElementFacade publishedProfileCardDesc;

    @FindBy(css=".card-profile__view-profile")
    public WebElementFacade publishedProfilePageLink;

    // content/unsw-sites/au/en/staff/abhirup-das

    @FindBy(xpath = "//foundation-autocomplete[contains(@name, './profilePagePath')]//input[@role='combobox']")
    public WebElementFacade profilePageTextbox;

    @FindBy(css = "input[placeholder='eg. First Name']")
    public WebElementFacade profileFirstName;

    @FindBy(css = "input[placeholder='eg. Last Name']")
    public WebElementFacade profileLastName;

    @FindBy(css="input[name='./profileCards/item0/./title']")
    public WebElementFacade profileTitle;

    @FindBy(xpath = "//div[contains(@name, './description')]")
    public WebElementFacade profileDescription;

    @FindBy(css=".cq-FileUpload-thumbnail")
    public WebElementFacade profileImage;

    @FindBy(xpath = "//input[contains(@name,'./linkText')]")
    public WebElementFacade profileLinkText;

    @FindBy(xpath = "//foundation-autocomplete[contains(@name,'./link')]//input[@role='combobox']")
    public WebElementFacade profileLinkCombobox;

    @FindBy(xpath = "//coral-checkbox-label[normalize-space()='Open in a new tab']")
    public WebElementFacade profileOpenInNewTab;










}
