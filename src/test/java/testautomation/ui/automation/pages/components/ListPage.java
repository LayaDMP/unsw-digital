package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class ListPage extends PageObject {

    @FindBy(xpath = "//coral-multifield-item[@aria-posinset='${value}']//input[@name='./options']")
    public WebElementFacade listTextBox;

    @FindBy(xpath = "//button[.='Add']")
    public WebElementFacade addListButton;

    @FindBy(xpath = "//li[contains(text(),'List-')]")
    public List<WebElementFacade> assertList;
}
