package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class HeadingPage extends PageObject {

    @FindBy(xpath = "//*[contains(text(), 'Heading (v2)') and @role]")
    public WebElementFacade headingComponent;

    @FindBy(xpath = "//input[@name='./heading']")
    public WebElementFacade headingtextField;

    @FindBy(xpath = "//*[contains(text(),'Heading') and @handle]")
    public WebElementFacade headingLevelDropdown;


    @FindBy(xpath = "//*[contains(text(),'Left') and @handle]")
    public WebElementFacade alignmentDropdownn;

    @FindBy(xpath = "//*[@title='Heading (v2)']")
    public WebElementFacade headingComponentAdded;

    @FindBy(xpath = "//*[@value = '${value}' and @role]")
    public WebElementFacade headingLevel;

    @FindBy(xpath = "//${value}")
    public WebElementFacade headingText;


}
