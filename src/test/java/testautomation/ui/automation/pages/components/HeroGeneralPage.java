package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HeroGeneralPage extends PageObject {


    @FindBy(xpath = "//coral-tab-label[contains(text(),'Text')]")
    public WebElementFacade textAndContentLink;

    @FindBy(xpath = "//coral-tab-label[contains(text(),'Asset')]")
    public WebElementFacade imageAndAssetLink;

    @FindBy(name = "./lastupdatedlabel")
    public WebElementFacade labelDateTextfield;

    @FindBy(name = "./publishDate")
    public WebElementFacade publishDateCheckbox;

    @FindBy(name = "./showDescription")
    public WebElementFacade descCheckbox;

    @FindBy(name = "./authorName")
    public WebElementFacade authorNameTextField;

    @FindBy(xpath = "//coral-select[@name='./themecolor']//button[@type='button']")
    public WebElementFacade backgroundDropdownBtn;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade backgroundDropdownValue;

    @FindBy(name = "./backgroundimage")
    public WebElementFacade backgroundImage;

    @FindBy(name = "./thumbnailImage")
    public WebElementFacade featuredImage;

    @FindBy(name = "./avatarImage")
    public WebElementFacade avatarImage;

    @FindBy(tagName = "h1")
    public WebElementFacade publishedHeader;

    @FindBy(css = ".author-name")
    public WebElementFacade publishedAuthorName;

    @FindBy(css = "img[class='avatar']")
    public WebElementFacade publishedAuthorImage;

    @FindBy(xpath = "//div[@class='tpl-image ']//img")
    public WebElementFacade publishedFeatureImage;

    @FindBy(xpath = "//div[contains(@class,'background-image')]//img")
    public WebElementFacade publishedBackgroundImage;

    @FindBy(xpath ="//div[contains(@class,'date')]")
    public WebElementFacade publishedDate;









}
