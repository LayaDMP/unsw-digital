package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;


public class RecentStoriesPage extends PageObject {


    @FindBy(xpath = "//foundation-autocomplete[contains(@name,'./recentStories/item${value}')]//input[@role='combobox']")
    public WebElementFacade recentStoriesURLTextBox;

    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Selection']//following::coral-icon[@aria-label='chevron down'][1]//ancestor::button")
    public WebElementFacade recentStoriesSelectionDropdown;


    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Selection']//following::coral-icon[@aria-label='chevron down'][2]//ancestor::button")
    public WebElementFacade RecentStories_JsonfeedDropdown;

    @FindBy(xpath = "//coral-multifield[@data-granite-coral-multifield-name='./recentStories']//button//coral-button-label[text()='Add']")
    public WebElementFacade Addbutton_RecentStories;

    @FindBy(xpath = "//input[@name = './recentStories/item0/title']")
    public WebElementFacade Title_RecentStories;

    @FindBy(xpath = "//input[@name = './recentStories/item0/standfirst']")
    public WebElementFacade Standfirst_RecentStories;

    @FindBy(xpath = "//input[@name = './recentStories/item0/altText']")
    public WebElementFacade AltText_RecentStories;

    @FindBy(xpath = "//input[@name = './recentStories/item${value}/altText']")
    public WebElementFacade altTextRecentStories;

    @FindBy(xpath = "//coral-datepicker[@name='./recentStories/item${value}/publishdate']//input[2][@aria-haspopup]")
    public WebElementFacade publishedDates;

    @FindBy(xpath = "//*[@name='./recentStories/item0/./readmoreURL']//button[@title = 'Open Selection Dialog']")
    public WebElementFacade ReadMoreURL_RecentStories;

    @FindBy(xpath = "//coral-datepicker[@name='./recentStories/item0/publishdate']//input[2][@aria-haspopup]")
    public WebElementFacade PublishedDate_RecentStories;

    @FindBy(xpath = "//div[@class='recent-stories uds-component aem-GridColumn aem-GridColumn--default--12']//div[@class='latest-stories' and @data-feed_url = '/content/dam/unsw/newsroom/aluminifeed.json']")
    public WebElementFacade alumniFeedPubishedPage;

    @FindBy(xpath = "//div[@class='recent-stories uds-component aem-GridColumn aem-GridColumn--default--12']//div[@class='latest-stories' and @data-feed_url = '/content/dam/unsw/newsroom/myfeed.json']")
    public WebElementFacade newsroomFeedPubishedPage;

    @FindBy(xpath = "//a//*[@class='image']")
    public List<WebElementFacade> publishedImage;

    @FindBy(xpath = "//a//*[@class='title']")
    public List<WebElementFacade> publishedTitle;

    @FindBy(xpath = "//a//*[@class='date']")
    public List<WebElementFacade> publishedDate;

    @FindBy(xpath = "//a//*[@class='description']")
    public List<WebElementFacade> publishedDescription;

    @FindBy(xpath = "//span[text()='${value}']//ancestor::div[1]")
    public WebElementFacade recentStoriesComponent;

    @FindBy(xpath = "//coral-selectlist-item[text()='${value}']")
    public WebElementFacade selectionValue;

    @FindBy(xpath = "//coral-selectlist-item[text()='${value}']")
    public WebElementFacade jsonFeedValue;

    @FindBy(xpath = "//input[@name = './recentStories/item${value}/title']")
    public WebElementFacade title;

    @FindBy(xpath = "//input[@name = './recentStories/item${value}/standfirst']")
    public WebElementFacade standFirst;

    @FindBy(xpath = "//*[@name='./recentStories/item0/standfirst']//following::span[text()='Drop an asset here.'][${value}]")
    public WebElementFacade dropHere;











}
