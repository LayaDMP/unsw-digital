package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class TestimonialPage extends PageObject {

    @FindBy(xpath = QuoteCopy_Testimonial_xpath)
    public WebElementFacade QuoteCopy_Testimonial;

    @FindBy(xpath = Name_Testimonial_xpath)
    public WebElementFacade Name_Testimonial;

    @FindBy(xpath = Role_Testimonial_xpath)
    public WebElementFacade Role_Testimonial;

    @FindBy(xpath = shortTestimonialAdded_xpath)
    public WebElementFacade shortTestimonialAdded;

    @FindBy(xpath = "//div[contains(@title,'Testimonial')]")
    public WebElementFacade testimonialAdded;

    @FindBy(xpath = "//q[@class='quote']" )
    public WebElementFacade quotecopy;

    @FindBy(xpath = "//div[@class='author-image']//img" )
    public WebElementFacade imageName;

    @FindBy(xpath = "//p[(contains(@class,'author-name'))]" )
    public WebElementFacade name;

    @FindBy(xpath = "//p[(contains(@class,'author-role lead-text'))]" )
    public WebElementFacade role;

    @FindBy(xpath = "//*[contains(@class,'quote-block')]/descendant::*" )
    public WebElementFacade publishedQuote;

    @FindBy(xpath = "//*[contains(@class,'author-name')]" )
    public WebElementFacade publishedAuthorName;

    @FindBy(xpath = "//*[contains(@class,'author-role')]")
    public WebElementFacade publishedAuthorRole;

    @FindBy(xpath = "//div[contains(@class,'-image')]//img" )
    public WebElementFacade publishedImage;



    public static final String QuoteCopy_Testimonial_xpath = "//textarea[contains(@name, 'quote')]";
    public static final String Name_Testimonial_xpath = "//*[@type='text' and @name ='./name']";
    public static final String Role_Testimonial_xpath = "//*[@type='text' and @name ='./role']";
    public static final String shortTestimonialAdded_xpath = "//div[contains(@title,'Testimonial')]";
    public static final String longTestimonialAdded_xpath = "//div[@title='Long Testimonial']";

}
