package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class KeyDatesPage extends PageObject {


    @FindBy(xpath = "//coral-select[@name='./keydates/item${value}/./readmoreTarget']//coral-selectlist-item[normalize-space()='Same Tab']")
    public WebElementFacade selectSameTabTargetDropdown;

    @FindBy(xpath = "//coral-select[@name='./keydates/item${value}/./readmoreTarget']//coral-selectlist-item[normalize-space()='New Tab']")
    public WebElementFacade selectNewTabTargetDropdown;

    @FindBy(xpath = "//input[@name='./keydates/item${value}/./readmoreTarget']//preceding::coral-overlay[1]//preceding::button[1]")
    public WebElementFacade readMoreTarget;


    @FindBy(xpath = "//input[@name='./keydates/item${value}/keydate']//following::input[1]")
    public WebElementFacade enterDateFieldKeyDates;

    @FindBy(xpath = "//foundation-autocomplete[contains(@name,'./keydates/item${value}')]//input[@role='combobox']")
    public WebElementFacade keyDatesURLTextBox;

    @FindBy(xpath = "//coral-selectlist-item[@value='/apps/unsw-common/components/content/keydates']")
    public WebElementFacade keyDatesDropdown;

    @FindBy(xpath = "//div[@title='Key Dates']")
    public WebElementFacade keyDatesComponentAdded;


    @FindBy(xpath = "//input[@name='./showmoretext']")
    public WebElementFacade showMoreTextFieldKeyDates;

    @FindBy(xpath = "//input[@name='./showlesstext']")
    public WebElementFacade showLessTextFieldKeyDates;

    @FindBy(xpath = "//coral-button-label[text()='Add']//ancestor::button[1]")
    public WebElementFacade addButtonKeyDates;

    @FindBy(xpath = "//input[@name='./keydates/item${value}/subtitle']")
    public WebElementFacade subtitleTextFieldKeyDates;


    @FindBy(xpath = "//input[@name='./keydates/item${value}/title']")
    public WebElementFacade titleTextFieldKeyDates;


    @FindBy(xpath = "//input[@name='./keydates/item${value}/readmoretext']")
    public WebElementFacade readMoreTextFieldKeyDates;

    @FindBy(xpath = "//h3[text()='${value}']")
    public WebElementFacade keyDatesTitle;

    @FindBy(xpath = "//span[normalize-space()='${value}']")
    public WebElementFacade keyDatesSubtitle;

    @FindBy(xpath = "//span[@class='date-block']")
    public List<WebElementFacade> keyDatesDate;

    @FindBy(xpath = "//*[normalize-space()='${value}']/parent::div//a")
    public WebElementFacade keyDatesReadMoreLink; //based on subtitle

    @FindBy(css = ".show-more")
    public WebElementFacade keyDatesShowmore;

    @FindBy(css = ".show-less")
    public WebElementFacade keyDatesShowless;


}
