package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class DegreeHeaderPage extends PageObject {

    @FindBy(xpath = "//div[contains(@title,'Page Header')]")
    public WebElementFacade PageHeaderComponentAdded;


    @FindBy(xpath = "//coral-select[@name='./headerType']")
    public WebElementFacade PageHeaderComponent_Type;

    @FindBy(xpath = "//coral-select[@name='./headerType']")
    public WebElementFacade headerType;

    @FindBy(xpath = "//coral-selectlist-item[text()='${value}']")
    public WebElementFacade pageHeaderComponentType;

    @FindBy(xpath = "//coral-tab-label[text()='${value}']")
    public WebElementFacade pageHeaderSecondTab;

    @FindBy(xpath = "//input[@name='./manual/heading']")
    public WebElementFacade pageHeaderComponentHeading;

    @FindBy(xpath = "//textarea[@name='./manual/summary']")
    public WebElementFacade pageHeaderComponentSummary;

    @FindBy(xpath = "//input[@name='./manual/date']//following::input[1]")
    public WebElementFacade pageHeaderComponentDate;

    @FindBy(xpath = "//div[@class='datetime']")
    public WebElementFacade dateOnPublishedPage;

    @FindBy(xpath = "//coral-datepicker[@name='./manual/date']//button[@title='Calendar']")
    public WebElementFacade PageHeader_DateIcon;

    @FindBy(xpath = "//td[contains(@title,'Today')]")
    public WebElementFacade PageHeader_todayDate;

    @FindBy(xpath = "//input[@name='./manual/author']")
    public WebElementFacade pageHeaderComponentAuthorDetails;

    @FindBy(xpath = "//span[text()='Shape']//ancestor::button")
    public WebElementFacade pageHeaderComponentBackgroundShape;

    @FindBy(xpath = "//span[text()='Shape']//ancestor::button//following::button[1]")
    public WebElementFacade pageHeaderComponentAsset;

    @FindBy(xpath = "//div[contains(@title,'Dropdown Filter')]")
    public WebElementFacade pageHeaderComponentDropdownFilter;

    @FindBy(xpath = "//coral-button-label[text()='Add']//ancestor::button[1]")
    public WebElementFacade pageHeaderComponentDropdownFilterAddbutton;

    @FindBy(xpath = "//div[@class='page-header background-shape-2']")
    public WebElementFacade pageHeaderComponentShapePublishedSite;

    @FindBy(xpath = "//div[@class='author-component']//following::div[@class='dropdown-filter']")
    public WebElementFacade publishedDropDown;

    @FindBy(xpath = "//div[@role='combobox']")
    public WebElementFacade publishedDropDownBox;

    @FindBy(xpath = "//span[contains(@class,'dropdown-filter')][contains(@data-query-value,'${value}')]/parent::li")
    public WebElementFacade selectDropdownOption;

    @FindBy(xpath = "//span[contains(@class,'dropdown-filter')][contains(@data-query-value,'${value}')]")
    public WebElementFacade selectDropdown;

    @FindBy(xpath = "//span[text()='Shape']//following::coral-selectlist-item[text()='${value}' ][1]")
    public WebElementFacade pageHeaderBackgroundShape;

    @FindBy(xpath = "//*[text()='Asset']/following-sibling::coral-select//*[text()='${value}']")
    public WebElementFacade pageHeaderComponentVideoAsset;

    @FindBy(xpath = "//*[normalize-space()='Asset']/following-sibling::coral-select//button")
    public WebElementFacade pageHeaderComponentVideoAssetBtn;


    @FindBy(xpath = "//div[@data-component_type='page-header']//picture//following::span[text()='${value}']")
    public WebElementFacade authorOnPublishedPage;

    @FindBy(xpath = "//div[@data-component_type='page-header']//picture//img[contains(@src,'${value}')]")
    public WebElementFacade authorLogo;

    @FindBy(xpath = "//div[@data-component_type='page-header']//p[text()='${value}']")
    public WebElementFacade summaryOnPublishedPage;

    @FindBy(xpath = "//div[@data-component_type='page-header' and @data-component_title='${value}']")
    public WebElementFacade headingOnPublishedPage;

    @FindBy(xpath = "//input[@name = './multifields/item${value}/title']")
    public WebElementFacade enterDropdownTitleTextField;

    @FindBy(xpath = " //input[@name='./isForDomestic']")
    public WebElementFacade inputDomesticOption;

    @FindBy(xpath = " //input[@name='./isForInternational']")
    public WebElementFacade inputInternationalOption;






}
