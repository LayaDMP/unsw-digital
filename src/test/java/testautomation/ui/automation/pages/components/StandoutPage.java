package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.List;

public class StandoutPage extends PageObject {
    @FindBy(xpath = "//div[@name='./text']")
    public WebElementFacade standoutText;

    @FindBy(xpath = "//div[@class='standouts']")
    public WebElementFacade publishedStandout;

    @FindBy(css = "div[class='standouts-content'] p")
    public WebElementFacade publishedStandoutText;

}
