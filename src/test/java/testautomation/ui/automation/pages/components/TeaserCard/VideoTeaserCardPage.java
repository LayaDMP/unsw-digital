package testautomation.ui.automation.pages.components.TeaserCard;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class VideoTeaserCardPage extends PageObject {

    @FindBy(xpath = "//coral-select[@name='./variance']//button[@type='button']")
    public WebElementFacade VarianceDropDown;

    @FindBy(xpath = "//coral-selectlist-item[@value='video']")
    public WebElementFacade videoSelectionDropDown;

    @FindBy(xpath = "//coral-tab-label[normalize-space()='Video Settings']")
    public WebElementFacade videoSettingsTab;

    @FindBy(xpath = "//coral-multifield[@data-granite-coral-multifield-name='./videoCards']//button[@type='button']")
    public WebElementFacade videoAddButton;

    @FindBy(xpath = "//input[contains(@name,'./youtubeVideoId')]")
    public WebElementFacade videoTeaserYouTubeID;

    @FindBy(xpath = "//label[text()='Title']/following-sibling::input")
    public WebElementFacade videoTeaserTitle;

    @FindBy(xpath = "//label[text()='Heading Text']/following-sibling::input")
    public WebElementFacade videoTeaserHeading;

    @FindBy(xpath = "//div[contains(@name, './description')]")
    public WebElementFacade videoTeaserDescription;

    @FindBy(xpath = "//div[contains(@name, './transcript')]")
    public WebElementFacade videoTeaserTranscript;

    @FindBy(xpath = "//input[contains(@name, './videoLength')]")
    public WebElementFacade videoTeaserVideoLength;

    @FindBy(xpath ="//foundation-autocomplete[@class='coral-Form-field cmp-image_pathfield']//input[@role='combobox']")
    public WebElementFacade videoTeaserImage;

    @FindBy(xpath = "//coral-buttonlist[@role='listbox']//button")
    public WebElementFacade videoTeaserImageList;

    @FindBy(xpath = "//foundation-autocomplete[contains(@name,'./link')]//input[@role='combobox']")
    public WebElementFacade videoTeaserLink;

    @FindBy(xpath = "//coral-datepicker[contains(@name,'./date')]//input[@role='combobox']")
    public WebElementFacade videoTeaserDate;

    @FindBy(xpath = "//label[text()='Tag']/following-sibling::foundation-autocomplete[contains(@name,'./tags')]//button[@title='Open Selection Dialog']")
    public WebElementFacade videoTeaserTag;

    @FindBy(xpath = "//coral-columnview[@role='tree']")
    public WebElementFacade videoTagSelectionPage;

    @FindBy(xpath = "//coral-columnview-item[@tabindex='0']//coral-columnview-item-thumbnail")
    public WebElementFacade videoTagValue;

    @FindBy(xpath = "//coral-button-label[normalize-space()='Select']")
    public WebElementFacade videoTagSelectButton;

    @FindBy(css=".card-video")
    public WebElementFacade publishedVideoTeaserCard;

    @FindBy(css=".video-transcript")
    public WebElementFacade publishedVideoTeaserTranscript;

    @FindBy(css=".video-teaser__topic")
    public WebElementFacade publishedVideoTeaserTopic;

    @FindBy(css=".video-teaser__heading")
    public WebElementFacade publishedVideoTeaserHeading;

    @FindBy(css="div[class='video-teaser__description'] p")
    public WebElementFacade publishedVideoTeaserDesc;

    @FindBy(css=".video-teaser__time")
    public WebElementFacade publishedVideoTeaserDate;

    @FindBy(css=".video-teaser__tag")
    public WebElementFacade publishedVideoTeaserTag;

    @FindBy(css=".video.base")
    public WebElementFacade publishedVideoTeaserVideo;

    @FindBy(css=".video-teaser__meta-content")
    public WebElementFacade publishedVideoTeaserLink;












}
