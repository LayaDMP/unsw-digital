package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class FlickrPage extends PageObject {

    @FindBy(css = "input[placeholder='Flickr URL']")
    public WebElementFacade flickrURL;

    @FindBy(css = "input[name='./flickrTitle']")
    public WebElementFacade flickrTitle;

    @FindBy(css="input[placeholder='Flickr Image']")
    public WebElementFacade flickrImage;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'flickr')]")
    public WebElementFacade dropDownFlickr;

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(xpath = "//iframe[@class='flickr-embed-frame']")
    public WebElementFacade publishedFlickrFrame;

    @FindBy(css = "a[title='time flies...']")
    public WebElementFacade publishedFlickrImage;


}
