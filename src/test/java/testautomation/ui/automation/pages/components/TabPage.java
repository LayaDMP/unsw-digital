package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class TabPage extends PageObject{

    @FindBy(xpath = "//div[contains(@class,'draggable')][@title='${value}']")
    public WebElementFacade configTabButton;

    @FindBy(xpath = "//button[@data-cmp-hook-childreneditor='add']")
    public WebElementFacade tabAddButton;

    @FindBy(css="input[placeholder='Enter Keyword']")
    public WebElementFacade tabNewComponentAddTextBox;

    @FindBy(css="coral-selectlist-item[value='/apps/unsw-common/components/content/text']")
    public WebElementFacade textOptionSelection;

    @FindBy(css="//coral-selectlist-item[@value='/apps/unsw-common/components/content/author/v3/author']")
    public WebElementFacade authorOptionSelection;

    @FindBy(xpath="//input[@placeholder='${value}']")
    public WebElementFacade titlePlaceholder;

    @FindBy(css="input[placeholder='Author']")
    public WebElementFacade authorPlaceholder;

    @FindBy(css="div[title='Text']")
    public WebElementFacade tabTextTitle;

    @FindBy(css="div[title='Author']")
    public WebElementFacade tabAuthorTitle;

    @FindBy(xpath="//span[contains(@data-click_name,'${value}')]")
    public WebElementFacade nextTab;
}