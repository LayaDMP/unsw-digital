package testautomation.ui.automation.pages.components;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class CardMinimalDividerPage extends PageObject{
    @FindBy(xpath = "//button[.='Add']")
    public WebElementFacade addButtonCardMinimal;

    @FindBy(xpath = "//input[@name='./multifields/item0/./heading']")
    public WebElementFacade headingCardMinimalDivider;

    @FindBy(xpath = "//textarea[@name='./multifields/item0/./description']")
    public WebElementFacade shortDescCardMinimalDivider;

    @FindBy(xpath = "//div[@class='coral-Form-fieldwrapper']//foundation-autocomplete[@class='coral-Form-field']//input[@role='combobox']")
    public WebElementFacade urlCardMinimalDivider;

    @FindBy(css = ".card.card-divider")
    public WebElementFacade publishedCardMinimalComponent;

    @FindBy(css =".title")
    public WebElementFacade publishedCardMinimalHeader;

    @FindBy(css = ".content")
    public WebElementFacade publishedCardMinimalDesc;

    @FindBy(css = "a[href*='library']")
    public WebElementFacade publishedCardMinimalUrl;






}
