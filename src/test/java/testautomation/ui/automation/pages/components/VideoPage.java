package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class VideoPage extends PageObject {


    @FindBy(xpath = "//div[contains(@title,'Video Player (v2)')]")
    public WebElementFacade VideoComponentAdded;

    @FindBy(xpath = "//input[@name = './youtubeVideoId']")
    public WebElementFacade YoutubeVideoID_textBox;

    @FindBy(xpath ="//input[contains(@name,'${value}') and @type='checkbox']")
    public WebElementFacade selectCrop;

    @FindBy(xpath = "//iframe[@class = 'youtube-player' and @data-src='${value}']")
    public WebElementFacade video;

}
