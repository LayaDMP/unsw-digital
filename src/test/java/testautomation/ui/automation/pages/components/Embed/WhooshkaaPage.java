package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class WhooshkaaPage extends PageObject {
    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'whooshka')]")
    public WebElementFacade dropDownWhooshkaa;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(css="input[placeholder='Whooshka ID']")
    public WebElementFacade whooshkaaID;

    @FindBy(xpath = "//div[@data-component_type='embed']//iframe")
    public WebElementFacade publishedWhooshkaaFrame;

    @FindBy(css="#app")
    public WebElementFacade publishedWhooshkaa;





}
