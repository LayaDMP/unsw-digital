package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class GeneralCardsPage extends PageObject {

    @FindBy(xpath = "//*[@name='./pages']//input[@role='combobox']")
    public WebElementFacade generalCardsTextBox;

    @FindBy(xpath = "//*[contains(text(), 'General Cards') and @role]")
    public WebElementFacade generalCardsComponent;

    @FindBy(xpath = "//div[(@title= 'General Cards (v2)')]")
    public WebElementFacade generalCardsAdded;

    @FindBy(xpath = "//div[contains(@class,'card-general')]")
    public WebElementFacade generalCardsOnPublishedSite;

    @FindBy(xpath = "//div[contains(@class,'card-general   unsw-brand-card-theme')]")
    public WebElementFacade squarecardthemealignment;

    @FindBy(xpath = "//img[contains(@src,'crop=square')]")
    public WebElementFacade squareImageOnPublishedSiteGeneralCards;

    @FindBy(xpath = "//input[@name = './unswBrandCard' and @value='true']")
    public WebElementFacade enableSqureCardTheme;

    @FindBy(xpath = "//div[@id='slick-slide0${value}']")
    public WebElementFacade publishedGeneralCards;

}
