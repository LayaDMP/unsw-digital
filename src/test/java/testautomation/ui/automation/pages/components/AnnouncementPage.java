package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class AnnouncementPage extends PageObject{


    @FindBy(xpath = "//coral-select[@name='./announcementType']//button[@type='button']")
    public WebElementFacade announcementTypeDropdown;

    @FindBy(xpath = "//div[@name = './text']")
    public WebElementFacade textboxTextComponent;

    @FindBy(xpath = "//div[@name='./shortAnnouncement']//p")
    public WebElementFacade pageAnnouncementShortTextBox;

    @FindBy(xpath = "//div[@name='./longAnnouncement']//p")
    public WebElementFacade pageAnnouncementLongTextBox;

    @FindBy(xpath = "//div[@class='showHideAnnouncementType']//div[@data-type='inline']//button[@title='Hyperlink']")
    public WebElementFacade hyperlinkDropdown;

    @FindBy(xpath = "//input[@placeholder='Path']")
    public WebElementFacade hyperlinkURLTextbox;

    @FindBy(xpath = "//input[@placeholder='Alt Text']")
    public WebElementFacade hyperlinkAltTextbox;

    @FindBy(xpath = "//div[contains(@class,'rte-dialog-column')]//button[@aria-expanded='false']")
    public WebElementFacade hyperlinkTargetDropdown;

    @FindBy(xpath = "//coral-selectlist-item[@value='_${value}']")
    public WebElementFacade selectHyperlinkDropDownOption;

    @FindBy(css = "button[title='Apply']")
    public WebElementFacade hyperlinkApplyButton;

    @FindBy(css = "coral-checkbox[name='./shortCloseIcon']")
    public WebElementFacade shortCloseIconCheckBox;

    @FindBy(css = "input[name='./longCloseIcon']")
    public WebElementFacade longCloseIconCheckBox;

    @FindBy(css = "button[aria-label='Close']")
    public WebElementFacade publishedCloseIcon;

    @FindBy(xpath = "//a[text()='Click here']")
    public WebElementFacade publishedLink;

    @FindBy(xpath = "//div[contains(@class,'pageAnnouncement')]//p")
    public List<WebElementFacade> publishedText;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade dropdownOption;

    @FindBy(xpath = "//div[contains(@class,'-component single-line-')]")
    public WebElementFacade postCloseDisappear;

    @FindBy(xpath = "//coral-button-label[contains(.,'Add')]")
    public WebElementFacade longAddButton;

    @FindBy(xpath = "//input[@name='./multifields/item${value}/./buttonCopy']")
    public WebElementFacade buttonTextfield;

    @FindBy(xpath = "//*[contains(@name,'./multifields/item${value}/./buttonLink')]//input[@role='combobox']")
    public WebElementFacade buttonLinkfield;

    @FindBy(xpath = "//coral-select[@name='./multifields/item${value}/./buttonColor']//button[@type='button']")
    public WebElementFacade buttonColorDropDown;

    @FindBy(xpath = "//coral-overlay[contains(@class,'open')]//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonColorDropDownSelect;

    @FindBy(xpath = "//coral-select[@name='./multifields/item${value}/./buttonStyle']//button[@type='button']")
    public WebElementFacade buttonStyleDropDown;

    @FindBy(xpath = "//coral-overlay[contains(@class,'open')]//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonStylerDropDownSelect;

    @FindBy(xpath = "//coral-select[@name='./multifields/item${value}/./labelStyle']//button[@type='button']")
    public WebElementFacade buttonLabelDropDown;

    @FindBy(xpath = "//coral-overlay[contains(@class,'open')]//coral-selectlist-item[text()='${value}']")
    public WebElementFacade buttonLabelDropDownSelect;

    @FindBy(xpath = "//coral-select[@name='./multifields/item${value}/./buttonSize']//button[@type='button']")
    public WebElementFacade buttonSizeDropDown;

    @FindBy(xpath = "//coral-overlay[contains(@class,'open')]//coral-selectlist-item[@value='${value}']")
    public WebElementFacade buttonSizeDropDownSelect;

    @FindBy(xpath = "//select[@name='./multifields/item${value}/./icon']/preceding-sibling::div//span[@class='selector-button']")
    public WebElementFacade buttonIcon;

    @FindBy(xpath = "//div[@class='selector-popup'][not(contains(@style,'none'))]//span[@title='${value}']")
    public WebElementFacade buttonIconDropDownSelect;

    @FindBy(css = "input[name='./title']")
    public WebElementFacade longAnnouncementTitle;

    @FindBy(xpath = "//div[contains(text(),'Page Title')]")
    public List<WebElementFacade> publishedButtons;

    @FindBy(xpath = "//a[contains(@class,'${value}')]")
    public WebElementFacade publishedButtonLink;

    @FindBy(xpath = "//div[@class='icon']//i")
    public List<WebElementFacade> publishedIcon;






}
