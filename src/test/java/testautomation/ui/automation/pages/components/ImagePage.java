package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class ImagePage extends PageObject {

    @FindBy(xpath = "//div[@class= 'cq-droptarget cq-subdroptarget cq-Overlay-subdroptarget' and @data-asset-accept='[\"image/.*\"]']")
    public WebElementFacade imageAddedOnImageComponent;

    @FindBy(xpath = "//button[@title= 'Edit']")
    public WebElementFacade editOption;

    @FindBy(xpath = "//button[@title= 'Enter Fullscreen']")
    public WebElementFacade fullScreen;

    @FindBy(xpath = "//*[@id=\"FullScreenWrapper\"]/div/div[1]/div[2]/div[1]/div[1]/button[1]")
    public WebElementFacade startCropButton;

    @FindBy(css = "#FullScreenWrapper > div > div.imageeditor-toolbars.icongroup > div:nth-child(2) > div:nth-child(2) > div.imageeditor-toolbar-right-hand-side > button:nth-child(2)")
    public WebElementFacade confirmCrop;

    @FindBy(css = "#coral-id-509 > coral-icon")
    public WebElementFacade AlignmentDropdown;

    @FindBy(xpath = "//button[@title='Exit Fullscreen']")
    public WebElementFacade exitFullScreen;

    @FindBy(xpath = "//*[(@value= '/apps/unsw-common/components/content/image/v2/image')]")
    public WebElementFacade imageOptionFromDropdown;

    @FindBy(xpath = "//*[(@data-asset-id= 'image')]")
    public WebElementFacade imageComponentAdded;

    @FindBy(xpath = "//foundation-autocomplete[@name='./linkURL']//child::input[1]")
    public WebElementFacade linkImageComponent;

    @FindBy(xpath = "//foundation-autocomplete[@name='./linkURL']//following::span[@class='coral-InputGroup-button'][1]")
    public WebElementFacade openSelectionDialogImageComponent;

    @FindBy(xpath = "//coral-tab-label[text()='Metadata']//ancestor::coral-tab[1]")
    public WebElementFacade metadataTab;

    @FindBy(xpath = "//input[@name='./isDecorative']")
    public WebElementFacade imageIsDecorative;

    @FindBy(xpath = "//img[@data-cmp-hook-image='image' and @alt and contains(@src,'${value}')]")
    public WebElementFacade publishedImage;

    @FindBy(xpath = "//input[@data-seeded-value='${value}']")
    public WebElementFacade altText;

    @FindBy(xpath = "//input[@data-seeded-value='${value}']")
    public WebElementFacade caption;

    @FindBy(xpath = "//button[@class= 'coral-Button popover-button' and text()='${value}']")
    public WebElementFacade cropModeSelect;

    @FindBy(xpath = "//img[@title = '${value}']//ancestor::div[1]")
    public WebElementFacade imageCaptionPublishedPage;

    @FindBy(xpath = "//img[@alt='${value}']")
    public WebElementFacade altTextPublished;

    @FindBy(xpath = "//img[@title = '${value}']")
    public WebElementFacade captionPublishedPage;








}
