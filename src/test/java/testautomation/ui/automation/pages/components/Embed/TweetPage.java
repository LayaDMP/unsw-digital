package testautomation.ui.automation.pages.components.Embed;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

public class TweetPage extends PageObject{


    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(css = "article[role='article']")
    public WebElementFacade publishedTwitterTweet;

    @FindBy(css = "iframe[title='Twitter Tweet']")
    public WebElementFacade publishedTwitterFrame;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'tweet')]")
    public WebElementFacade dropDownTweet;

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//textarea[@name='./tweetHtml']")
    public WebElementFacade pasteEmbedHtmlTweet;
}
