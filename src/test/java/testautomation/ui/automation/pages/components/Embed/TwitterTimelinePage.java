package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class TwitterTimelinePage extends PageObject {

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'twitterTimeline')]")
    public WebElementFacade dropDownTwitterTimeline;

    @FindBy(xpath = "//textarea[@name='./twitterTimelineHtml']")
    public WebElementFacade pasteEmbedHtmlTwitterTimeline;

    @FindBy(css = ".timeline-Widget")
    public WebElementFacade publishedTwitterTimeline;

    @FindBy(css = "iframe[title='Twitter Timeline']")
    public WebElementFacade publishedTwitterTimelineFrame;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;


}
