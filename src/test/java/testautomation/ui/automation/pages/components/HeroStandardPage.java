package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class HeroStandardPage extends PageObject {

    @FindBy(xpath = "//input[@name='./heading']")
    public WebElementFacade headingHeroStandard;

    @FindBy(xpath = "//input[@name='./subheading']")
    public WebElementFacade subHeadingHeroStandard;

    @FindBy(xpath = "//div[@id='EditableToolbar']//coral-icon[@icon='brush']")
    public WebElementFacade styleOption;

    @FindBy(xpath = "(//div[@class='editor-StyleSelector-actions']//coral-icon[@aria-label='check'])[2]//ancestor::button[1]")
    public WebElementFacade StyleCheckbox;

    @FindBy(xpath = "//div[contains(@data-path,'/content/unsw-dex-reference/en/test-automation/') and @role='link' and @title = '${value}']")
    public WebElementFacade addedComponent;

    @FindBy(xpath = "//div[@title='Hero Standard']")
    public WebElementFacade heroStandard;

    @FindBy(xpath = "//div[contains(@class,'hero-standard aem-shape-mask-')]")
    public WebElementFacade verifyStyle;

    @FindBy(xpath = "//div[contains(@class,'hero-standard')]//h1[text()='${value}']")
    public WebElementFacade verifyHeading;

    @FindBy(xpath = "//div[contains(@class,'hero-standard')]//img[contains(@src,'${value}')]")
    public WebElementFacade verifyBackgroundImage;

    @FindBy(xpath = "//div[contains(@class,'hero-standard')]//p[contains(text(),'${value}')]")
    public WebElementFacade verifySubheading;

    @FindBy(xpath = "//coral-selectlist-item[text()='${value}']")
    public WebElementFacade styleOptionSe;

    @FindBy(xpath = "//coral-tab-label[contains(text(),'${value}')]")
    public WebElementFacade heroTypeTabLink;

    @FindBy(css = "coral-select[name='./heroType']")
    public WebElementFacade heroTypeDropdown;

    @FindBy(xpath = "//coral-selectlist-item[@value='${value}']")
    public WebElementFacade heroTypeOption;












}
