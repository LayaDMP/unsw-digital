package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class SoundcloudPage extends PageObject {
    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//div[@data-component_type='embed']//iframe")
    public WebElementFacade publishedSoundCloudFrame;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'soundCloud')]")
    public WebElementFacade dropDownSoundCloud;

    @FindBy(css = "input[placeholder='SoundCloud URL']")
    public WebElementFacade soundCloudURL;

    @FindBy(css = "input[name='./bandLinkTitle']")
    public WebElementFacade soundBandLinkTitle;

    @FindBy(css = "input[name='./bandLink']")
    public WebElementFacade soundBandLink;

    @FindBy(css= "input[name='./songLinkTitle']")
    public WebElementFacade soundSongLinkTitle;

    @FindBy(css = "input[name='./songLink']")
    public WebElementFacade soundSongLink;

    @FindBy(css="#widget")
    public WebElementFacade publishedSoundCloud;

}
