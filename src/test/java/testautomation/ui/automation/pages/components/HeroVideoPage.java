package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;


public class HeroVideoPage extends PageObject {

    @FindBy(xpath = "//div[contains(@title,'Hero Video')]")
    public WebElementFacade heroVideoSettings;
    @FindBy(xpath = "//coral-tab-label[contains(text(),'Content')]/parent::coral-tab")
    public WebElementFacade textAndContentTab;
    @FindBy(name = "./title")
    public WebElementFacade titleTxt;
    @FindBy(name = "./subtitle")
    public WebElementFacade subTitleTxt;
    @FindBy(name = "./authorName")
    public WebElementFacade authorNameTxt;
    @FindBy(xpath = "//*[@type='date']//input[contains(@is,'textfield')]")
    public WebElementFacade publishedDateTxt;
    @FindBy(xpath = "//input[@type='checkbox' and @name='./showPublishedDate']")
    public WebElementFacade showPublishedDateCheckBox;
    @FindBy(xpath = "//coral-tab-label[contains(text(),'Video')]/parent::coral-tab")
    public WebElementFacade videoTab;
    @FindBy(name = "./youtubeVideoId")
    public WebElementFacade youTubeVideoIdtxt;
    @FindBy(xpath = "//h1[text()='${value}']")
    public WebElementFacade publishedTitle;
    @FindBy(className = "standfirst")
    public WebElementFacade publishedSubTitle;
    @FindBy(className = "name")
    public WebElementFacade publishedAuthorName;
    @FindBy(className = "date")
    public WebElementFacade publishedDate;
    @FindBy(xpath = "//iframe[@data-src='${value}']")
    public WebElementFacade publishedYouTubeId;

}
