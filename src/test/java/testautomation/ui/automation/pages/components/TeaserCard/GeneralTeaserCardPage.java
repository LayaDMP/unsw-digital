package testautomation.ui.automation.pages.components.TeaserCard;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.annotations.findby.FindBy;

public class GeneralTeaserCardPage extends PageObject {



    @FindBy(xpath = "//coral-select[@placeholder='Select']//button[@type='button']")
    public WebElementFacade teaserTypeDropDown;

    @FindBy(css="coral-selectlist-item[value='manual']")
    public WebElementFacade teaserTypeManualSelection;

    @FindBy(css = "input[value='true'][name='./unswBrandCard']")
    public WebElementFacade teaserEnableSquareTheme;

    @FindBy(css="coral-multifield[data-dep-value='manual'] button[type='button']")
    public WebElementFacade teaserAddButton;

    @FindBy(xpath = "//input[@data-multifield-required='true']")
    public WebElementFacade teaserGeneralHeadingText;

    @FindBy(xpath = "//foundation-autocomplete[@class='coral-Form-field cmp-image_pathfield']//input[@role='combobox']")
    public WebElementFacade teaserGeneralImage;

    @FindBy(css = "coral-buttonlist[role='listbox'] button")
    public WebElementFacade teaserGeneralImageList;

    @FindBy(css = "textarea[maxlength='350']")
    public WebElementFacade teaserGeneralStandfirstText;

    @FindBy(xpath="//foundation-autocomplete[@data-multifield-required='true']//input[@role='combobox']")
    public WebElementFacade teaserGeneralLinkText;

    @FindBy(css="input[name='./manualCards/item0/category']")
    public WebElementFacade teaserGeneralCategoryText;

    @FindBy(css="coral-datepicker[type='date'] input[role='combobox']")
    public WebElementFacade teaserGeneralNewsDate;

    @FindBy(xpath = "//coral-dialog[@backdrop='none']//div//form")
    public WebElementFacade teaserGeneralAltText;


    @FindBy(css ="a[data-click_category='teaser-cards']")
    public WebElementFacade publishedGeneralTeaserCardComponent;

    @FindBy(css="img[title='Red sand']")
    public WebElementFacade publishedGeneralTeaserCardImage;

    @FindBy(css="div[class='category '] span")
    public WebElementFacade publishedGeneralTeaserCardCategory;

    @FindBy(css=".title")
    public WebElementFacade publishedGeneralTeaserCardTitle;

    @FindBy(css="div[class='content '] p")
    public WebElementFacade publishedGeneralTeaserCardStandfirst;

    @FindBy(css=".date")
    public WebElementFacade publishedGeneralTeaserCardDate;
}
