package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class TextPage extends PageObject {


    @FindBy(xpath = "//${value}")
    public WebElementFacade publishedText;

    @FindBy(xpath = "//div[@name = './text']")
    public WebElementFacade textboxTextComponent;

    @FindBy(xpath = "//button[contains(@class,'is-selected')][@title='Paragraph formats']")
    public WebElementFacade paragraphDropdownTextComponent;

    @FindBy(xpath = "//button[@title='Justify'][contains(@class,'is-')]")
    public WebElementFacade textSizeDropdownTextComponent;

    @FindBy(xpath = "//button[@data-action='justify#justify${value}']")
    public WebElementFacade textAlignmentSelect;

    @FindBy(xpath = "//*[contains(@class,'popover is-open')]//button[contains(@data-action,'paraformat#${value}')]")
    public WebElementFacade textHeadingSelect;

}
