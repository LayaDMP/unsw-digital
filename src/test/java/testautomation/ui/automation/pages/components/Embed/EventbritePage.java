package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class EventbritePage extends PageObject {

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'eventbrite')]")
    public WebElementFacade dropDownEventBrite;

    @FindBy(css="input[placeholder='Eventbrite Event ID']")
    public WebElementFacade eventbriteID;

    @FindBy(xpath = "//div[@data-component_type='embed']//iframe")
    public WebElementFacade publishedEventBriteFrame;

    @FindBy(css="#root")
    public WebElementFacade publishedEventBrite;

}
