package testautomation.ui.automation.pages.components;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.List;

public class QuotePage extends PageObject {
    @FindBy(xpath = "//input[@name='./quoteText']")
    public WebElementFacade quoteTextingSpace;

    @FindBy(xpath = "//input[@name='./authorName']")
    public WebElementFacade quoteAuthorName;

    @FindBy(xpath = "//input[@name='./positionTitle']")
    public WebElementFacade quotePositionTitle;

    @FindBy(xpath = "//div[@class='cq-FileUpload-thumbnail']")
    public WebElementFacade quoteImage;

    @FindBy(xpath = "//div[@title='Quote']")
    public WebElementFacade quoteComponentAdded;

    @FindBy(xpath = "//img[@class='quote-icon']")
    public WebElementFacade publishPageQuoteIcon;

    @FindBy(xpath = "//img[@class='author-image']")
    public WebElementFacade publishedPageQuoteImage;

    @FindBy(xpath= "//div[@class='author-name']")
    public WebElementFacade publishedQuoteAuthorName;

    @FindBy(xpath = "//div[@class='author-role']")
    public WebElementFacade publishedQuotePositionTitle;

    @FindBy(xpath = "//p[@class='cite']")
    public WebElementFacade publishedQuoteText;
}
