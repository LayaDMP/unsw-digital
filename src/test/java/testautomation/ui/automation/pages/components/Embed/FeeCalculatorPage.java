package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class FeeCalculatorPage extends PageObject {
    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'feeCalculator')]")
    public WebElementFacade dropDownFeeCalculator;

   @FindBy(css = "input[name='./url']")
    public WebElementFacade feeCalculatorURL;

   @FindBy(css="input[name='./title']")
    public WebElementFacade feeCalculatorTitle;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(xpath = "//iframe[@class='unsw-fees-calculator']")
    public WebElementFacade publishedFeeCalculatorFrame;

    @FindBy(css="#unsw-fees-calculator")
    public WebElementFacade publishedFeeCalculator;

}
