package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class QualtricsPage extends PageObject {
    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'qualtrics')]")
    public WebElementFacade dropDownQualtrics;

    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(css="input[placeholder='Qualtrics Form URL']")
    public WebElementFacade qualtricsURL;

    @FindBy(xpath = "//coral-numberinput[@aria-disabled='false']//input[@name='./iframeheight']")
    public WebElementFacade qulatricsHeight;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(css="#SurveyEngineBody")
    public WebElementFacade publishedQualtrics;

    @FindBy(xpath = "//div[@class='container-iframe']//iframe")
    public WebElementFacade publishedQualtricsFrame;


}
