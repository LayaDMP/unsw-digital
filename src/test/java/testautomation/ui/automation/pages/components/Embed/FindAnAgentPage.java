package testautomation.ui.automation.pages.components.Embed;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class FindAnAgentPage extends PageObject {
    @FindBy(xpath = "//coral-select[@name='./embeddableResourceType']//button[@type='button']")
    public WebElementFacade dropdownEmbeddable;

    @FindBy(xpath = "//div[@data-component_type='embed']")
    public WebElementFacade publishedEmbedGrid;

    @FindBy(xpath = "//coral-selectlist-item[contains(@value,'iframe')]")
    public WebElementFacade dropDownIFrame;

    @FindBy(css="input[placeholder='iFrame URL']")
    public WebElementFacade iFrameID;

    @FindBy(css=".embeded")
    public WebElementFacade publishedFindAnAgent;

    @FindBy(xpath = "//div[@data-component_type='embed']//iframe")
    public WebElementFacade publishedFindAnAgentFrame;

}
