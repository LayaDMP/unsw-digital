package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class VideoStandFirstPage extends PageObject {


    @FindBy(xpath = "//div[@title = 'Video Player Standfirst (v2)']")
    public WebElementFacade VideoplayerStandfirstComponentAdded;

    @FindBy(xpath = "//div[@data-text = 'Video Player Standfirst (v2)']//following::input[@name='./title']")
    public WebElementFacade VideoplayerStandfirstComponent_Title;

    @FindBy(xpath = "//div[@name='./standfirst']")
    public WebElementFacade VideoplayerStandfirstComponent_Standfirst;

    @FindBy(xpath = "//input[@name='./buttonLabel']")
    public WebElementFacade VideoplayerStandfirstComponent_Buttonlabel;


    @FindBy(xpath = "//label[text()='Button URL']//following::input[1]")
    public WebElementFacade VideoplayerStandfirstComponent_ButtonURL;


    @FindBy(xpath = "(//label[text()='Button URL']//following::button[@title = 'Open Selection Dialog'])[1]")
    public WebElementFacade VideoplayerStandfirstComponent_ButtonURL_OpenSelectionDialog;

    @FindBy(xpath = "//input[@name= './externallinklabel']")
    public WebElementFacade VideoplayerStandfirstComponent_MinimumLinkLabel;

    @FindBy(xpath = "//label[text()='Minimal Link URL']//following::input[1]")
    public WebElementFacade VideoplayerStandfirstComponent_MinimumLinkURL;

    @FindBy(xpath = "(//label[text()='Button URL']//following::button[@title = 'Open Selection Dialog'])[2]")
    public WebElementFacade VideoplayerStandfirstComponent_MinimumLinkURL_OpenSelectionDialog;

    @FindBy(xpath = "//input[@name= './youtubeVideoId']")
    public WebElementFacade VideoplayerStandfirstComponent_YoutubeID;

    @FindBy(xpath = "//input[@name= './showTranscript']")
    public WebElementFacade VideoplayerStandfirstComponent_ShowTranscriptCheckbix;

    @FindBy(xpath = "//input[@name= './transcripttitle']")
    public WebElementFacade VideoplayerStandfirstComponent_TranscriptTitle;

    @FindBy(xpath = "//img[@src = '/content/unsw-dex-reference.thumb.48.48.png']//ancestor::coral-columnview-item-thumbnail[1]")
    public WebElementFacade DexReferencePopup_VideoplayerStandfirst;

    @FindBy(xpath = "//div[@name= './transcript']")
    public WebElementFacade VideoplayerStandfirstComponent_TranscriptOfTheVideo;

    @FindBy(xpath = "//h2[normalize-space()='VideoplayerStandfirst_Title']")
    public WebElementFacade publishedVideoStandPlayerTitle;

    @FindBy(css = "div[class='video-intro-text'] p")
    public WebElementFacade publishedVideoStandPlayerStandFirst;

    @FindBy(css = ".uds-button.light.outline")
    public WebElementFacade publishedVideoStandPlayerbuttonlabel;

    @FindBy(css = ".external-link")
    public WebElementFacade publishedVideoStandPlayerMiniLabel;

    @FindBy(css = "iframe[title='YouTube video']")
    public WebElementFacade publishedVideoStandPlayerYoutubeId;

    @FindBy(css = ".video-transcript-item")
    public WebElementFacade publishedVideoStandPlayerTranscripttitle;

    @FindBy(xpath = "//p[2]")
    public WebElementFacade publishedVideoStandPlayerTranscriptContent;






}
