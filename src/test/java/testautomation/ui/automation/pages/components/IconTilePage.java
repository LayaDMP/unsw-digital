package testautomation.ui.automation.pages.components;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class IconTilePage extends PageObject {



    @FindBy(xpath = "//div[@title = 'Icon Tile' and @role = 'link']")
    public WebElementFacade icontilesComponentAdded;

    @FindBy(xpath = "//coral-select[@name='./tileStyle']//coral-icon[@icon='chevronDown']//ancestor::button")
    public WebElementFacade titleStyleDropdown;

    @FindBy(xpath = "//coral-multifield[@data-granite-coral-multifield-name='./multifields']//button//coral-button-label[text()='Add']")
    public WebElementFacade addButtonIconTile;

    @FindBy(xpath = "//label[text()='Icon for this Tile *']//following::button[@title='Open Selection Dialog'][1]")
    public WebElementFacade iconForThisTileOpenSelectionDialog;

    @FindBy(xpath = "//input[@name = './multifields/item0/./heading']")
    public WebElementFacade headingIcontile;

    @FindBy(xpath = "//textarea[@name = './multifields/item0/./description']")
    public WebElementFacade subtitleIcontile;

    @FindBy(xpath = "//textarea[@name = './multifields/item0/./tileCopy']")
    public WebElementFacade TitleCopy_Icontile;

    @FindBy(xpath = "//input[@name='./multifields/item0/./button']")
    public WebElementFacade buttonLabelIcontile;

    @FindBy(xpath = "//foundation-autocomplete[@name='./multifields/item0/./buttonurl']//following::button[@title='Open Selection Dialog'][1]")
    public WebElementFacade ButtonURL_OpenSelectionDialog_Icontile;

    @FindBy(xpath = "//img[@src= '/content/dam/images/graphics/icons/unsw-logo-favicon-32x32.png.thumb.48.48.png']")
    public WebElementFacade UNSWLogoPopupIcontile;

    @FindBy(css = ".image")
    public WebElementFacade iconForThisTilePublishedPage;

    @FindBy(xpath = "//foundation-autocomplete[contains(@name,'buttonurl')]//input[@role='combobox']")
    public WebElementFacade iconTilesTextBox;

    @FindBy(xpath = "//coral-selectlist-item[text()='${value}']")
    public WebElementFacade titleStyle;

    @FindBy(xpath = "//li[contains(@class,'icon-tile-item no-background')]")
    public WebElementFacade titleStyleBackground;
    @FindBy(xpath = "//li[contains(@class,'icon-tile-item shadow')]")
    public WebElementFacade titleStyleShadow;
    @FindBy(xpath = "//li[contains(@class,'icon-tile-item white-background')]")
    public WebElementFacade titleStyleWhiteBackground;

    @FindBy(css = ".tile-heading")
    public WebElementFacade headingIconTile;
    @FindBy(css = ".tile-subtitle")
    public WebElementFacade subtitleIconTile;
    @FindBy(css = ".tile-copy")
    public WebElementFacade titleCopyPublished;
    @FindBy(xpath = "//p[@class='label1' and text()='${value}']")
    public WebElementFacade buttonLabel;




}
