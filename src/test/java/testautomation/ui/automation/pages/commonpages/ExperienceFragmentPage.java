package testautomation.ui.automation.pages.commonpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;




public class ExperienceFragmentPage extends PageObject {


    @FindBy(xpath = "//div[@class='globalnav-homecard-title' and text()='Experience Fragments']")
    public WebElementFacade experienceFragments;

    @FindBy(xpath = "//coral-columnview-item-content[@title='UNSW']")
    public WebElementFacade unswfolderExfragment;

    @FindBy(xpath = "//coral-columnview-item-content[@title='Test Automation']")
    public WebElementFacade testAutomationFolderExFragment;

    @FindBy(xpath = "//coral-list-item-content[text()='Experience Fragment']")
    public WebElementFacade experienceFragmentDropdown;

    @FindBy(xpath = "//*[@data-foundation-wizard-control-action='next' and @type='button']")
    public WebElementFacade nextButtonUnderCreatePageExFragment;

    @FindBy(xpath = "//input[@name='./jcr:title']")
    public WebElementFacade titleExperienceFragment;

    @FindBy(xpath = "//input[@name='pageName']")
    public WebElementFacade nameExperienceFragment;

    @FindBy(xpath = "//textarea[@name='./jcr:description']")
    public WebElementFacade descriptionExperienceFragment;

    @FindBy(xpath = "//coral-button-label[contains(text(), 'Create')]")
    public WebElementFacade createButtonUnderCreatePageExFrag;

    @FindBy(xpath = "//coral-card-title[text()='UNSW Common Experience Fragment Template']")
    public WebElementFacade UNSWCommonExperienceFragmentTemplate;

    @FindBy(css = "#granite-shell-actionbar > betty-titlebar-secondary > button.granite-collection-create.foundation-toggleable-control.coral3-Button.coral3-Button--primary")
    public WebElementFacade createButtonExFragment;

    @FindBy(css = "body > coral-dialog > div.coral3-Dialog-wrapper > coral-dialog-footer > button.coral3-Button.coral3-Button--secondary")
    public WebElementFacade doneButtonCreateButtonExFragment;

    @FindBy(css = "//div[contains(@data-path,'/master/jcr:content/root/*')]")
    public WebElementFacade RootColumn_ExperienceFragment;

    @FindBy(xpath = "//button[@class=\"toggle-sidepanel editor-GlobalBar-item js-editor-SidePanel-toggle coral3-Button coral3-Button--minimal\"][@title=\"Toggle Side Panel\"]")
    public WebElementFacade toggleSidePanel;

    @FindBy(xpath = "//input[@placeholder='Enter Keyword']")
    public WebElementFacade enterKeywordTextBox;

    @FindBy(xpath = "//coral-selectlist-item[@value='/apps/unsw-common/components/structure/header/v3/globalheader']")
    public WebElementFacade unswcommonglobalheadercomponentDropdown;

    @FindBy(xpath = "//div[@class='cq-Overlay cq-Overlay--component cq-Overlay--container cq-draggable cq-droptarget']")
    public WebElementFacade unswcommonglobalheadercomponentadded;

    @FindBy(xpath = "//coral-tab[@title='Assets']")
    public WebElementFacade assetsOptionSidePanel;

    @FindBy(xpath = "//foundation-autocomplete[@name='./logoLink']//button[@title='Open Selection Dialog']")
    public WebElementFacade openSelectionDialogLogoLinkFieldGlobalHeaderComponent;

    @FindBy(xpath = "//div[@class='coral-Form-fieldwrapper']//following::label[contains(text(),'Logo Link')]//following::foundation-autocomplete[2]//div[1]//div//input")
    public WebElementFacade searchPageLinkLinkFieldGlobalHeaderComponent;

    @FindBy(xpath = "//input[@name='./searchLabel']")
    public WebElementFacade searchLabelFieldGlobalHeaderComponent;

    @FindBy(xpath = "//coral-tab-label[text()='Sections']")
    public WebElementFacade sectionsTabGlobalHeaderComponent;

    @FindBy(xpath = "//coral-tab-label[text()='Logo']//ancestor::coral-tab[1]")
    public WebElementFacade logoTabGlobalHeaderComponent;

    @FindBy(xpath = "//button[@data-cmp-hook-childreneditor='add']")
    public WebElementFacade addButtonUnderSectionsTabGlobalHeaderComponent;

    @FindBy(xpath = "//coral-selectlist-item[text()='Global Header Section']")
    public WebElementFacade globalHeaderSectionDropdownSectionsTabGlobalHeaderComponent;

    @FindBy(xpath = "//coral-tab[@title='Content Tree']")
    public WebElementFacade contentTreeOptionSidePanel;

    @FindBy(xpath = "//span[@class='editor-ContentTree-itemTitle' and text()='UNSW Common Global Header']")
    public WebElementFacade UNSWCommonGlobalHeaderUnderContentTreeOptionSidePanel;

    @FindBy(xpath = "//div[@id='EditableToolbar']//button[@title='Configure']")
    public WebElementFacade configureButtonExFragment;

    @FindBy(xpath = "//img[contains(@src,'/content/unsw-dex-reference.thumb.48.48.png')]")
    public WebElementFacade DexReferenceSitePopup_ButtonComponent;

    @FindBy(xpath = "//coral-button-label[text()='Select']")
    public WebElementFacade selectButtonPopup;

    @FindBy(xpath = "//input[@name='./linkLabel']")
    public WebElementFacade sectionsTitleGlobalHeaderSection;

    @FindBy(xpath = "//label[text()='Section Title Link *']//following::input[1]")
    public WebElementFacade sectionsTitleLinkGlobalHeaderSection;

    @FindBy(xpath = "//coral-button-label[text()='Add']//ancestor::button[1]")
    public WebElementFacade addSubSectionSettings;

    @FindBy(xpath = "//coral-selectlist-item[text()='Global Header Sub Section']")
    public WebElementFacade globalHeaderSubSection;

    @FindBy(xpath = "//label[text()='Sub Section Link']//following::input[1]")
    public WebElementFacade subSectionLinkGlobalHeader;

    @FindBy(xpath = "//input[@name='./linkLabel']")
    public WebElementFacade subSectionsTitleGlobalHeaderSection;

@FindBy(xpath = "//img[@src='/content/experience-fragments/unsw/test_automation/${value}/master.thumb.128.128.png']")
public WebElementFacade page;

@FindBy(xpath =  "//coral-columnview-item-content[@title = '${value}']")
    public WebElementFacade folder;

@FindBy(xpath = "//img[@src='/content/unsw-dex-reference.thumb.48.48.png']")
    public WebElementFacade dexReferenceSitePopup;

@FindBy(xpath ="//img[@src='/content/unsw-dex-reference/en.thumb.48.48.png']")
    public WebElementFacade referenceSitePopup;

@FindBy(xpath = "(//span[@class='editor-ContentTree-item u-coral-ellipsis']//span[contains(text(),'Global Header Sub Section')])[${value}]")
    public WebElementFacade globalHeaderSubSec;

@FindBy(xpath = "(//coral-tree-item-content//span[@class='editor-ContentTree-itemTitle' and text()='Global Header Section'])[${value}]")
public WebElementFacade globalHeaderSection;

}
