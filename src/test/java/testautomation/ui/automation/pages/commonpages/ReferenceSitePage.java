package testautomation.ui.automation.pages.commonpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;


import java.time.LocalDate;




public class ReferenceSitePage extends PageObject {

    public static LocalDate today = LocalDate.now();
    public static final String IMAGENAMETOUSE="unique2";
    public static final String YOUTUBE_ID="HooIs8Ewc2k";
    public static final String GENERIC_EXTERNAL_LINK = "https://www.unsw.edu.au/";
    public static final String PATH_TO_TEST_AUTOMATION="/content/unsw-dex-reference/en/test-automation";



    //Map Objects defined in ObjectRepository to a WebElementFacade for enabling UI interaction
    @FindBy(xpath = "//*[contains(@data-path,'root/responsivegrid-layout-fixed-width/responsivegrid-full-top/*') and @title = 'Layout Container [Root]']")
    public WebElementFacade degreeFullWidthLayout;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-full-top/*')]")
    public WebElementFacade oneColumnFullWidthLayout;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-left-col/*')]")
    public WebElementFacade twoColumnLeftLayout;

    @FindBy(xpath = "//div[contains(@data-path,'jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-left-col/*')]")
    public WebElementFacade TwoColumn_LeftColumnLayout;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-left-col/*')]")
    public WebElementFacade oneColumnLeftLayout;

    @FindBy(css = "#EditableToolbar > button:nth-child(1)")
    public WebElementFacade PlusButton;

    @FindBy(xpath = "//input[@placeholder='Enter Keyword']")
    public WebElementFacade EnterKeywordTextBox;

    @FindBy(xpath = "//*[(@data-asset-id= 'contentfragment')]")
    public WebElementFacade ContentFragmentAdded;

    @FindBy(xpath = "//div[contains(@title,'${value}')]")
    public WebElementFacade addedComponent;

    @FindBy(xpath = "//*[contains(@value, 'cfcomponents/contentfragment') and @role]")
    public WebElementFacade ContentFragmentComponent;

    @FindBy(css = "button[title='Toggle Side Panel']")
    public WebElementFacade ToggleSidePanel;

    @FindBy(xpath = "//input[@placeholder= 'Search']")
    public WebElementFacade searchTextBoxUnderToggleSidePanel;

    @FindBy(xpath = "//coral-card[@draggable='true']")
    public WebElementFacade imageinsearchtextboxundertogglesidepanel;

    @FindBy(xpath = "//button[@title='Configure']")
    public WebElementFacade ConfigureButton;

    @FindBy(xpath = "//span[text()='Drop an asset here.']")
    public WebElementFacade dragToLocationForImage;

    @FindBy(xpath = "(//span[text()='Drop an asset here.'])[2]")
    public WebElementFacade DragToLocation_for_Image2;

    @FindBy(xpath = "//button[contains(@class,'cq-dialog-submit')][@title='Done']")
    public WebElementFacade DoneButton;

    @FindBy(xpath = "//*[@id='pageinfo-trigger']")
    public WebElementFacade PageInformation;

    @FindBy(xpath = "#pageinfo-trigger")
    public WebElementFacade pageInformationCss;

    @FindBy(xpath = "//button[@title='Publish Page']")
    public WebElementFacade PublishPage;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid_fullwidth/*')]")
    public WebElementFacade OneColumn_TwoColumnTemplate;


    @FindBy(xpath = "//coral-button-label[text()='Publish']")
    public WebElementFacade publishButtonForUnpublishedItems;

    @FindBy(xpath = "//*[@placeholder='Enter or select Content Fragment' and @autocomplete]")
    public WebElementFacade ContentFragmentTextBox;

    @FindBy(xpath = "//coral-button-label[text()='Add']")
    public WebElementFacade AddButton_ContentFragmentElements;

    @FindBy(xpath = "//*[@placeholder='Select']")
    public WebElementFacade SelectDropdown_AddElements_ContentFragment;

    @FindBy(xpath = "//coral-selectlist-item[@value='articleContent']")
    public WebElementFacade ArticleContentElementDropdown;

    @FindBy(xpath = "//input[@value='singleText']")
    public WebElementFacade SingleElement;

    @FindBy(xpath = "//input[@value='multi']")
    public WebElementFacade MultipleElements;

    @FindBy(xpath = "//coral-tab-label[text()='Properties']")
    public WebElementFacade PropertiesTab_contentFragment;


    @FindBy(xpath = "//coral-tab-label[text()='Content Fragment Card Settings']")
    public WebElementFacade ContentFragmentCardSettings;

    @FindBy(xpath = "//coral-select[@name='./listFrom']/button")
    public WebElementFacade pageDropDownButton;



    @FindBy(xpath ="//span[text()='Child pages']" )
    public WebElementFacade ChildPagesDropwdown;

    @FindBy(xpath = "//coral-selectlist-item[text()='Fixed list']")
    public WebElementFacade FixedPagesDropwdown;

    @FindBy(xpath = "//coral-button-label[text()= 'Add'][1]")
    public WebElementFacade AddButton_GeneralCards;

    @FindBy(xpath = "/html/body/coral-dialog/div[2]/form/coral-dialog-content/div/coral-tabview/coral-panelstack/coral-panel[1]/coral-panel-content/div/div/div[1]/foundation-autocomplete/div/div/span/button")
    public WebElementFacade OpenSelectDialog_ContentFragment;

    @FindBy(xpath = "//div[@class='foundation-collection-item-title' and contains(text(),'Test Automation')]")
    public WebElementFacade TestAutomation_popup;

    @FindBy(xpath = "//coral-button-label[text()='Select']")
    public WebElementFacade SelectButton_popup;

    @FindBy(xpath = "//coral-columnview-item-content[@title='UNSW']")
    public WebElementFacade UNSWFolderExFragment;

    @FindBy(xpath = "//coral-columnview-item-content[@title='Test Automation']")
    public WebElementFacade testAutomationFolderExFragment;

    @FindBy(xpath = "//*[@title='automation']")
    public WebElementFacade AutomationFolder;

    @FindBy(xpath = "//*[@data-foundation-collection-item-id='/content/dam/text/en/automation/${value}']//coral-columnview-item-thumbnail")
    public WebElementFacade contentFragmentLinkInPopup;


    @FindBy(xpath = "//span[text()='Content Fragment List']")
    public WebElementFacade TypeDropdown_GeneralCards;

    @FindBy(xpath = "//coral-selectlist-item[text()='Manual Settings']")
    public WebElementFacade ManualSettingsDropdown;

    @FindBy(xpath = "//coral-tab-label[text()='Manual Settings']")
    public WebElementFacade ManualSettingsTab;

    @FindBy(xpath = "//input[@name='./manualCards@Delete']//preceding::button[1]")
    public WebElementFacade AddButton_ManualSettingsContentFragment;

    @FindBy(xpath = "//input[@name='./manualCards/item0/heading']")
    public WebElementFacade HeadingTextField_contnetFragment;

    @FindBy(xpath = "//input[@name='./manualCards/item0/altText']")
    public WebElementFacade AltTextField_contnetFragment;

    @FindBy(xpath = "//textarea[@name='./manualCards/item0/standFirstText']")
    public WebElementFacade StandFirstTextField_contnetFragment;

    @FindBy(xpath = "//div[@class='coral-InputGroup']//input[@is='coral-textfield' and @aria-required='true' and @aria-invalid]//following::button[1]")
    public WebElementFacade Link_OpenSelectionDialog_GeneralCards;

    @FindBy(xpath = "//div[@class='coral-InputGroup']//input[@is='coral-textfield' and @aria-required='true' and @aria-invalid]")
    public WebElementFacade LinkField_contnetFragment;

    @FindBy(xpath = "//input[@name='./manualCards/item0/category']")
    public WebElementFacade CategoryField_contnetFragment;

    @FindBy(xpath = "//input[@name='./manualCards/item0/category']//following::input[2]")
    public WebElementFacade NewsDateField_contnetFragment;

    @FindBy(xpath = "//coral-button-label[text()= 'Add'][2]")
    public WebElementFacade AddButton_ManualSettingsGeneralCard;

    @FindBy(xpath = "//input[@name='./manualCards/item0/heading']//following::coral-icon[@icon='alert'][1]")
    public WebElementFacade HeadingText_MandatoryAlert;

    @FindBy(xpath = "//input[@name='./manualCards/item0/altText']//following::coral-icon[@icon='alert'][1]")
    public WebElementFacade AltText_MandatoryAlert;

    @FindBy(xpath = "//input[@name='./manualCards/item0/link']//following::coral-icon[@icon='alert'][1]")
    public WebElementFacade Link_MandatoryAlert;

    @FindBy(xpath = "//coral-multifield-item-content//coral-fileupload/following-sibling::coral-tooltip")
    public WebElementFacade Image_MandatoryAlert;

    @FindBy(xpath = "//img[contains(@src,'/content/unsw-dex-reference/en.thumb.48.48.png')]")
    public WebElementFacade referenceSitePopupButtonComponent;

    @FindBy(xpath = "//label[@class='coral-Form-fieldlabel' and text()='Link *']//following::span[1]")
    public WebElementFacade LinkFieldInternalPages;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-left-col/*')]")
    public WebElementFacade standardTemplateLeftLaytout;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-right-col/*')]")
    public WebElementFacade standardTemplateRightLayout;

    @FindBy(xpath = "//div[contains(@data-path,'/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-full-top/*')]")
    public WebElementFacade standardTemplateFullwidthLayout;

    @FindBy(xpath = "//input[@placeholder='Experience fragment variation path']//following::button[1]")
    public WebElementFacade OpenSelectionDialog_ExperienceFragment_StandardPage;

    @FindBy(xpath = "//div[@class='foundation-collection-item-title' and contains(text(),'DEX Reference Site')]")
    public WebElementFacade DEXReferenceSiteFolder;

    @FindBy(xpath = "//div[contains(@data-path,'/master/jcr:content/root/*')]")
    public WebElementFacade RootColumn_ExperienceFragment;

    @FindBy(xpath = "(//div//div[@data-asset-id='experiencefragment'])[1]")
    public WebElementFacade ExperienceFragmentRoot_ExperienceFragment;

    @FindBy(xpath = "//coral-selectlist-item[text()='Column Layout']")
    public WebElementFacade ColumnLayout_dropdown;

    @FindBy(xpath = "//div[@data-path = '/content/unsw-dex-reference/en/test-automation/standardpage_icontiles/jcr:content/root/responsivegrid-layout-fixed-width/responsivegrid-full-top' and @title = 'Layout Container [Root]']")
    public WebElementFacade ColumnLayoutAdded;

    @FindBy(xpath = "//coral-select[@name='./columnConfig']//coral-selectlist-item[text()='${value}']")
    public WebElementFacade columnLayoutDropdown;



    @FindBy(xpath = "//coral-search[@placeholder='Enter Keyword']//following::coral-selectlist-group//coral-selectlist-item")
    public WebElementFacade selectDropdownComponent;

    @FindBy(xpath = "//input[@name='./dropdownfilterkey']")
    public WebElementFacade dropdownKey;

    @FindBy(xpath = "//input[@name='./dropdownLabel']")
    public WebElementFacade dropdownlabel;

    @FindBy(xpath = "//coral-select[@name='./componentType']")
    public WebElementFacade componentTypeDropdown;

    @FindBy(css = "coral-selectlist-item[value='manual']")
    public WebElementFacade componentOptionManual;

    @FindBy(css = "coral-selectlist-item[value='contentfragment']")
    public WebElementFacade componentOptionContentFragment;

    @FindBy(xpath = "//coral-tab-label[contains(text(),'Manual Settings')]")
    public WebElementFacade manualSettingTab;

    @FindBy(xpath = "//coral-tab-label[normalize-space()='Content Fragment']")
    public WebElementFacade contentFragmentTab;

    @FindBy(xpath = "//coral-tab-label[contains(text(),'General')]")
    public WebElementFacade generalTabSettings;

    @FindBy(xpath = "//*[@title='${value}']")
    public WebElementFacade clickOnAddedComponent;



}



