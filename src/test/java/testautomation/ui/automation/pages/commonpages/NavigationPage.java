package testautomation.ui.automation.pages.commonpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;



public class NavigationPage extends PageObject {

    //Map Objects defined in ObjectRepository to a WebElementFacade for enabling UI interaction

    @FindBy(xpath = "//*[@class='globalnav-homecard-title' and text()='Sites']")
    public WebElementFacade sitesFolder;

    @FindBy(xpath = "//*[(@title='Test Automation')]")
    public WebElementFacade testAutomationfolder;


    @FindBy(css = "#granite-shell-actionbar > betty-titlebar-secondary > button.granite-collection-create.foundation-toggleable-control.coral3-Button.coral3-Button--primary")
    public WebElementFacade createButton;

    @FindBy(xpath = "//coral-list-item-content[@class='coral3-BasicList-item-content' and contains(text(),'Page')]")
    public WebElementFacade pageOptionUnderCreateButton;


    @FindBy(xpath = "//button[contains(@class,'selectall')]")
    public WebElementFacade selectAllButtonUnderNavigationPage;


    @FindBy(xpath = "//coral-actionbar-primary[@role='toolbar']//button[@title='More']")
    public WebElementFacade moreOption;

    @FindBy(xpath = "//*[@icon='delete' and @type]")
    public WebElementFacade deleteButton;

    @FindBy(css = "body > coral-dialog > div.coral3-Dialog-wrapper > coral-dialog-footer > button.coral3-Button.coral3-Button--warning")
    public WebElementFacade deleteButtonConfirm;

    @FindBy(xpath = "/html/body/coral-dialog/div[2]/coral-dialog-footer/button[2]")
    public WebElementFacade forceDelete;

    @FindBy(xpath = "//button[@data-foundation-command-label='Edit']")
    public WebElementFacade EditButton;

    @FindBy(xpath = "//*[(@data-foundation-layout-columnview-columnid='/content/unsw-dex-reference/en/test-automation')]//following::coral-columnview-column-content//coral-columnview-item[1]//coral-columnview-item-thumbnail//img")
    public WebElementFacade firstPage_underTestAutomationfolder;

    // common objects across multiple pages

    @FindBy(xpath = "//div[@class='foundation-collection-item-title' and contains(text(),'DEX Reference Site')]")
    public WebElementFacade dexreferencesitefolder;

    @FindBy(xpath = "//div[contains(@title,'Reference Home') or contains(@title,'Reference Site') and not(contains(@title,'DEX'))]")
    public WebElementFacade referenceSiteButtonComponent;

    @FindBy(xpath = "//div[(text()='Reference Home') or (text()='Reference Site')]")
    public WebElementFacade referenceSiteSearchBoxXpath;

}
