//import static testautomation.objectRepository.ObjectRepository.StudentProgram.AdminApp_SwitchApp_xpath;
//import org.openqa.selenium.interactions.Locatable;
//import static testautomation.objectRepository.ObjectRepository.HomPageLocators.usernameLoginPage_css;
//import static testautomation.objectRepository.ObjectRepository.FSRpageLocators.FSRhomepage_xpath;
//import static testautomation.objectRepository.ObjectRepository.HomePageLocators.Popup_xpath;
//import static testautomation.objectRepository.ObjectRepository.HomePageLocators.SwitchApp_xpath;


package testautomation.ui.automation.pages.commonpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.util.EnvironmentVariables;




public class Loginpage extends PageObject {

    private EnvironmentVariables environmentVariables;

    //Map Objects defined in ObjectRepository to a WebElementFacade for enabling UI interaction

    @FindBy(css = "#coral-id-1 > div > coral-accordion-item-label")
    public WebElementFacade SignInWithAdobeButton;

    @FindBy(css = "#username")
    public WebElementFacade Username;

    @FindBy(css = "#password")
    public WebElementFacade Password;

    @FindBy(css = "#submit-button")
    public WebElementFacade SignInButton;



}
