package testautomation.ui.automation.pages.commonpages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;


public class TemplatesPage extends PageObject {



    @FindBy(xpath = "//img[contains(@src,'/content/unsw-dex-reference/en/test-automation/${value}.thumb.48.48.png')]")
    public WebElementFacade imagePageXpath;

    //    Map Objects defined in ObjectRepository to a WebElementFacade for enabling UI interaction

    @FindBy(xpath = "//coral-masonry-item[@data-foundation-collection-item-id='/conf/unsw-common/settings/wcm/templates/one-column-template']")
    public WebElementFacade OneColumnStarterTemplate;

    @FindBy(xpath = "//coral-masonry-item[@data-foundation-collection-item-id='/conf/unsw-common/settings/wcm/templates/unsw-common---two-column-template']")
    public WebElementFacade TwoColumnStarterTemplate;

    @FindBy(xpath = StandardPageTemplate_xpath)
    public WebElementFacade StandardPageTemplate;

    @FindBy(xpath = DegreePageTemplate_xpath)
    public WebElementFacade DegreePageTemplate;

    @FindBy(xpath = NextButton_underCreatePage_xpath)
    public WebElementFacade NextButton_underCreatePage;

    @FindBy(xpath = TitleTextBox_xpath)
    public WebElementFacade TitleTextBox;

    @FindBy(xpath = NameTextBox_xpath)
    public WebElementFacade NameTextBox;

    @FindBy(xpath = PageTitleTextBox_xpath)
    public WebElementFacade PageTitleTextBox;

    @FindBy(xpath = NavigationTitleTextBox_xpath)
    public WebElementFacade NavigationTitleTextBox;

    @FindBy(xpath = CreateButton_underCreatePage_xpath)
    public WebElementFacade CreateButton_underCreatePage;

    @FindBy(xpath = OpenButton_underCreatePage_xpath)
    public WebElementFacade OpenButton_underCreatePage;

    @FindBy(css = DoneButton_CreateButton_css)
    public WebElementFacade DoneButton_CreateButton;



    public static final String StandardPageTemplate_xpath = "//coral-card-title[contains(text(),'Standard Page Template')]";//coral-card-asset//img[contains(@src,'/unsw-common---standard-page-template--design-v3-/thumbnail.png')]
    public static final String DegreePageTemplate_xpath = "//img[@src = '/conf/unsw-common/settings/wcm/templates/unsw-common---degree-template/thumbnail.png']";
    public static final String NextButton_underCreatePage_xpath = "//*[@data-foundation-wizard-control-action='next' and @type='button']";
    public static final String TitleTextBox_xpath = "//input[@name=\"./jcr:title\"]";
    public static final String NameTextBox_xpath = "//input[@name=\"pageName\"]";
    public static final String PageTitleTextBox_xpath = "//input[@name=\"./pageTitle\"]";
    public static final String NavigationTitleTextBox_xpath = "//*[@data-cq-msm-lockable ='navTitle']";
    public static final String CreateButton_underCreatePage_xpath = "//coral-button-label[contains(text(), 'Create')]";
    public static final String OpenButton_underCreatePage_xpath = "//coral-button-label[contains(text(), 'Open')]";
    public static final String DoneButton_CreateButton_css = "body > coral-dialog > div.coral3-Dialog-wrapper > coral-dialog-footer > button.coral3-Button.coral3-Button--secondary";


}

