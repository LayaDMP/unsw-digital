package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import net.thucydides.core.annotations.Step;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.components.HeroVideoPage;

import java.time.format.DateTimeFormatter;

import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.today;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.YOUTUBE_ID;


public class HeroVideoActions extends BaseInteractions {

    HeroVideoPage heroVideoPage;
    ReferenceSiteActions referenceSiteActions;

    @Step
    public void selectHeroVideoSettings(){
        waitFor(heroVideoPage.heroVideoSettings);
        heroVideoPage.heroVideoSettings.waitUntilClickable().click();
        referenceSiteActions.clickOnConfigureButton();
        heroVideoPage.textAndContentTab.waitUntilClickable().click();
    }
    public void enterTitle(String title){
    heroVideoPage.titleTxt.sendKeys(title);
    }
    public void enterSubTitle(String subTitle){
    heroVideoPage.subTitleTxt.sendKeys(subTitle);
    }
    public void enterAuthorName(String author){
    heroVideoPage.authorNameTxt.sendKeys(author);
    }
    public void enterpublishedDate(String Date){
    heroVideoPage.publishedDateTxt.sendKeys(Date);
    }
    public void clickShowPublishedDate(){
        clickOn(heroVideoPage.showPublishedDateCheckBox);
    }
    public void setYoutubeId(String youtubeId){
        heroVideoPage.youTubeVideoIdtxt.sendKeys(youtubeId);
    }
    public void clickOnVideoTab(){
        clickOn(heroVideoPage.videoTab);
    }
    @Step
    public Boolean getPublishedTitle(String title){
        return getDynamicElement(heroVideoPage.publishedTitle,title).isDisplayed();
    }
    public String getPublishedSubTitle(){
        return heroVideoPage.publishedSubTitle.getText();
    }
    public String getPublishedAuthorName(){
        return heroVideoPage.publishedAuthorName.getText();
    }
    public String getPublishedDate(){
        return heroVideoPage.publishedDate.getText();
    }
    @Step
    public Boolean getPublishedYouTubeId(String youtube){
        return getDynamicElement(heroVideoPage.publishedYouTubeId,youtube).isDisplayed();
    }

    @Step
    public void setHeroVideoDefaultContent(Map<String,String> heroVidewithValues)
    {
        enterTitle(heroVidewithValues.get("title"));
        enterSubTitle(heroVidewithValues.get("subTitle"));
        enterAuthorName(heroVidewithValues.get("authorName"));
        enterpublishedDate(heroVidewithValues.get("publishedDate"));
        clickShowPublishedDate();
        clickOnVideoTab();
        setYoutubeId(heroVidewithValues.get("youTubeId"));
    }
}
