package testautomation.ui.automation.actions.componentactions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.VideoStandFirstPage;

public class VideoPlayerStandfirstActions extends BaseInteractions {
    VideoStandFirstPage videoStandFirstPage;
    ReferenceSitePage referenceSitePage;

    public void clickOnVideoplayerStandfirstComponentAdded() {
        videoStandFirstPage.VideoplayerStandfirstComponentAdded.waitUntilClickable().click();
    }

    public void addVideoStandfirstTitle(String title) {
        videoStandFirstPage.VideoplayerStandfirstComponent_Title.waitUntilClickable().type(title);

    }

    public void addVideoStandfirstStandfirst(String standfirst) {
        videoStandFirstPage.VideoplayerStandfirstComponent_Standfirst.waitUntilClickable().type(standfirst);
    }

    public void addVideoStandfirstButtonlabel(String button_label) {
        videoStandFirstPage.VideoplayerStandfirstComponent_Buttonlabel.waitUntilClickable().type(button_label);

    }

    public void addVideoStandfirstButtonURL(String button_uRl) {

        if (button_uRl.contains("http")) {
            videoStandFirstPage.VideoplayerStandfirstComponent_ButtonURL.waitUntilClickable().type(button_uRl);
            videoStandFirstPage.VideoplayerStandfirstComponent_ButtonURL.type(button_uRl);
            videoStandFirstPage.VideoplayerStandfirstComponent_Buttonlabel.click();
        }

        else {

            videoStandFirstPage.VideoplayerStandfirstComponent_ButtonURL_OpenSelectionDialog.click();

            WebDriver driver = getDriver();
            Actions act = new Actions(driver);
            waitFor(1).seconds();
            act.moveToElement(videoStandFirstPage.DexReferencePopup_VideoplayerStandfirst, 1, 5).build().perform();
            act.click().build().perform();
            referenceSitePage.SelectButton_popup.waitUntilClickable().click();;
        }
    }

    public void addVideoStandfirstMinimumLinkLabel(String minimum_link_label) {
        videoStandFirstPage.VideoplayerStandfirstComponent_MinimumLinkLabel.type(minimum_link_label);


    }

    public void addVideoStandfirstMinimumLinkURL(String minimum_link_uRl) {

        if (minimum_link_uRl.contains("http")) {
            videoStandFirstPage.VideoplayerStandfirstComponent_MinimumLinkURL.waitUntilClickable().type(minimum_link_uRl);
            videoStandFirstPage.VideoplayerStandfirstComponent_MinimumLinkURL.type(minimum_link_uRl);

            videoStandFirstPage.VideoplayerStandfirstComponent_Buttonlabel.waitUntilClickable().click();
            waitFor(1).second();
        }

        else {

            videoStandFirstPage.VideoplayerStandfirstComponent_MinimumLinkURL_OpenSelectionDialog.click();
            videoStandFirstPage.DexReferencePopup_VideoplayerStandfirst.waitUntilClickable().click();
            referenceSitePage.SelectButton_popup.waitUntilClickable().click();
        }
    }

    public void addVideoplayerStandfirstYoutubeId(String youtube_video_id) {
        videoStandFirstPage.VideoplayerStandfirstComponent_YoutubeID.type(youtube_video_id);

    }


    public void addVideoStandfirstShowVideoTranscript(String showTranscript) {


        if (showTranscript.equals("Yes")) {
            videoStandFirstPage.VideoplayerStandfirstComponent_Buttonlabel.waitUntilClickable().click();
            waitFor(1).second();
            videoStandFirstPage.VideoplayerStandfirstComponent_ShowTranscriptCheckbix.click();
        } else {
            System.out.print("Do not click");
        }
    }

    public void addVideoStandfirstTrasnscriptTitle(String transcript_title) {
        videoStandFirstPage.VideoplayerStandfirstComponent_TranscriptTitle.type(transcript_title);
    }

    public void addVideoStandfirstTrasnscriptOfTheVideo(String transcript_of_the_video) {
        videoStandFirstPage.VideoplayerStandfirstComponent_TranscriptOfTheVideo.type(transcript_of_the_video);
    }

    public String verifyVideoStandfirstTitlePublishedPage() {
       return videoStandFirstPage.publishedVideoStandPlayerTitle.getText();
    }

    public String verifyVideoStandfirstStandfirstPublishedPage() {
        return videoStandFirstPage.publishedVideoStandPlayerStandFirst.getText();
    }

    public String verifyVideoStandfirstButtonlabelPublishedPage() {
        return videoStandFirstPage.publishedVideoStandPlayerbuttonlabel.getText();
    }

    public String verifyVideoStandfirstButtonURLPublishedPage() {
        return videoStandFirstPage.publishedVideoStandPlayerbuttonlabel.getAttribute("href");
    }

    public String verifyVideoStandfirstMinimumLinkLabelPublishedPage() {
        return videoStandFirstPage.publishedVideoStandPlayerMiniLabel.getText();
    }

    public String verifyVideoStandfirstMinimumLinkURLPublishedPage() {
        return videoStandFirstPage.publishedVideoStandPlayerMiniLabel.getAttribute("href");
    }

    public String verifyVideoStandfirstYoutubeIdPublishedPage() {
       return videoStandFirstPage.publishedVideoStandPlayerYoutubeId.getAttribute("src");

    }

    public String verifyVideoStandfirstShowVideoTranscriptPublished() {
        return videoStandFirstPage.publishedVideoStandPlayerTranscripttitle.getText();
    }

    public String verifyVideoStandfirstTrasnscriptOfTheVideoPubpishedPage() {
        clickOn(videoStandFirstPage.publishedVideoStandPlayerTranscripttitle);
        return videoStandFirstPage.publishedVideoStandPlayerTranscriptContent.getText();
    }

}
