package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.FlickrPage;

public class FlickrActions extends BaseInteractions {
    FlickrPage flickrPage;

    public void editFlickrOption(String urlText, String flickrTitleText, String flickrImg) {
        flickrPage.flickrURL.type(urlText);
        flickrPage.flickrTitle.type(flickrTitleText);
        flickrPage.flickrImage.type(flickrImg);
    }

    public void selectFlickrDropDown() {
        flickrPage.dropdownEmbeddable.waitUntilClickable().click();
        flickrPage.dropDownFlickr.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return flickrPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedFlickrFrame() {
        waitForPageToLoadWith(flickrPage.publishedFlickrFrame);
        getDriver().switchTo().frame(flickrPage.publishedFlickrFrame);
        return flickrPage.publishedFlickrImage.waitUntilVisible().isVisible();

    }

}
