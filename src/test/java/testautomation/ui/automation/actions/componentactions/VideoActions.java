package testautomation.ui.automation.actions.componentactions;

import org.openqa.selenium.By;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.VideoPage;

public class VideoActions extends BaseInteractions {

        ReferenceSitePage referenceSitePage;
        VideoPage videoPage;

    public void clickOnConfigureVideoComponent(String cropMode, String videoLink) {
        waitABit(500);
        waitforPageTodisplayWith(videoPage.VideoComponentAdded);
        videoPage.VideoComponentAdded.waitUntilClickable().click();
        referenceSitePage.ConfigureButton.click();
        videoPage.YoutubeVideoID_textBox.waitUntilClickable().click();
        videoPage.YoutubeVideoID_textBox.type(videoLink);
        getDynamicElement(videoPage.selectCrop,cropMode).click();
        referenceSitePage.DoneButton.click();
    }

    public boolean verifyVideoOnPublishedSite(String videoLink) {
        return getDynamicElement(videoPage.video,videoLink ).isVisible();
    }



}
