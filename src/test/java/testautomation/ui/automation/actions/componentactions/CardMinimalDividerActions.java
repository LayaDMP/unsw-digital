package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.CardMinimalDividerPage;

public class CardMinimalDividerActions extends BaseInteractions{
    CardMinimalDividerPage cardMinimalDividerPage;

    public void clickOnAddButtonCardMinimalDivider(){cardMinimalDividerPage.addButtonCardMinimal.waitUntilClickable().click();}

    public void editCardMinimalDividerComponent(String heading, String desc, String url){
        waitForJSPageLoadComplete();
        cardMinimalDividerPage.headingCardMinimalDivider.type(heading);
        cardMinimalDividerPage.shortDescCardMinimalDivider.type(desc);
        cardMinimalDividerPage.urlCardMinimalDivider.type(url);
    }
    public boolean verifyPublishedCardMinimalComponent(){
        return cardMinimalDividerPage.publishedCardMinimalComponent.isVisible();
            }
    public boolean verifyPublishedCardMinimalHeader() {
        return cardMinimalDividerPage.publishedCardMinimalHeader.isVisible();
    }

    public boolean verifyPublishedCardMinimalDesc(){
        return cardMinimalDividerPage.publishedCardMinimalDesc.isVisible();
    }
    public boolean verifyPublishedCardMinimalUrl()
    {
        return cardMinimalDividerPage.publishedCardMinimalUrl.isPresent();
    }
}
