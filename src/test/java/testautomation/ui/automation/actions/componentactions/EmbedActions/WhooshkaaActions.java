package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.WhooshkaaPage;

public class WhooshkaaActions extends BaseInteractions {
    WhooshkaaPage whooshkaaPage;

    public void editWhooshkaaOption(int wID) {
        whooshkaaPage.whooshkaaID.type(Integer.toString(wID));

    }

    public void selectWhooshkaaDropDown() {
        whooshkaaPage.dropdownEmbeddable.waitUntilClickable().click();
        whooshkaaPage.dropDownWhooshkaa.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return whooshkaaPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedWhooshkaaFrame() {
        waitForPageToLoadWith(whooshkaaPage.publishedWhooshkaaFrame);
        getDriver().switchTo().frame(whooshkaaPage.publishedWhooshkaaFrame);
        return whooshkaaPage.publishedWhooshkaa.waitUntilVisible().isVisible();

    }
}
