package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;

import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.pages.components.AnnouncementPage;

import java.util.*;
import java.util.stream.Collectors;


import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.PATH_TO_TEST_AUTOMATION;

public class AnnouncementActions extends BaseInteractions {

    AnnouncementPage announcementPage;
    String randomText= RandomStringUtils.randomAlphanumeric(500)
            .replaceAll("[a-f]"," ");

    Map<String,String> configureProperties=new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    String type="";
    String link="";
    String tab="";
    String icon="";
    String title="Long Announcement Title";
    String clickLink="Click here";
    int buttonSize=0;
    List<String> iconsList=new ArrayList<>();
    MultiValueMap buttonConfig=new MultiValueMap();





    public void selectDropdownOption(String type){
        clickOn(announcementPage.announcementTypeDropdown);
        clickOn(getDynamicElement(announcementPage.dropdownOption,type));
    }

    @Step
    public void configureHyperlink(String url, String tab){
        String linkOpenOn=setValueForHyperlinkDropDown(tab);
        announcementPage.hyperlinkDropdown.click();
        announcementPage.hyperlinkURLTextbox.sendKeys(url);
        announcementPage.hyperlinkAltTextbox.sendKeys("Hyperlink text");
        announcementPage.hyperlinkTargetDropdown.click();
        getDynamicElement(announcementPage.selectHyperlinkDropDownOption,
                linkOpenOn).click();
        announcementPage.hyperlinkApplyButton.click();
    }

    public void inputTextboxContentAs(String type){
        if(type.contains("short")) {
            clickOn(announcementPage.pageAnnouncementShortTextBox);
            announcementPage.pageAnnouncementShortTextBox.sendKeys(clickLink);
        }else
        {
            clickOn(announcementPage.pageAnnouncementLongTextBox);
            announcementPage.pageAnnouncementLongTextBox.sendKeys(clickLink);
        }
    }


    @Step
    public void configureTypeWith(Map<String,String> valuesToconfigure){
        configureProperties.putAll(valuesToconfigure);
        type=configureProperties.get("type").toLowerCase();
        if (type.contains("short")){
        setShortComponentProperties();
        shortComponentConfiguration();
        }else{
            setLongComponentProperties();
        }
    }

    public void shortComponentConfiguration(){
        setShortComponentProperties();
        commonSetupPageAnnouncement();
        if (icon.contains("enabled")){
            announcementPage.shortCloseIconCheckBox.click();
        }
    }


    public void commonSetupPageAnnouncement(){

        if (link.contains("external")){
            link=GENERIC_EXTERNAL_LINK;
        }else{
            link=PATH_TO_TEST_AUTOMATION;
        }
        selectDropdownOption(type);
        inputTextboxContentAs(type);
        if(type.contains("short")) {
            announcementPage.pageAnnouncementShortTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
            configureHyperlink(link, tab);
            announcementPage.pageAnnouncementShortTextBox.click();
            announcementPage.pageAnnouncementShortTextBox.sendKeys(" for more Updates");
        }else
        {
            announcementPage.pageAnnouncementLongTextBox.sendKeys(Keys.chord(Keys.CONTROL,"a"));
            configureHyperlink(link,tab);
            announcementPage.pageAnnouncementLongTextBox.click();
            announcementPage.pageAnnouncementLongTextBox.sendKeys(" " + randomText);
        }

    }
    public void setShortComponentProperties(){
        link=configureProperties.get("link").toLowerCase();
        tab= configureProperties.get("tab").toLowerCase();
        icon=configureProperties.get("icon").toLowerCase();
    }

    public void setLongComponentProperties(){
        link=configureProperties.get("link").toLowerCase();
        icon=configureProperties.get("icon").toLowerCase();
    }
    public List<String> isPublishedTextPresent(){
        List<String> publishedText=new ArrayList<>();
        announcementPage.publishedText.forEach(n->n.getText());
        return publishedText;
    }

    public boolean isPublishedLinkPresent(){
        return announcementPage.publishedLink.isVisible();
    }

    public boolean validateCloseIcon(){
        waitforPageTodisplayWith(announcementPage.publishedCloseIcon);
        announcementPage.publishedCloseIcon.click();
        waitABit(200);
        return isOptionalElementDisplayed(announcementPage.publishedCloseIcon);
    }
    public boolean validateTab(){
        String currentURL=getDriver().getCurrentUrl();
        Set<String> existingHandles=getDriver().getWindowHandles();
        String currentTitle=getTitle();
        boolean flag=true;
        String currentHandle=getDriver().getWindowHandle();
        announcementPage.publishedLink.click();
        waitForJSPageLoadComplete();
        if (tab.contains("new")) {
            switchToTab(currentHandle,existingHandles);
            waitForTitleToDisappear(currentTitle);
            flag=!getDriver().getCurrentUrl().equals(currentURL);
            getDriver().switchTo().window(currentHandle);
        }else{
            flag=!getDriver().getCurrentUrl().equals(currentURL);
            getDriver().navigate().back();
            waitForJSPageLoadComplete();
        }

        return flag;
    }

    public List<String> validateIcons(){
        announcementPage.publishedIcon.forEach(n->iconsList.add(n.getAttribute("class")));
        return iconsList;
        }

    public void validatePublishedButtonLink(){

    }


    public boolean validateButtonSize(){
        Integer buttonNumbers=announcementPage.publishedButtons.size();
        return buttonNumbers.equals(buttonSize);
    }




    public String setValueForHyperlinkDropDown(String value) {
        Map<String,String> test=new HashMap<>();
        test.put("same tab","self");
        test.put("","self");
        test.put("new tab","blank");
        test.put("parent frame","parent");
        test.put("top frame","top");
        return test.get(value);
    }

    public void configureLongComponent(List<Map<String, String>> buttonSize){

        commonSetupPageAnnouncement();
        announcementPage.longAnnouncementTitle.sendKeys(title);
        configureButtons(buttonSize);
        if (icon.contains("enabled")){
            announcementPage.longCloseIconCheckBox.click();
        }
    }

    public void configureButtons(List<Map<String, String>> buttonDetails){

        for (Map<String,String> details: buttonDetails) {
            buttonConfig.put("button colour", details.get("button colour"));
            buttonConfig.put("button style", details.get("button style"));
            buttonConfig.put("label style", details.get("label style"));
            buttonConfig.put("button size", details.get("button size"));
            buttonConfig.put("button icon", details.get("button icon"));
        }

            buttonSize=buttonDetails.size();
        int i=0;
        for (Map<String,String> details: buttonDetails) {

            announcementPage.longAddButton.click();
            waitforPageTodisplayWith(getDynamicElement(announcementPage.buttonTextfield,String.valueOf(i)));
            getDynamicElement(announcementPage.buttonTextfield,String.valueOf(i))
                    .sendKeys("Long Page Title"+i);
            waitForPageToLoadWith(getDynamicElement(announcementPage.buttonLinkfield,String.valueOf(i)));
            getDynamicElement(announcementPage.buttonLinkfield,String.valueOf(i)).typeAndTab(link);
            waitforPageTodisplayWith(getDynamicElement(announcementPage.buttonColorDropDown,String.valueOf(i)));
            getDynamicElement(announcementPage.buttonColorDropDown,String.valueOf(i))
                    .waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonColorDropDownSelect,
                    details.get("button colour").toLowerCase()).waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonStyleDropDown,String.valueOf(i))
                    .waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonStylerDropDownSelect,
                    details.get("button style").toLowerCase()).waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonLabelDropDown,String.valueOf(i))
                    .waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonLabelDropDownSelect,
                    details.get("label style")).waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonSizeDropDown,String.valueOf(i))
                    .waitUntilClickable().click();
            getDynamicElement(announcementPage.buttonSizeDropDownSelect,
                    details.get("button size").toLowerCase()).waitUntilClickable().click();

            getDynamicElement(announcementPage.buttonIcon,String.valueOf(i)).waitUntilClickable().click();
            waitForPageToLoadWith(getDynamicElement(announcementPage.buttonIconDropDownSelect,
                    details.get("button icon")));
            getDynamicElement(announcementPage.buttonIconDropDownSelect,
                    details.get("button icon")).waitUntilClickable().click();

            i++;
        }
    }





}
