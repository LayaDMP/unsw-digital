package testautomation.ui.automation.actions.componentactions.TeaserCard;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TeaserCard.GeneralTeaserCardPage;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

public class GeneralTeaserActions extends BaseInteractions {
    GeneralTeaserCardPage generalTeaserCardPage;
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;


    public void editGeneralTeaserCard(Map<String, String> generalTeaserDetails){
        Map<String, String> gt = new HashMap<>(generalTeaserDetails);
        generalTeaserCardPage.teaserTypeDropDown.waitUntilClickable().click();
        generalTeaserCardPage.teaserTypeManualSelection.waitUntilClickable().click();
        generalTeaserCardPage.teaserEnableSquareTheme.waitUntilClickable().click();
        referenceSitePage.manualSettingTab.click();
        generalTeaserCardPage.teaserAddButton.waitUntilClickable().click();
        generalTeaserCardPage.teaserGeneralHeadingText.type(gt.get("generalTeaserHeading"));
        generalTeaserCardPage.teaserGeneralImage.type(gt.get("generalTeaserImage"));
        generalTeaserCardPage.teaserGeneralImageList.waitUntilClickable().click();
        generalTeaserCardPage.teaserGeneralAltText.waitUntilClickable().click();
        generalTeaserCardPage.teaserGeneralStandfirstText.type(gt.get("generalTeaserStandfirstText"));
        generalTeaserCardPage.teaserGeneralLinkText.type(gt.get("generalTeaserLinkText"));
        generalTeaserCardPage.teaserGeneralCategoryText.type(gt.get("generalTeaserCategoryText"));
        generalTeaserCardPage.teaserGeneralNewsDate.type(gt.get("generalNewsDate"));

    }
    public boolean publishedGeneralTeaserComponent(){
        return generalTeaserCardPage.publishedGeneralTeaserCardComponent.isVisible(); }

    public boolean publishedGeneralTeaserImage(){
        return generalTeaserCardPage.publishedGeneralTeaserCardImage.isVisible();}

    public boolean publishedGeneralTeaserStandfirst(){
        return generalTeaserCardPage.publishedGeneralTeaserCardStandfirst.isVisible();}

    public boolean publishedGeneralTeaserCategory(){
        return generalTeaserCardPage.publishedGeneralTeaserCardCategory.isVisible();}

    public boolean publishedGeneralTeaserDate(){
        return generalTeaserCardPage.publishedGeneralTeaserCardDate.isVisible();}

    }


