package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.FindAnAgentPage;

public class FindAnAgentActions extends BaseInteractions {
    FindAnAgentPage findAnAgentPage;

    public void editFindAnAgentPageOption(String fID) {
        findAnAgentPage.iFrameID.type(fID);

    }

    public void selectFindAnAgentPageDropDown() {
        findAnAgentPage.dropdownEmbeddable.waitUntilClickable().click();
        findAnAgentPage.dropDownIFrame.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return findAnAgentPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedFindAnAgentPageFrame() {
        waitForPageToLoadWith(findAnAgentPage.publishedFindAnAgentFrame);
        getDriver().switchTo().frame(findAnAgentPage.publishedFindAnAgentFrame);
        return findAnAgentPage.publishedFindAnAgent.waitUntilVisible().isVisible();

    }

}
