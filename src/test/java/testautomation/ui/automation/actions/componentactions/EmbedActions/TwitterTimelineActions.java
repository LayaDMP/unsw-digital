package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.TwitterTimelinePage;

public class TwitterTimelineActions extends BaseInteractions {
    TwitterTimelinePage twitterTimelinePage;


    public void selectTwittertimelineDropdown(){
        twitterTimelinePage.dropdownEmbeddable.waitUntilClickable().click();
        twitterTimelinePage.dropDownTwitterTimeline.waitUntilClickable().click();
    }
    public void editTwitterTimelineOption(String htmlText){
        twitterTimelinePage.pasteEmbedHtmlTwitterTimeline.type(htmlText);
    }
    public boolean publishedTwitterTimelineOption()
    {
        waitForPageToLoadWith(twitterTimelinePage.publishedTwitterTimelineFrame);
        getDriver().switchTo().frame(twitterTimelinePage.publishedTwitterTimelineFrame);
        return  twitterTimelinePage.publishedTwitterTimeline.waitUntilVisible().isVisible();}

    public boolean publishedEmbedGrid()
    { waitForJSPageLoadComplete();
        return twitterTimelinePage.publishedEmbedGrid.isVisible();}

}
