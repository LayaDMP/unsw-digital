package testautomation.ui.automation.actions.componentactions;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import framework.BaseInteractions;
import org.openqa.selenium.WebDriver;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.ImagePage;

import static org.junit.Assert.assertEquals;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;


public class ImageActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;

    ImagePage imagePage;

    ReferenceSiteActions referenceSiteActions;

    EnvironmentVariables environmentVariable;

    String caption="";

    @Step
    public void cropImage(String CropMode) {
        clickOnImage();
        clickOnEditButton();
        clickOnFullScreenButton();
        clickOnCropButton();
        selectCropMode(CropMode);
        confirmCrop();
    }
    @Step
    public boolean verifyImageOnPublishedPage(String image) {

        waitforPageTodisplayWith(getDynamicElement(imagePage.publishedImage,image));
        return getDynamicElement(imagePage.publishedImage,image).isVisible();

    }
    public void clickOnMetadataTab() {
        imagePage.metadataTab.waitUntilClickable().click();
    }

    public void clickOnImageIsDecorativeCheckbox() {
        imagePage.imageIsDecorative.waitUntilClickable().click();
    }

    @Step
    public boolean verifyAltTextImageComponent(String altText) {
        return  getDynamicElement(imagePage.altText,altText).isVisible();
    }

    @Step
    public boolean verifyCaptionImageComponent(String caption) {
        return  getDynamicElement(imagePage.caption,caption).isVisible();
    }

    @Step
    public void addExternalLinkImageComponent(String link) {
        imagePage.linkImageComponent.type(link);
        waitABit(200);
    }

    public void clickOnOpenSelectionDialogImageComponent() {
        imagePage.openSelectionDialogImageComponent.waitUntilClickable().click();
    }

    @Step
    public void clickOnConfigure() {
        imagePage.imageComponentAdded.waitUntilClickable().click();
        referenceSitePage.ConfigureButton.waitUntilClickable().click();
        referenceSitePage.dragToLocationForImage.waitUntilClickable();
    }

    @Step
    public void clickOnImage() {
        waitforPageTodisplayWith(imagePage.imageAddedOnImageComponent);
        imagePage.imageAddedOnImageComponent.click();
    }

    @Step
    public void clickOnEditButton() {

        imagePage.editOption.waitUntilClickable().click();
    }
    @Step
    public void clickOnFullScreenButton() {

        imagePage.fullScreen.click();
    }

    @Step
    public void clickOnCropButton() {
        waitFor(2).seconds();
        imagePage.startCropButton.waitUntilClickable().click();
    }

    @Step
    public void selectCropMode(String cropMode) {
        getDynamicElement(imagePage.cropModeSelect,cropMode).click();
    }

    @Step
    public void confirmCrop() {
        waitFor(2).seconds();
        imagePage.confirmCrop.waitUntilClickable().click();
        imagePage.exitFullScreen.waitUntilClickable().click();
    }

    @Step
    public boolean clickOnImagePublishedSite(String captionText) {
        this.caption=captionText;
        waitforPageTodisplayWith(getDynamicElement(imagePage.captionPublishedPage,caption));
        getDynamicElement(imagePage.captionPublishedPage,caption).click();
        return true;
    }
    public boolean verifyLinkPublishedPage(String link) {
        String navigatedURL ="";
        if (link.toLowerCase().contains("internal")|link.toLowerCase().contains("dex")) {
            String currentUrl = getDriver().getCurrentUrl();
            String env = EnvironmentSpecificConfiguration.from(environmentVariable)
                    .getProperty("environment");
            navigatedURL = env + ".reference.dex.unsw.edu.au/en";
            return currentUrl.contains(navigatedURL);

        }
        else  {
            waitABit(300);
            navigatedURL = getDriver().getCurrentUrl();
            return navigatedURL.contains(GENERIC_EXTERNAL_LINK);
        }
    }


    public boolean verfyAltTextPublishedPage(String altText) {
       return getDynamicElement(imagePage.altTextPublished,altText).isVisible();
    }

    public boolean verifyCaptionPublishedPage(String caption) {
       return getDynamicElement(imagePage.captionPublishedPage,caption).isVisible();

    }
}
