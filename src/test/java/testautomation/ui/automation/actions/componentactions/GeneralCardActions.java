package testautomation.ui.automation.actions.componentactions;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.GeneralCardsPage;

import java.util.ArrayList;
import java.util.List;


import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.*;


public class GeneralCardActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;
    NavigationActions navigationActions;
    GeneralCardsPage generalCardsPage;

    private EnvironmentVariables environmentVariables;

    public void clickOnGeneralComponent(){
        referenceSiteActions.clickOnAddedComponent("General Cards (v2)");
    }

    @Step
    public boolean verifyGeneralCardOnPublishedSite() {

        waitforPageTodisplayWith(generalCardsPage.generalCardsOnPublishedSite);
        return generalCardsPage.generalCardsOnPublishedSite.isVisible();
    }

    @Step
    public void verifySquareCardAlignmentOnPblishedSite() {
        /*  The following 2 classes should be together for te square card them to be applied to general cards:
           'card-general'& 'unsw-brand-card-theme'*/
        generalCardsPage.squarecardthemealignment.shouldBeVisible();
        /* check the square card theme is applied on the image*/
        generalCardsPage.squareImageOnPublishedSiteGeneralCards.shouldBeVisible();
    }

    @Step
    public void clickOnSquareCardTheme() {
        getDriver().switchTo().defaultContent();
        generalCardsPage.enableSqureCardTheme.waitUntilClickable().click();

    }

    @Step
    public void clickOnContentFragmentCardSettingsTab() {

        referenceSitePage.ContentFragmentCardSettings.waitUntilClickable().click();
    }

    @Step
    public void clickOnGeneralCardAddedOnPage() {

        generalCardsPage.generalCardsAdded.waitUntilClickable().click();
    }

    @Step
    public void click_on_generalCard_on_publishedSite() {
        generalCardsPage.generalCardsOnPublishedSite.click();
    }

    @Step
    public boolean verifyNavigatedPageFromGeneralCard(String link) {
        boolean flag=false;

        if (link.toLowerCase().contains("external")) {
            String CurrentURL = getDriver().getCurrentUrl();
            flag=CurrentURL.contains(GENERIC_EXTERNAL_LINK);

        } else {
            String NavigatedURL = getDriver().getCurrentUrl();
            String env = EnvironmentSpecificConfiguration.from(environmentVariables)
                    .getProperty("environment");
            flag=NavigatedURL.contains("reference.dex.unsw.edu.au/en/test-automation/");
        }
        return flag;
    }

    @Step
    public void addPathToContentFragmentPage(String templateName) {
        clickOnAddGeneralCardsButton();
        enterPathInFixedListText(PATH_TO_TEST_AUTOMATION+"/"+templateName);
    }

    public void enterPathInFixedListText(String path){
        generalCardsPage.generalCardsTextBox.sendKeys(path);
        waitABit(2000);
    }
    public void clickOnAddGeneralCardsButton() {
        referenceSitePage.AddButton_GeneralCards.waitUntilClickable().click();
        waitABit(2000);
    }

    @Step
    public void selectDisplayMode(String displayMode) {

        switch (displayMode) {
            case "Multiple":
                referenceSitePage.MultipleElements.waitUntilClickable().click();
                break;
            case "Single":
                referenceSitePage.SingleElement.waitUntilClickable().click();
                break;
            default:
                break;
        }
    }

    @Step
    public void fillFields(String heading_text, String alt_text, String standFirst, String category, String newsDate, String link, String imageName) {

        referenceSitePage.AddButton_ManualSettingsContentFragment.waitUntilClickable().click();
        referenceSiteActions.dragAndDropImageToComponent();
        referenceSitePage.HeadingTextField_contnetFragment.waitUntilClickable().click();
        referenceSitePage.HeadingTextField_contnetFragment.type(heading_text);

        referenceSitePage.AltTextField_contnetFragment.type(alt_text);
        referenceSitePage.StandFirstTextField_contnetFragment.type(standFirst);

        if (link.toLowerCase().contains("external")) {
            referenceSitePage.LinkField_contnetFragment.type(GENERIC_EXTERNAL_LINK);

        }
        else {

            referenceSitePage.Link_OpenSelectionDialog_GeneralCards.click();
            navigationActions.clickOnDEXReferenceSiteFolder();
            navigationActions.clickOnReferenceSiteFolder();
            referenceSiteActions.clickOnTestAutomationFolder();
            referenceSiteActions.clickOnTemplatePage(link);
            referenceSitePage.SelectButton_popup.waitUntilClickable().click();
        }
        referenceSitePage.StandFirstTextField_contnetFragment.click();
        waitFor(1).seconds();
        referenceSitePage.CategoryField_contnetFragment.type(category);
        referenceSitePage.NewsDateField_contnetFragment.type(newsDate);
        referenceSitePage.DoneButton.click();
    }

    @Step
    public void clickOnManualSettingsDropdown() {
        referenceSitePage.TypeDropdown_GeneralCards.waitUntilClickable().click();
        referenceSitePage.ManualSettingsDropdown.waitUntilClickable().click();
    }

    public void navigateToManualSettingsTab() {

        referenceSitePage.ManualSettingsTab.click();
    }

    public void clickOnAddButton() {

        referenceSitePage.AddButton_ManualSettingsContentFragment.waitUntilClickable().click();
    }

    @Step
    public List<Boolean> verifyAlertsAreDisplayed() {
        List<Boolean> values=new ArrayList<>();
        values.add(referenceSitePage.HeadingText_MandatoryAlert.isVisible());
        values.add(referenceSitePage.AltText_MandatoryAlert.isVisible());
        values.add(referenceSitePage.Image_MandatoryAlert.isVisible());
        values.add(referenceSitePage.Link_MandatoryAlert.isVisible());
       return values;
    }

}
