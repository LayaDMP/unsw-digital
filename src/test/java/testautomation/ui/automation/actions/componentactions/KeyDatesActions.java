package testautomation.ui.automation.actions.componentactions;

import net.serenitybdd.core.pages.WebElementFacade;
import framework.BaseInteractions;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebElement;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.KeyDatesPage;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.*;


public class KeyDatesActions extends BaseInteractions {

    ReferenceSiteActions referenceSiteActions;

    int count;

    KeyDatesPage keyDatesPage;

    @Step
    public void enterKeydefaultDatesURL(int count) {
        if(count>1){
            typeInto(getDynamicElement(keyDatesPage.keyDatesURLTextBox,
                    String.valueOf(count)),PATH_TO_TEST_AUTOMATION);
    }else
        {
            typeInto(getDynamicElement(keyDatesPage.keyDatesURLTextBox,
                    String.valueOf(count)),GENERIC_EXTERNAL_LINK);
        }

    }

    @Step
    public void selectReadMoreTab(String tabName, String num){
        WebElement element;
        clickOn(getDynamicElement(keyDatesPage.readMoreTarget,num));
        if(tabName.toLowerCase().contains("same")) {
            element=getDynamicElement(keyDatesPage.selectSameTabTargetDropdown, num);
            element.click();
        }else{
            element=getDynamicElement(keyDatesPage.selectNewTabTargetDropdown,num);
            evaluateJavascript("arguments[0].click();",element);
        }
    }

    @Step
    public void addParametersToKeyDates(int c,String readMoreTarget)
    {
        count=c;
        waitforPageTodisplayWith(keyDatesPage.keyDatesComponentAdded);
        waitABit(200);
        keyDatesPage.keyDatesComponentAdded.click();
        referenceSiteActions.clickOnConfigureButton();
        keyDatesPage.showMoreTextFieldKeyDates.type("showMoreText");
        keyDatesPage.showLessTextFieldKeyDates.type("showLessText");
        for(int i=0;i<=count-1;i++) {
            String val= String.valueOf(i);
            clickOn(keyDatesPage.addButtonKeyDates);
            waitforPageTodisplayWith(getDynamicElement(keyDatesPage.subtitleTextFieldKeyDates,val));
            typeInto(getDynamicElement(keyDatesPage.subtitleTextFieldKeyDates,val),"subtitleText"+val);
            waitforPageTodisplayWith(getDynamicElement(keyDatesPage.titleTextFieldKeyDates,val));
            typeInto(getDynamicElement(keyDatesPage.titleTextFieldKeyDates,val),"title"+val);
            waitforPageTodisplayWith(getDynamicElement(keyDatesPage.readMoreTextFieldKeyDates,val));
            typeInto(getDynamicElement(keyDatesPage.readMoreTextFieldKeyDates,val),"readMoreText"+val);
            enterKeydefaultDatesURL(i);
            selectReadMoreTab(readMoreTarget,val);
            getDynamicElement(keyDatesPage.enterDateFieldKeyDates,val).typeAndTab(today.format(DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy")));
        }
    }

    @Step
    public List<Boolean> validateDate(){
        List<Boolean> datesStatus = new ArrayList<>();
        keyDatesPage.keyDatesDate.forEach(n-> datesStatus.add(n.isDisplayed()));
        return datesStatus;
    }

    @Step
    public List<Boolean> validateSubtitle(){
        List<Boolean> sub = new ArrayList<>();
        for(int i=0;i<=count-1;i++) {
            sub.add(getDynamicElement(keyDatesPage.keyDatesSubtitle, "subtitleText"+(i)).isVisible());
        }
        return sub;
    }

    @Step
    public List<Boolean> validateTitle(){
        List<Boolean> sub = new ArrayList<>();
        for(int i=0;i<=count-1;i++) {
            sub.add(getDynamicElement(keyDatesPage.keyDatesTitle, "title"+i).isVisible());
        }
        return sub;
    }

    @Step
    public boolean validateShowmoreBtn(){
        if(isOptionalElementDisplayed(keyDatesPage.keyDatesShowless)){
            clickOn(keyDatesPage.keyDatesShowless);
        }
        waitABit(200);
        return isElementDisplayed.test(keyDatesPage.keyDatesShowmore);
    }

    @Step
    public boolean validateShowlessBtn(){
        if(isOptionalElementDisplayed(keyDatesPage.keyDatesShowmore)){
            clickOn(keyDatesPage.keyDatesShowmore);
        }
        waitABit(200);
        return isElementDisplayed.test(keyDatesPage.keyDatesShowless);
    }

    @Step
    public List<Boolean> validateReadmoreLink(){
        List<Boolean> sub = new ArrayList<>();
        for(int i=0;i<=count-1;i++) {
            validateShowlessBtn();
            WebElementFacade current=getDynamicElement(keyDatesPage.keyDatesReadMoreLink,
                    "subtitleText"+i);
            String tab=current.getAttribute("target");
            sub.add(validateNavigation(tab,current));
        }
        return sub;
    }

    @Step
    public boolean validateNavigation(String tab,WebElement current) {
        String navigated = "";
        String currentUrl = getDriver().getCurrentUrl();
        boolean flag = false;
        if (tab.toLowerCase().contains("self")) {
            waitforPageTodisplayWith(current);
            current.click();
            waitABit(300);
            navigated = driver.getCurrentUrl();
            flag = !currentUrl.equalsIgnoreCase(navigated);
            getDriver().navigate().back();
            waitForWithRefresh().withMessage("Wait for page load");
        } else {
            String currentHandle = driver.getWindowHandle();
            Set<String> currentHandles = driver.getWindowHandles();
            current.click();
            waitForWithRefresh().withMessage("waiting for page to load");
            Set<String> windowhandles = driver.getWindowHandles();
            windowhandles.removeAll(currentHandles);
            for (String window : windowhandles) {
                if (!currentHandle.equals(window))
                    flag = !currentUrl.equalsIgnoreCase(navigated);
                driver.switchTo().window(currentHandle);
            }
        }
        return flag;
    }
}
