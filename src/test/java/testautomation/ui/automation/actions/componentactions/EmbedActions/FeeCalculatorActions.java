package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.FeeCalculatorPage;

public class FeeCalculatorActions extends BaseInteractions {
    FeeCalculatorPage feeCalculatorPage;

    public void editFeeCalculatorOption(String feeUrlText, String feeTitleText) {
        feeCalculatorPage.feeCalculatorURL.type(feeUrlText);
        feeCalculatorPage.feeCalculatorTitle.type(feeTitleText);

    }

    public void selectFeeCalculatorDropDown() {
        feeCalculatorPage.dropdownEmbeddable.waitUntilClickable().click();
        feeCalculatorPage.dropDownFeeCalculator.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return feeCalculatorPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedFeeCalculatorFrame() {
        waitForPageToLoadWith(feeCalculatorPage.publishedFeeCalculatorFrame);
        getDriver().switchTo().frame(feeCalculatorPage.publishedFeeCalculatorFrame);
        return feeCalculatorPage.publishedFeeCalculator.waitUntilVisible().isVisible();

    }
}
