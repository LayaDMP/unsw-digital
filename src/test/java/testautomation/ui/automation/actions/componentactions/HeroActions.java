package testautomation.ui.automation.actions.componentactions;

import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.HeroStandardPage;

public class HeroActions extends BaseInteractions {
    HeroStandardPage heroStandardPage;

    @Step
    public void clickOnHerostandardComponent() {
        waitforPageTodisplayWith(heroStandardPage.heroStandard);
        evaluateJavascript("arguments[0].click();",heroStandardPage.heroStandard);
    }

    @Step
    public void enterInHeadingfieldHeroStandard(String heading) {
        waitforPageTodisplayWith(heroStandardPage.headingHeroStandard);
        typeInto( heroStandardPage.headingHeroStandard,heading);
    }

    @Step
    public void chooseType(String type){
        getDynamicElement(heroStandardPage.heroTypeTabLink,"Hero")
                .waitUntilPresent().click();
        clickOn(heroStandardPage.heroTypeDropdown);
        getDynamicElement(heroStandardPage.heroTypeOption,type.toLowerCase()).click();
        getDynamicElement(heroStandardPage.heroTypeTabLink,type)
                .waitUntilPresent()
                .click();
    }

    @Step
    public void enterInSubheadingfieldHeroStandard(String sub_heading) {
        heroStandardPage.subHeadingHeroStandard.type(sub_heading);
    }

    @Step
    public boolean verifyComponentStyleHeroStandard() {
        return heroStandardPage.verifyStyle.isVisible();
    }

    @Step
    public boolean verifyHeadingHeroStandard(String heading) {
        return getDynamicElement(heroStandardPage.verifyHeading,heading).isVisible();
    }

    @Step
    public boolean verifyBackgroundImageHeroStandard(String backgroundImage) {
        return getDynamicElement(heroStandardPage.verifyBackgroundImage,backgroundImage).isVisible();
    }

    @Step
    public boolean verifySubheadingHeroStandard(String subHeading) {
        return getDynamicElement(heroStandardPage.verifySubheading,subHeading).isVisible();
    }

    @Step
    public void clickOnTheConfiguredComponent(String keyword) {
        waitABit(800);
        waitforPageTodisplayWith(getDynamicElement(heroStandardPage.addedComponent,keyword));
        clickOn(getDynamicElement(heroStandardPage.addedComponent,keyword));
        waitABit(300);
    }

    @Step
    public void clickOnStyleOption() {
        waitforPageTodisplayWith(heroStandardPage.styleOption);
        heroStandardPage.styleOption.click();

    }

    @Step
    public void selectStyeFromDropdown(String style) {
        clickOn(getDynamicElement(heroStandardPage.styleOptionSe,style));
        heroStandardPage.StyleCheckbox.click();

    }

}
