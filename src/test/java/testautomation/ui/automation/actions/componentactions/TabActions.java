package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import net.thucydides.core.annotations.Step;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TabPage;

import java.util.List;

public class TabActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;

    TabPage tabPage;

    @Step
    public void clickTabComponent(String title) {
        waitABit(1000);
        clickWithJS(getDynamicElement(tabPage.configTabButton,title));
    }

    public void editTabComponent(){
        tabPage.tabAddButton.waitUntilClickable().click();
        tabPage.tabNewComponentAddTextBox.waitUntilClickable().click();
        tabPage.tabNewComponentAddTextBox.type("text");
        waitForJSPageLoadComplete();
        tabPage.textOptionSelection.waitUntilClickable().click();
        tabPage.tabAddButton.waitUntilClickable().click();
        tabPage.authorOptionSelection.waitUntilClickable().click();
        tabPage.titlePlaceholder.type("TextInfoHere");
        tabPage.authorPlaceholder.type("AuthorInfoHere");
        
    }

    public void editTabComponent(List<String> component){

        for (String comp:component) {
            tabPage.tabAddButton.waitUntilClickable().click();
            referenceSiteActions.enterKeyword(comp);
            referenceSiteActions.addComponentFromDropdown();
            getDynamicElement(tabPage.titlePlaceholder,comp).type(comp+" component");
        }
    }

    public void editTabComponent(String component1,String component2){
        tabPage.tabAddButton.waitUntilClickable().click();
        referenceSiteActions.enterKeyword(component1);
        referenceSiteActions.addComponentFromDropdown();


        tabPage.tabAddButton.waitUntilClickable().click();
        tabPage.authorOptionSelection.waitUntilClickable().click();
        tabPage.titlePlaceholder.type("TextInfoHere");
        tabPage.authorPlaceholder.type("AuthorInfoHere");

    }

    public void editTabTitles(){
        tabPage.tabTextTitle.waitUntilClickable().click();
        referenceSiteActions.clickOnConfigureButton();
        referenceSiteActions.gotoManualSettingsTab();
        tabPage.tabTextTitle.type("Tab One Testing");
        tabPage.tabAuthorTitle.type("Tab Two Testing");
    }

    public void navigateToTab(String tabName) {
        getDynamicElement(tabPage.nextTab,tabName).click();
    }
}
