package testautomation.ui.automation.actions.componentactions;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.DegreeHeaderPage;

public class DegreePageHeaderActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    DegreeHeaderPage degreeHeaderPage;

    @Steps
    ReferenceSiteActions referenceSiteActions;

    @Step
    public void clickOnPageHeaderComponent() {
        waitABit(500);
        evaluateJavascript("arguments[0].click();",degreeHeaderPage.PageHeaderComponentAdded);
    }

    @Step
    public void selectType(String type) {
        waitforPageTodisplayWith(degreeHeaderPage.PageHeaderComponent_Type);
        degreeHeaderPage.PageHeaderComponent_Type.click();
        getDynamicElement(degreeHeaderPage.pageHeaderComponentType,type).click();
        waitABit(300);
        getDynamicElement(degreeHeaderPage.pageHeaderSecondTab,type).click();
    }


    @Step
    public void enterInHeadingField(String heading) {
        degreeHeaderPage.pageHeaderComponentHeading.waitUntilClickable().type(heading);
    }


    public void clickGeneralTab(){
        referenceSitePage.generalTabSettings.click();
    }

    @Step
    public void enterInSummaryField(String summary) {
        waitforPageTodisplayWith(degreeHeaderPage.pageHeaderComponentSummary);
        degreeHeaderPage.pageHeaderComponentSummary.type(summary);

    }

    @Step
    public void enterInDateField(String date) {
        degreeHeaderPage.pageHeaderComponentDate.type(date);
        degreeHeaderPage.pageHeaderComponentSummary.click();
    }
    @Step
    public void enterInAuthorDetails(String author_details) {
        degreeHeaderPage.pageHeaderComponentAuthorDetails.type(author_details);

    }
    @Step
    public void chooseBackgroundShape(String backgroundShape) {
        degreeHeaderPage.pageHeaderComponentBackgroundShape.click();
        getDynamicElement(degreeHeaderPage.pageHeaderBackgroundShape,backgroundShape).click();
    }

    @Step
    public void chooseAsset(String asset) {
        degreeHeaderPage.pageHeaderComponentVideoAssetBtn.click();
         if (asset.contains("Video")) {
           getDynamicElement(degreeHeaderPage.pageHeaderComponentVideoAsset, asset).click();

        } else if (asset.contains("Image")) {
             getDynamicElement(degreeHeaderPage.pageHeaderComponentVideoAsset, asset).click();
             dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,
                     referenceSitePage.DragToLocation_for_Image2);
        }

    }
    @Step
    public void clickOnDropdownFilter() {
        getDriver().switchTo().defaultContent();
        waitABit(900);
        degreeHeaderPage.pageHeaderComponentDropdownFilter.click();
    }


    public void selectDomesticOption(){
        clickOn(degreeHeaderPage.inputDomesticOption);
    }
    public void selectInternationalOption(){
        clickOn(degreeHeaderPage.inputInternationalOption);
    }

    @Step
    public boolean verifyHeadingPageHeaderOnPublishedPage(String heading) {
        return getDynamicElement(degreeHeaderPage.headingOnPublishedPage, heading).isVisible();
    }

    @Step
    public boolean verifySummaryPageHeaderOnPublishedPage(String summary) {
       return getDynamicElement(degreeHeaderPage.summaryOnPublishedPage, summary).isDisplayed();
    }


    @Step
    public boolean verifyLogoPageHeaderOnPublishedPage(String authorLogo) {
        return getDynamicElement(degreeHeaderPage.authorLogo, authorLogo).isDisplayed();
    }

    @Step
    public boolean verifyAuthorPageHeaderOnPublishedPage(String authorDetails) {

       return getDynamicElement(degreeHeaderPage.authorOnPublishedPage, authorDetails ).isDisplayed();

    }

    @Step
    public boolean verifyPageHeaderComponentShapePublishedSite() {
      return degreeHeaderPage.pageHeaderComponentShapePublishedSite.isVisible();
    }

    @Step
    public boolean verifyPageHeaderShape() {
       return degreeHeaderPage.pageHeaderComponentShapePublishedSite.isVisible();
    }

    @Step
    public String selectOptionFromDropdown(String option){
        waitforPageTodisplayWith(degreeHeaderPage.publishedDropDownBox);
        degreeHeaderPage.publishedDropDownBox.click();
        waitforPageTodisplayWith(getDynamicElement(degreeHeaderPage.selectDropdownOption,option));
        getDynamicElement(degreeHeaderPage.selectDropdownOption,option).click();
        return getDynamicElement(degreeHeaderPage.selectDropdown,option).getAttribute("class");
    }

}
