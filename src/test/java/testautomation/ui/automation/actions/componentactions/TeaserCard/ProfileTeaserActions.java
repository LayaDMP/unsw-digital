package testautomation.ui.automation.actions.componentactions.TeaserCard;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TeaserCard.ProfileTeaserCardPage;

import java.util.HashMap;
import java.util.Map;

public class ProfileTeaserActions extends BaseInteractions {

    ProfileTeaserCardPage profileTeaserCardPage;
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;

    public void editProfileTeaserCard(Map<String, String> profileTeaserDetails) {
        Map<String, String> pt = new HashMap<>(profileTeaserDetails);
        profileTeaserCardPage.VarianceDropDown.waitUntilClickable().click();
        profileTeaserCardPage.profileSelectionDropDown.waitUntilClickable().click();
        profileTeaserCardPage.profileSettingTab.waitUntilClickable().click();
        profileTeaserCardPage.profileAddButton.waitUntilClickable().click();
        profileTeaserCardPage.profilePageTextbox.type(pt.get("profilePage"));
        profileTeaserCardPage.profileFirstName.type(pt.get("profileFirstName"));
        profileTeaserCardPage.profileLastName.type(pt.get("profileLastName"));
        profileTeaserCardPage.profileTitle.type(pt.get("profileTitle"));
        profileTeaserCardPage.profileDescription.type(pt.get("profileDescription"));
        //profileTeaserCardPage.profileImage.type(pt.get("profileImage"));
        profileTeaserCardPage.profileLinkText.type(pt.get("profileLinkText"));
        profileTeaserCardPage.profileLinkCombobox.type(pt.get("profileLinkToNavigate"));

    }

    public void addProfileTeaserCardImage() {
        referenceSiteActions.dragAndDropImageToComponent();
    }

    public boolean publishedProfileTeaserComponent(){
        return profileTeaserCardPage.publishedProfileCard.isVisible(); }

    public boolean publishedProfileTitle(){
        return profileTeaserCardPage.publishedProfileCardTitle.isVisible(); }

    public boolean publishedProfileNameCard(){
        return profileTeaserCardPage.publishedProfileCardName.isVisible(); }

    public boolean publishedProfileCardDesc(){
        return profileTeaserCardPage.publishedProfileCardDesc.isVisible(); }

    public boolean publishedProfilePgeLink(){
        return profileTeaserCardPage.publishedProfilePageLink.isVisible(); }

    public boolean publishedProfileLnk(){
        return profileTeaserCardPage.publishedProfileLink.isVisible(); }

    public boolean publishedProfileImg(){
        return profileTeaserCardPage.publishedProfileImage.isVisible(); }

}
