package testautomation.ui.automation.actions.componentactions;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebElement;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.MosaicPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;


public class MosaicActions extends BaseInteractions {
    ReferenceSitePage refereceSitePage;
    ReferenceSiteActions referenceSiteActions;
    MosaicPage mosaicPage;


    @Step
    public void setDefaultMosaicTile(Map<String,String> mosaicTileDetails){

        int numberOfTiles= Integer.parseInt(mosaicTileDetails.get("noOfTiles"));
        String path= mosaicTileDetails.get("path");
        String titleText= mosaicTileDetails.get("title");
        String linkLabel= mosaicTileDetails.get("linkLabelTxt");
        String subTitleTxt= mosaicTileDetails.get("subTitle");

        for(int i=1;i<=numberOfTiles;i++){
            clickTileNo(i);
            if(i==2 || i==6){
                dropImagefromDialogIntoTileNo(i);
            }
            tileTitleforTileNo(i,titleText+i);
            linkLabelForTileNo(i,linkLabel+i);
            linkforTileNo(i, GENERIC_EXTERNAL_LINK);
            if(i!=1){
                subTitleforTileNo(i,subTitleTxt+i);
            }

        }
    }

    public void clickOnMosaicComponentAdded() {
        mosaicPage.mosaicTitle.waitUntilClickable().click();
    }

    public void tileTitleforTileNo(int i,String title){
        getDynamicElement(mosaicPage.mosaicTileTitle,String.valueOf(i)).sendKeys(title);}

    public void linkLabelForTileNo(int i,String labelText){
        getDynamicElement(mosaicPage.mosaicTileLinkLabel,String.valueOf(i)).sendKeys(labelText);}

    public void subTitleforTileNo(int i,String subtitleText){
        getDynamicElement(mosaicPage.mosaicTileSubTitle,String.valueOf(i)).sendKeys(subtitleText);}


    public void dropImagefromDialogIntoTileNo(int i){
        waitforPageTodisplayWith(getDynamicElement(mosaicPage.mosaicTileImageDrop,String.valueOf(i)));
        dragAndDrop(refereceSitePage.imageinsearchtextboxundertogglesidepanel,
                getDynamicElement(mosaicPage.mosaicTileImageDrop,String.valueOf(i)));
    }

    public void linkforTileNo(int i,String path){
        getDynamicElement(mosaicPage.mosaicLinkTile,String.valueOf(i)).sendKeys(path);
        clickOn(getDynamicElement(mosaicPage.mosaicLinkTile,String.valueOf(i)));
    }
    public void clickTileNo(int i){
        waitforPageTodisplayWith(getDynamicElement(mosaicPage.mosaicTileConfiguration,String.valueOf(i)));
        getDynamicElement(mosaicPage.mosaicTileConfiguration,String.valueOf(i)).click();
    }
    public void publishMosaicPage(){
        refereceSitePage.pageInformationCss.waitUntilClickable().click();
        referenceSiteActions.clickOnPublishPage();
    }
    public int getNoOfMosaicTile(){
        return mosaicPage.noOfMosaicTiles.size();
    }

    public List<String> mosaicTitle(){
        List<String> titles = new ArrayList<>();
        for (WebElementFacade title:mosaicPage.mosaicTitles) {
            titles.add(title.getText());
        }
        return titles;
    }
    public int getNoOfMosaicSubTitles(){

        return mosaicPage.subTitles.size();
    }
    public List<String> getMosaicURLs(){
        List<String> mosaicURL = new ArrayList<>();
        for (WebElementFacade title:mosaicPage.noOfMosaicTiles) {
            mosaicURL.add(title.getAttribute("href"));
        }
        return mosaicURL;
    }
    public List<String> mosaicAltText(){
        List<String> altText = new ArrayList<>();
        for (WebElement test:mosaicPage.mosaicImage) {
            altText.add(test.getAttribute("alt"));
            System.out.println(test.getAttribute("alt"));
        }
        return altText;
    }
}
