package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import org.openqa.selenium.WebElement;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.ListPage;


import java.util.ArrayList;
import java.util.List;

public class ListActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;
    ListPage listPage;
    List<String> listNames = new ArrayList<>();
    List<String> publishedList = new ArrayList<>();

    public void editListComponent(String text) {
        listPage.addListButton.waitUntilClickable().click();
        listPage.listTextBox.waitUntilClickable().click();
        listPage.listTextBox.type(text);

    }

    public List<String> editListComponent(int items) {
        String textToEnter="";
        for (int i = 1; i <= items; i++) {
            textToEnter = "List-" + i;
            listPage.addListButton.waitUntilClickable().click();
            getDynamicElement(listPage.listTextBox, String.valueOf(i)).waitUntilClickable().click();
            getDynamicElement(listPage.listTextBox, String.valueOf(i)).type(textToEnter);
            listNames.add(textToEnter);
        }
        return listNames;
    }

    public List<String> getListNames() {
        return listNames;
    }

    public List<String> checkTheListItems() {
    for (WebElement we : listPage.assertList) {
        String temp = we.getText();
        publishedList.add(temp);
    }
    return publishedList;
}
}
