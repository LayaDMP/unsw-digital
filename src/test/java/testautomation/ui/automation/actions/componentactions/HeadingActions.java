package testautomation.ui.automation.actions.componentactions;


import framework.BaseInteractions;

import net.thucydides.core.annotations.Step;
import testautomation.ui.automation.pages.components.HeadingPage;

public class HeadingActions extends BaseInteractions {

    HeadingPage headingPage;

    @Step
    public void clickOnAddedHeadingComponent(){
        waitforPageTodisplayWith(headingPage.headingComponentAdded);
        headingPage.headingComponentAdded.click();
    }

    public void addHeadingTitle(String headingTitle) {
           headingPage.headingtextField.type(headingTitle);
    }

    @Step
    public void editHeadingComponent(String headingTitle, String headingSize, String headingAlignment) {
        addHeadingTitle(headingTitle);
        addHeadingLevel(headingSize);
        addHeadingAlignment(headingAlignment);
    }

    @Step
    public void addHeadingLevel(String headingSize) {

        headingPage.headingLevelDropdown.waitUntilClickable().click();
        getDynamicElement(headingPage.headingLevel,headingSize).click();
    }
    @Step
    public void addHeadingAlignment(String headingAlignment) {

        headingPage.alignmentDropdownn.click();
        getDynamicElement(headingPage.headingLevel,headingAlignment).click();
    }

    @Step
    public boolean headinglevel(String headingLevel){
        return getDynamicElement(headingPage.headingText,headingLevel).isVisible();
    }
    @Step
    public String headingText(String headingLevel){
        return getDynamicElement(headingPage.headingText,headingLevel).getText();
    }
    @Step
    public String headingAlignment(String headingLevel){
        return getDynamicElement(headingPage.headingText,headingLevel).getAttribute("class");
    }

}
