package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.SideBarCTAPage;

public class SideBarCTAActions extends BaseInteractions {
    SideBarCTAPage sideBarCTAPage;

    public void editSideBarCTAComponent(String barTitle,String barSubTitle,String barButtonLabel,String barLinkURL){
        sideBarCTAPage.sidebarTitle.waitUntilClickable().click();
        sideBarCTAPage.sidebarTitle.type(barTitle);
        sideBarCTAPage.sidebarSubTitle.type(barSubTitle);
        sideBarCTAPage.sidebarButtonLabel.type(barButtonLabel);
        sideBarCTAPage.sidebarLinkURL.type(barLinkURL);

        sideBarCTAPage.sidebarOpenLinkDropDown.waitUntilClickable().click();
        sideBarCTAPage.sidebarSelectDropDownNewPage.waitUntilClickable().click();
    }

    public boolean publishedSideCTAComponent(){
        return sideBarCTAPage.publishedSideBarCTA.isVisible();
    }
    public boolean publishedSideBarButtonText(){
        return sideBarCTAPage.publishedSideBarButton.isVisible();
    }

    public boolean publishedSideBarTitleText(){
        return sideBarCTAPage.publishedSideBarTitle.isVisible();
    }

    public boolean publishedSideBarSubTitleText()
    {
        return sideBarCTAPage.publishedSideBarSubTitle.isVisible();
    }

    public boolean publishedSideBarNewPageOpen(){
        return sideBarCTAPage.publishedSideBarOpensNewPage.isVisible();
    }


}
