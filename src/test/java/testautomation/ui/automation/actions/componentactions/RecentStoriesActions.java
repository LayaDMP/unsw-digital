package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.RecentStoriesPage;

import java.util.ArrayList;
import java.util.List;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.PATH_TO_TEST_AUTOMATION;


public class RecentStoriesActions extends BaseInteractions {

        ReferenceSiteActions referenceSiteActions;
        ReferenceSitePage referenceSitePage;
        RecentStoriesPage recentStoriesPage;
        int add=0;

    @Step
    public void chooseSelection(String selection, String Title, String Standfirst, String Image, String AltText, String PublishedDate, String keyword, String Jsonfeed) {

        if (selection.toLowerCase().equals("default")) {
             clickOnRecentStoriesComponentAdded(keyword);
            referenceSiteActions.clickOnConfigureButton();
            chooseSelection(selection);
            selectJsonFeed(Jsonfeed);
            referenceSiteActions.clickOnDoneButton();
        } else if (selection.toLowerCase().contains("manual")) {
            referenceSiteActions.clickOnTogglePanel();
            referenceSiteActions.searchForImage(Image);
            clickOnRecentStoriesComponentAdded(keyword);
            referenceSiteActions.clickOnConfigureButton();
            configureManualSelectionRecentStories(selection);
            for (int i = 0; i < 3; i++) {
                if (add == 0 && i != 3) {
                    clickOnAddButtonRecentStories();
                }
                configureTitleManualSelectionRecentStories(i, Title);
                configureStandfirstManualSelectionRecentStories(i, Standfirst);
                configureImageManualSelectionRecentStories(i, Image);
                configureReadMoreURLManualSelectionRecentStories(i);
                configureAltTextManualSelectionRecentStories(i, AltText);
                configurePublishedDateManualSelectionRecentStories(i, PublishedDate);
            }
            referenceSiteActions.clickOnDoneButton();
        }
    }

    @Step
    public void clickOnAddButtonRecentStories() {
        recentStoriesPage.Addbutton_RecentStories.waitUntilClickable().click();
    }

    public List<Boolean> validateTitle(String s){
        List<Boolean> elementPresent=new ArrayList<>();
        recentStoriesPage.publishedTitle.
                forEach(n->elementPresent.
                        add(n.getText().contains(s)));

        return elementPresent;
    }
    public List<Boolean> validateDate(String s){
        List<Boolean> elementPresent=new ArrayList<>();
        recentStoriesPage.publishedDate.
                forEach(n->elementPresent.
                        add(n.getText().endsWith(s.substring(s.lastIndexOf(" ")+1))));

        return elementPresent;
    }
    public List<Boolean> validateImage(){
        List<Boolean> elementPresent=new ArrayList<>();
        recentStoriesPage.publishedImage.
                forEach(n->elementPresent.
                        add(n.isVisible()));

        return elementPresent;
    }

    public boolean verifyAlumnifeedPublishedpage() {
       return recentStoriesPage.alumniFeedPubishedPage.isVisible();
    }

    public boolean verifyNewsroomfeedPublishedpage() {
        waitFor(2).second();
        return recentStoriesPage.newsroomFeedPubishedPage.isVisible();
    }

    public void clickOnRecentStoriesComponentAdded(String keyword) {
        waitforPageTodisplayWith(getDynamicElement(recentStoriesPage.recentStoriesComponent,keyword));
        getDynamicElement(recentStoriesPage.recentStoriesComponent,keyword).
                waitUntilClickable().
                click();
    }

    public void chooseSelection(String selection) {
        waitFor(1).second();
        recentStoriesPage.recentStoriesSelectionDropdown.waitUntilClickable().click();
        getDynamicElement(recentStoriesPage.selectionValue,selection).waitUntilClickable().click();
    }

    public void selectJsonFeed(String jsonfeed) {

        recentStoriesPage.RecentStories_JsonfeedDropdown.waitUntilClickable().click();
        getDynamicElement(recentStoriesPage.jsonFeedValue,jsonfeed).waitUntilClickable().click();

    }


    public void configureManualSelectionRecentStories(String selection) {

        waitFor(1).second();
        recentStoriesPage.recentStoriesSelectionDropdown.waitUntilClickable().click();
        getDynamicElement(recentStoriesPage.selectionValue,selection).waitUntilClickable().click();
    }

    public void configureTitleManualSelectionRecentStories(Integer i, String title) {
        getDynamicElement(recentStoriesPage.title,String.valueOf(i)).waitUntilClickable().click();
        getDynamicElement(recentStoriesPage.title,String.valueOf(i)).type(title);
    }

    public void configureStandfirstManualSelectionRecentStories(Integer i, String standfirst) {
        getDynamicElement(recentStoriesPage.standFirst,String.valueOf(i)).waitUntilClickable().click();
        getDynamicElement(recentStoriesPage.standFirst,String.valueOf(i)).type(standfirst);
    }

    public void configureImageManualSelectionRecentStories(Integer i, String image) {
        WebDriver driver = getDriver();
        Actions act = new Actions(driver);
        WebElement dropit = getDynamicElement(recentStoriesPage.dropHere,String.valueOf(i+1));
        act.dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel, dropit).build().perform();
        referenceSiteActions.dragAndDropImageToComponent();
    }

    public void configureAltTextManualSelectionRecentStories(Integer i, String altText) {
        getDynamicElement(recentStoriesPage.altTextRecentStories,String.valueOf(i)).type(altText);
        recentStoriesPage.AltText_RecentStories.type(altText);
    }

    public void configureReadMoreURLManualSelectionRecentStories(Integer i) {
        getDynamicElement(recentStoriesPage.recentStoriesURLTextBox,String.valueOf(i)).sendKeys(PATH_TO_TEST_AUTOMATION);
    }

    public void configurePublishedDateManualSelectionRecentStories(Integer i, String publishedDate) {

        getDynamicElement(recentStoriesPage.publishedDates,String.valueOf(i)).type(publishedDate);

    }


}
