package testautomation.ui.automation.actions.componentactions.EmbedActions;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.Embed.TweetPage;


public class TweetActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;
    TweetPage tweetPage;

    public void selectTweetDropdown(){
        tweetPage.dropdownEmbeddable.waitUntilClickable().click();
        tweetPage.dropDownTweet.waitUntilClickable().click();
    }
    public void editTweetOption(String htmlText){
        tweetPage.pasteEmbedHtmlTweet.type(htmlText);
         }
    public boolean publishedTweetOption()
    {
        waitForPageToLoadWith(tweetPage.publishedTwitterFrame);
        getDriver().switchTo().frame(tweetPage.publishedTwitterFrame);
        return  tweetPage.publishedTwitterTweet.waitUntilVisible().isVisible();}

    public boolean publishedEmbedGrid()
    { waitForJSPageLoadComplete();
            return tweetPage.publishedEmbedGrid.isVisible();}

}
