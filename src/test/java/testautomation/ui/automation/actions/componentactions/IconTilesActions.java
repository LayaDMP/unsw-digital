package testautomation.ui.automation.actions.componentactions;

import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.IconTilePage;

import java.util.HashMap;
import java.util.Map;



public class IconTilesActions extends BaseInteractions {

    IconTilePage iconTilePage;
    ReferenceSitePage referenceSitePage;

    public void clickOnIconTileAdded() {
        waitforPageTodisplayWith(iconTilePage.icontilesComponentAdded);
        iconTilePage.icontilesComponentAdded.click();
    }

    @Step
    public void selectTitleStyle(String titleStyle) {
        getDriver().switchTo().defaultContent();
        waitABit(500);
        iconTilePage.titleStyleDropdown.waitUntilClickable().click();
        getDynamicElement(iconTilePage.titleStyle,titleStyle).waitUntilClickable().click();

    }

    public void iconTileAddHeading(String heading)
    {
        iconTilePage.headingIcontile.waitUntilClickable().type(heading);
    }

    public void iconTileAddSubtitle(String subtitle) {

        iconTilePage.subtitleIcontile.waitUntilClickable().type(subtitle);
    }
    @Step
    public void iconTileAddTitleCopy(String titleCopy) {

        iconTilePage.TitleCopy_Icontile.waitUntilClickable().type(titleCopy);
    }
    @Step
    public void iconTileAddButtonLabel(String buttonLabel) {
        iconTilePage.buttonLabelIcontile.waitUntilClickable().type(buttonLabel);
    }

    @Step
    public void selectButtonURLIconTile(String path) {
        getDriver().switchTo().activeElement();
        typeInto(iconTilePage.iconTilesTextBox,path);
    }
    @Step
    public void clickOnAddButtonIcontile() {

        iconTilePage.addButtonIconTile.waitUntilClickable().click();
    }
    @Step
    public void addIconForThisTile() {
        iconTilePage.iconForThisTileOpenSelectionDialog.waitUntilClickable().click();
        iconTilePage.UNSWLogoPopupIcontile.waitUntilClickable().click();
        referenceSitePage.SelectButton_popup.click();
    }
    @Step
    public boolean verifyIconForThisTilePublishedPage() {
        waitABit(300);
        return iconTilePage.iconForThisTilePublishedPage.isVisible();
    }
    @Step
    public boolean verifyTitleStylePublishedPage(String titleStyle) {

        Map<String,Boolean> test=new HashMap<>();
        test.put("No Background",iconTilePage.titleStyleBackground.isVisible());
        test.put("Shadow Background",iconTilePage.titleStyleShadow.isVisible());
        test.put("White Background",iconTilePage.titleStyleWhiteBackground.isVisible());
        return test.get(titleStyle);
    }

    @Step
    public String verifyHeadingPublisedPage() {

        return iconTilePage.headingIconTile.getText();

    }
    @Step
    public String verifySubtitlePublishedPage() {
        return iconTilePage.subtitleIconTile.getText();
    }
    @Step
    public String verifyTitleCopyPublishedPage() {
        return iconTilePage.titleCopyPublished.getText();

    }
    @Step
    public boolean verifyButtonLabelPubishedPage(String buttonLabel) {
        return getDynamicElement(iconTilePage.buttonLabel,buttonLabel).isVisible();
    }
}
