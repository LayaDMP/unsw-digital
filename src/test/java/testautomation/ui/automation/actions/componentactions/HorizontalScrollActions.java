package testautomation.ui.automation.actions.componentactions;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.NavigationActions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.GeneralCardsPage;
import testautomation.ui.automation.pages.components.HorizontalScrollPage;


import java.util.ArrayList;
import java.util.List;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.PATH_TO_TEST_AUTOMATION;



public class HorizontalScrollActions extends BaseInteractions {
        ReferenceSitePage referenceSitePage;
        ReferenceSiteActions referenceSiteActions;
        NavigationActions navigationActions;
        HorizontalScrollPage horizontalScrollPage;
        TextActions textActions;
        GeneralCardsPage generalCardsPage;
        GeneralCardActions generalCardActions;

    @Step
    public void addHorizontalScrollComponent(Integer numberOfComponents, String column_layout, String horizontalScrollComponent) {
        referenceSiteActions.clickOnTheSuppliedColumnLayout(column_layout);
        referenceSiteActions.clickOnPlusButton();
        addHorizontalscrollComponent();
        referenceSiteActions.clickOnConfigureButton();
        for (int i = 1; i <= numberOfComponents; i++) {
            addcomponentsUnderHorizontalscrollComponent(i, horizontalScrollComponent);
        }
        referenceSiteActions.clickOnDoneButton();
    }

    @Step
    public boolean verifySiderDottedPublishedSite() {
       return horizontalScrollPage.sliderDottedHorizontalScrollPublishedSite.isVisible();

    }

    @Step
    public boolean verifySliderArrowHorizontalScrollPublishedSite() {
        waitforPageTodisplayWith(horizontalScrollPage.sliderArrowHorizontalScrollPublishedSite);
        clickOn(horizontalScrollPage.sliderArrowHorizontalScrollPublishedSite);
        return true;
    }

    @Step
    public List<Boolean> verifyTextComponent(String text,int componentNumber) {
        List<Boolean> textContains=new ArrayList<>();
        for (int i=0;i<=componentNumber-1;i++){
            textContains.add(getDynamicElement(horizontalScrollPage.publishedTextComponent,
                    String.valueOf(i)).getText().contains(text));
        }
       return textContains;
    }

    @Step
    public List<Boolean> verifyImageComponent(int componentNumber,String image) {
        List<Boolean> imageContains=new ArrayList<>();
        for (int i=0;i<=componentNumber-1;i++){
           imageContains.add(getDynamicElement(horizontalScrollPage.publishedImageComponent,
                   String.valueOf(i)).getAttribute("src").contains(image));
        }
        return imageContains;

    }

    @Step
    public void configureEachComponent(Integer numberOfComponents, String componentToAdd, String contentFragmentPath, String text,
                                       String image, String Templatename) {

        for (int i = 1; i <= numberOfComponents; i++) {
            configureComponentadded(i,componentToAdd, contentFragmentPath, text, image, Templatename);
        }
    }

    @Step
    public void addHorizontalscrollComponent() {

        referenceSitePage.EnterKeywordTextBox.type("Horizontal scroll");
        waitABit(500);
        horizontalScrollPage.horizontalScrollComponentDropdown.waitUntilClickable().click();
        navigationActions.refreshPageURL();
        WebDriver driver = getDriver();
        Actions act = new Actions(driver);
        waitABit(500);
        act.moveToElement(horizontalScrollPage.horizontalScrollComponentAdded, 5, 5);
        act.click().build().perform();
    }

    @Step
    public void addcomponentsUnderHorizontalscrollComponent(int i, String horizontalScrollComponent) {

       waitABit(300);
        horizontalScrollPage.addButtonHorizontalScroll.waitUntilClickable().click();
        referenceSitePage.EnterKeywordTextBox.waitUntilClickable().click();
        referenceSitePage.EnterKeywordTextBox.type(horizontalScrollComponent);
        getDriver().switchTo().defaultContent();
        waitABit(500);
        waitforPageTodisplayWith(horizontalScrollPage.addDropdownComponentHorizontalScroll);
        horizontalScrollPage.addDropdownComponentHorizontalScroll.click();
    }
    boolean flag = true;

    @Step
    public void configureComponentadded(Integer i,String componentToAdd, String contentFragmentPath, String text, String image, String templateName) {
        String xpath=componentToAdd+"']["+i+"]";
        WebElementFacade elementFacade=getDynamicElement(horizontalScrollPage.componentUnderHorzontalScrollComponent,xpath);
        switch (componentToAdd) {
            case "General Cards (v2)":
                waitFor(3).seconds();
                clickOn(elementFacade);
                waitABit(500);
                referenceSiteActions.clickOnConfigureButton();
                generalCardActions.clickOnSquareCardTheme();
                referenceSitePage.ContentFragmentCardSettings.waitUntilClickable().click();
                referenceSiteActions.selectFixedListDropdownOption();
                referenceSitePage.AddButton_GeneralCards.waitUntilClickable().click();
                generalCardsPage.generalCardsTextBox.sendKeys(PATH_TO_TEST_AUTOMATION+"/"+templateName);
                waitABit(800);
                referenceSiteActions.clickOnDoneButton();
                break;

            case "Image (v2)":
               WebElementFacade imageXpath=getDynamicElement(horizontalScrollPage.componentUnderHorzontalScrollComponent,
                        componentToAdd+"'][1]");

                if (flag) {
                    referenceSiteActions.clickOnTogglePanel();
                    referenceSiteActions.searchForImage(image);
                    waitFor(3).seconds();
                    clickOn(imageXpath);
                    waitABit(500);
                    referenceSiteActions.clickOnConfigureButton();
                    referenceSiteActions.dragAndDropImageToComponent();
                    referenceSiteActions.clickOnDoneButton();
                    flag = false;

                } else {
                    waitFor(3).seconds();
                    clickOn(imageXpath);
                    referenceSiteActions.clickOnConfigureButton();
                    referenceSiteActions.dragAndDropImageToComponent();
                    referenceSiteActions.clickOnDoneButton();
                }
                break;

            case "Text":
                WebElementFacade textXpath=getDynamicElement(horizontalScrollPage.componentUnderHorzontalScrollComponent,
                        componentToAdd+"'][1]");
                getDriver().getCurrentUrl();
                getDriver().switchTo().defaultContent();
                waitFor(2).seconds();
                clickOn(textXpath);
                referenceSiteActions.clickOnConfigureButton();
                referenceSiteActions.gotoManualSettingsTab();
                textActions.editHeadingTitle(text);
                referenceSiteActions.clickOnDoneButton();

                break;

            default:
                break;

        }


    }

}
