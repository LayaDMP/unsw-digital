package testautomation.ui.automation.actions.componentactions;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;

public class UNSWGlobalHeaderActions extends BaseInteractions {

    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;

    public void verify_GlobalHeaderSection_onPublishedPage(String sectionTitile1, String sectionTitileLink) {
        waitFor(10).seconds();
        Actions action = new Actions(getDriver());
        String GlobalHeaderSection = "//a[@href = '" + sectionTitileLink + "' and @data-click_name='" + sectionTitile1 + "']";
        action.moveToElement(find(By.xpath(GlobalHeaderSection))).build().perform();
    }

    public void verify_GlobalHeaderSubSections_onPublishedPage(String subSection_sectionTitile1, String subSection_sectionTitile2, String subSection_sectionTitile3, String sectionTitileLink) {
        waitFor(1).seconds();
        String GlobalHeaderSubSection1 = "//a[@href = '" + sectionTitileLink + "' and @data-click_name='" + subSection_sectionTitile1 + "']";
        find(By.xpath(GlobalHeaderSubSection1)).shouldBeVisible();

        String GlobalHeaderSubSection2 = "//a[@href = '" + sectionTitileLink + "' and @data-click_name='" + subSection_sectionTitile2 + "']";
        find(By.xpath(GlobalHeaderSubSection2)).shouldBeVisible();

        String GlobalHeaderSubSection3 = "//a[@href = '" + sectionTitileLink + "' and @data-click_name='" + subSection_sectionTitile3 + "']";
        find(By.xpath(GlobalHeaderSubSection3)).shouldBeVisible();
    }

    public void click_on_GlobalHeaderSubSection_onPublishedPage(String subSection_sectionTitile1, String SectionTitileLink) {

        waitFor(1).seconds();
        String GlobalHeaderSubSection1 = "//a[@href = '" + SectionTitileLink + "' and @data-click_name='" + subSection_sectionTitile1 + "']";
        find(By.xpath(GlobalHeaderSubSection1)).waitUntilClickable().click();

    }

    public void add_component(String componentName) {

        waitFor(2).seconds();
        switch (componentName) {
            case "Hori":
                referenceSitePage.oneColumnFullWidthLayout.waitUntilClickable().click();
                break;

            default:
                break;
        }
    }


}
