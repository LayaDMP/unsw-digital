package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import net.thucydides.core.annotations.Step;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;

import java.time.Duration;

public class ContentFragmentActions extends BaseInteractions {

    ReferenceSitePage referenceSitePage;


    @Step
    public void addContentFragmentPath(String contentFragmentPath) {
        referenceSitePage.OpenSelectDialog_ContentFragment.waitUntilClickable().click();
        referenceSitePage.AutomationFolder.waitUntilClickable().click();
        getDynamicElement(referenceSitePage.contentFragmentLinkInPopup,contentFragmentPath).click();
        referenceSitePage.SelectButton_popup.click();
    }
    @Step
    public void addArticleContentElement() {
        referenceSitePage.SingleElement.waitUntilClickable().click();
        referenceSitePage.MultipleElements.waitUntilClickable().click();
        waitforPageTodisplayWith(referenceSitePage.AddButton_ContentFragmentElements);
        referenceSitePage.AddButton_ContentFragmentElements.click();
        waitforPageTodisplayWith(referenceSitePage.SelectDropdown_AddElements_ContentFragment);
        referenceSitePage.SelectDropdown_AddElements_ContentFragment.click();
        referenceSitePage.ArticleContentElementDropdown.waitUntilClickable().click();
    }


}
