package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import org.jcodings.util.Hash;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.HeroGeneralPage;
import java.util.HashMap;
import java.util.Map;


public class HeroGeneralActions extends BaseInteractions {

    HeroGeneralPage heroGeneralPage;
    ReferenceSitePage referenceSitePage;
    String option;
    String date="";
    String enableDate="";
    String enableDescription="";
    String authorName="";
    Map<String,String> configuration=new HashMap<>();

    private void setConfiguration(){

        date=configuration.get("date");
        enableDate=configuration.get("enableDate");
        enableDescription=configuration.get("enableDescription");
        authorName=configuration.get("authorName");

    }

    public void configureTextAndContent(Map<String,String> config){
        configuration.putAll(config);
        setConfiguration();
        clickOnTextAndContentLink();
        heroGeneralPage.labelDateTextfield
                .sendKeys(date);
        actionOnDatePublish(enableDate);
        actionOnDescription(enableDescription);
        heroGeneralPage.authorNameTextField.
                waitUntilEnabled().sendKeys(authorName);
    }

    public void configureImageprop(String background){
        String bgValue=background.toLowerCase();
        clickOnImageAndAssetLink();
        waitforPageTodisplayWith(heroGeneralPage.backgroundDropdownBtn);
        heroGeneralPage.backgroundDropdownBtn.click();
        if(bgValue.equalsIgnoreCase("image")){
            bgValue="theme-dark";
        }
        getDynamicElement(heroGeneralPage.backgroundDropdownValue
                ,bgValue).waitUntilClickable().click();
        setupBackground(background);
    }

    public void clickOnTextAndContentLink(){
        heroGeneralPage.
                textAndContentLink
                .waitUntilClickable()
                .click();
    }

    public void clickOnImageAndAssetLink(){
        heroGeneralPage.
                imageAndAssetLink
                .waitUntilClickable()
                .click();
    }

    public void actionOnDatePublish(String decision){
        if(decision.equalsIgnoreCase("yes")){
            heroGeneralPage.publishDateCheckbox.click();
        }
    }
    public void actionOnDescription(String checkbox){
        if(checkbox.equalsIgnoreCase("yes")){
            heroGeneralPage.descCheckbox.click();
        }
    }
    private void setupBackground(String background){
        option= background.toLowerCase();
        switch (option) {
            case "image":
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.backgroundImage);
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.featuredImage);
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.avatarImage);
                break;
            case "illustration":
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.featuredImage);
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.avatarImage);
            default:
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.featuredImage);
                dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,heroGeneralPage.avatarImage);
        }
    }

    public boolean publishedPageHeading(){
        return heroGeneralPage.publishedHeader.isVisible();
    }
    public boolean publishedFeatureImage(){
        return heroGeneralPage.publishedFeatureImage.isVisible();
    }
    public boolean publishedAuthorImage(){
        return heroGeneralPage.publishedAuthorImage.isVisible();
    }
    public boolean publishedBackgroundImage(){
        if(option.equalsIgnoreCase("image")) {
            return heroGeneralPage.publishedBackgroundImage.isPresent();
        }else return true;
    }
    public String publishedAuthorName(){
        return heroGeneralPage.publishedAuthorName.getText();
    }
    public String publishedDate(){

            return heroGeneralPage.publishedDate.getText();
    }

}
