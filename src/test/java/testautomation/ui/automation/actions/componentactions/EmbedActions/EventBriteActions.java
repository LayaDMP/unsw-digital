package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.EventbritePage;

public class EventBriteActions extends BaseInteractions {
    EventbritePage eventbritePage;

    public void editEventBriteOption(long wID) {
        eventbritePage.eventbriteID.type(String.valueOf(wID));

    }

    public void selectEventBriteDropDown() {
        eventbritePage.dropdownEmbeddable.waitUntilClickable().click();
        eventbritePage.dropDownEventBrite.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return eventbritePage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedEventBriteFrame() {
        waitForPageToLoadWith(eventbritePage.publishedEventBriteFrame);
        getDriver().switchTo().frame(eventbritePage.publishedEventBriteFrame);
        return eventbritePage.publishedEventBrite.waitUntilVisible().isVisible();

    }
}
