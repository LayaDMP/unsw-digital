package testautomation.ui.automation.actions.componentactions.TeaserCard;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TeaserCard.GeneralTeaserCardPage;
import testautomation.ui.automation.pages.components.TeaserCard.VideoTeaserCardPage;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
public class VideoTeaserActions extends BaseInteractions{
    VideoTeaserCardPage videoTeaserCardPage;
    GeneralTeaserCardPage generalTeaserCardPage;
    ReferenceSitePage referenceSitePage;

    public void editVideoTeaserCard(Map<String, String> videoTeaserDetails) {
        Map<String, String> vt = new HashMap<>(videoTeaserDetails);
        videoTeaserCardPage.VarianceDropDown.waitUntilClickable().click();
        videoTeaserCardPage.videoSelectionDropDown.waitUntilClickable().click();
        videoTeaserCardPage.videoSettingsTab.waitUntilClickable().click();
        videoTeaserCardPage.videoAddButton.waitUntilClickable().click();
        videoTeaserCardPage.videoTeaserYouTubeID.type(vt.get("videoID"));
        videoTeaserCardPage.videoTeaserTitle.type(vt.get("videoTitle"));
        videoTeaserCardPage.videoTeaserHeading.type(vt.get("videoHeading"));
        videoTeaserCardPage.videoTeaserDescription.type(vt.get("videoDesc"));
        videoTeaserCardPage.videoTeaserImage.type(vt.get("videoImage"));
        videoTeaserCardPage.videoTeaserImageList.waitUntilClickable().click();
        videoTeaserCardPage.videoTeaserLink.type(vt.get("videoLink"));
        videoTeaserCardPage.videoTeaserTranscript.type(vt.get("videoTranscript"));
        videoTeaserCardPage.videoTeaserVideoLength.type(vt.get("videoLength"));
        videoTeaserCardPage.videoTeaserDate.type(vt.get("vDate"));
        
    }

    public void tagSelection(){
        videoTeaserCardPage.videoTeaserTag.waitUntilClickable().click();
        videoTeaserCardPage.videoTagSelectionPage.waitUntilVisible();
        videoTeaserCardPage.videoTagValue.waitUntilVisible();
        videoTeaserCardPage.videoTagValue.waitUntilClickable().click();
        videoTeaserCardPage.videoTagSelectButton.waitUntilClickable().click();
    }

    public boolean publishVideoTeaserCard(){ return videoTeaserCardPage.publishedVideoTeaserCard.isVisible();}

    public boolean publishVideoTeaserTopic(){return videoTeaserCardPage.publishedVideoTeaserTopic.isVisible();}

    public boolean publishVideoTeaserHeading(){return videoTeaserCardPage.publishedVideoTeaserHeading.isVisible();}

    public boolean publishVideoTeaserDate(){return videoTeaserCardPage.publishedVideoTeaserDate.isVisible();}

    public boolean publishVideoTeaserDesc(){return videoTeaserCardPage.publishedVideoTeaserDesc.isVisible();}

    public boolean publishVideoTeaserTag(){ return videoTeaserCardPage.publishedVideoTeaserTag.isVisible();}

    public boolean publishVideoTeaserTranscript(){return videoTeaserCardPage.publishedVideoTeaserTranscript.isVisible();}

    public boolean publishVideoTeaserVideo(){
               return  videoTeaserCardPage.publishedVideoTeaserVideo.waitUntilVisible().isVisible();}

    public boolean publishVideoTeaserLink(){return videoTeaserCardPage.publishedVideoTeaserLink.isVisible();}
}
