package testautomation.ui.automation.actions.componentactions;

import org.openqa.selenium.By;
import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TextPage;

import java.time.Duration;

public class TextActions extends BaseInteractions {
        ReferenceSitePage referenceSitePage;
        ReferenceSiteActions referenceSiteActions;
        TextPage textPage;

    String headingToGet = "";


    public void editHeadingTitle(String heading_title) {
        waitForPageToLoadWith(textPage.textboxTextComponent);
        textPage.textboxTextComponent.waitUntilClickable().click();
        textPage.textboxTextComponent.type(heading_title);
    }

    public void editHeadingLevel(String headingSize) {

        textPage.paragraphDropdownTextComponent.setImplicitTimeout(Duration.ofSeconds(10));
        textPage.paragraphDropdownTextComponent.waitUntilClickable().click();
        headingToGet=getHeadingLevel(headingSize);
        waitforPageTodisplayWith(getDynamicElement(textPage.textHeadingSelect, headingToGet));
        clickOn(getDynamicElement(textPage.textHeadingSelect, headingToGet));
    }



    public void editHeadingAlignment(String headingAlignment) {
        waitForPageToLoadWith(textPage.textSizeDropdownTextComponent);
        textPage.textSizeDropdownTextComponent.waitUntilClickable().click();
        waitforPageTodisplayWith(getDynamicElement(textPage.textAlignmentSelect,headingAlignment.toLowerCase()));
        clickOn(getDynamicElement(textPage.textAlignmentSelect,headingAlignment.toLowerCase()));

    }


    public boolean validateSize(String headingSize){
        return getDynamicElement(textPage.publishedText,getHeadingLevel(headingSize)).isVisible();
    }
    public String validateText(String headingSize){
        return getDynamicElement(textPage.publishedText,getHeadingLevel(headingSize)).getText();
    }
    public String validateAlignment(String headingSize){
        return getDynamicElement(textPage.publishedText,getHeadingLevel(headingSize)).getAttribute("style");
    }
    public void editTextComponent(String headingTitle, String headingSize, String headingAlignment) {
        editHeadingTitle(headingTitle);
        editHeadingLevel(headingSize);
        editHeadingAlignment(headingAlignment);
    }

    public String getHeadingLevel(String headingSize){

        String heading = headingSize.toLowerCase();
        String headingLevel="";
        int size = headingSize.length();
        if (heading.startsWith("p")) {
            headingLevel = "p";
        } else if (heading.startsWith("h")) {
            String lastDigit = heading.substring(size - 1, size);
            headingLevel = heading.substring(0, 1).concat(lastDigit);
        } else {
            headingLevel = "blockquote";
        }
        return headingLevel;
    }

}
