package testautomation.ui.automation.actions.componentactions;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.CTAbuttonPage;

import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.GENERIC_EXTERNAL_LINK;


public class CTAButtonActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    CTAbuttonPage ctaBbuttonPage;
    EnvironmentVariables environmentVariables;


    @Step
    public void clickOnCTAButtonAdded() {
        waitForJSPageLoadComplete();
        waitABit(800);
        clickWithJS(ctaBbuttonPage.CTAbuttonAdded);
    }

    @Step
    public void clickOnAddButtonCTAButton() {
        waitforPageTodisplayWith(ctaBbuttonPage.addButtonCTAComponent);
        ctaBbuttonPage.addButtonCTAComponent.click();
        waitforPageTodisplayWith( ctaBbuttonPage.textFieldButtonComponent);
    }

    public void enterInTextField(String text) {
        ctaBbuttonPage.textFieldButtonComponent.type(text);
    }

    @Step
    public void enterValueInLinkField(String link) {
        if(link.toLowerCase().contains("internal")) {
            ctaBbuttonPage.linkFieldChevron.waitUntilClickable().click();
            waitforPageTodisplayWith(ctaBbuttonPage.dexReferenceSitePopupButtonComponent);
            ctaBbuttonPage.dexReferenceSitePopupButtonComponent.click();
            referenceSitePage.SelectButton_popup.waitUntilClickable().click();
        }
        else {
            ctaBbuttonPage.linkFieldButtonComponent.type(GENERIC_EXTERNAL_LINK);
        }
        waitforPageTodisplayWith(ctaBbuttonPage.textFieldButtonComponent);
        ctaBbuttonPage.textFieldButtonComponent.click();
    }

    @Step
    public void selectButtonColor(String colour) {
        waitforPageTodisplayWith(ctaBbuttonPage.buttonColorDropdown);
        ctaBbuttonPage.buttonColorDropdown.click();
        getDynamicElement(ctaBbuttonPage.buttonColour,colour).click();
    }
    @Step
    public void selectButtonStyle(String style) {
        waitforPageTodisplayWith( ctaBbuttonPage.buttonStyleDropdown);
        ctaBbuttonPage.buttonStyleDropdown.click();
        getDynamicElement(ctaBbuttonPage.buttonStyle,style).click();
    }

    @Step
    public void selectLabelStyle(String labelStyle) {
        ctaBbuttonPage.labelStyleDropdown.click();
        switch (labelStyle) {
            case "Married":
                waitforPageTodisplayWith(ctaBbuttonPage.layoutMarried);
                ctaBbuttonPage.layoutMarried.click();
                break;

            case "Married with icon on left":
                waitforPageTodisplayWith(ctaBbuttonPage.layoutMarriedWithIcon);
                ctaBbuttonPage.layoutMarriedWithIcon.click();
                break;

            case "Divorced":
                waitforPageTodisplayWith(ctaBbuttonPage.layoutDivorced);
                ctaBbuttonPage.layoutDivorced.click();
                break;

            case "Text Only":
                waitforPageTodisplayWith(ctaBbuttonPage.layoutTextOnly);
                ctaBbuttonPage.layoutTextOnly.click();
                break;
            default:
                break;
        }
    }

    @Step
    public void selectButtonSize(String size) {
        waitforPageTodisplayWith(ctaBbuttonPage.buttonSizeDropdown);
        ctaBbuttonPage.buttonSizeDropdown.click();
        getDynamicElement(ctaBbuttonPage.buttonSize,size).click();
    }

    @Step
    public void selectButtonAlignment(String alignment) {
        waitforPageTodisplayWith(ctaBbuttonPage.alignmentDropdownButtonComponent);
        ctaBbuttonPage.alignmentDropdownButtonComponent.click();
        getDynamicElement(ctaBbuttonPage.selectButtonAlignment,alignment).click();
    }

    @Step
    public boolean isCTAButtonAlignmentPublishedSite(String alignment) {
        return isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.buttonAlignment,alignment));
    }
    @Step
    public boolean isCTAButtonTextPublishedSite(String text) {
        return isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.buttonText,text));
    }
    @Step
    public boolean isCTAButtonLinkPublishedSite(String link) {
        boolean flag=false;
        if(link.toLowerCase().contains("internal")) {
            waitforPageTodisplayWith(ctaBbuttonPage.buttonPublishedSite);
            ctaBbuttonPage.buttonPublishedSite.click();
            waitABit(200);
            String CurrentURL = getDriver().getCurrentUrl();
            String env = EnvironmentSpecificConfiguration.from(environmentVariables)
                    .getProperty("environment");
            String NavigatedURL = "https://" + env + ".reference.dex.unsw.edu.au/en";
            flag= CurrentURL.equalsIgnoreCase(NavigatedURL);
            getDriver().navigate().back();
        }else if(link.toLowerCase().contains("external")) {
            flag= isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.externalLink, GENERIC_EXTERNAL_LINK)) ;
        }
        return flag;
    }
    @Step
    public boolean isCTAButtonLabelStylePublishedSite(String labelStyle) {

        boolean flag=false;
        waitABit(500);
        switch (labelStyle) {
            case "Married":
                flag= isElementDisplayed.test(ctaBbuttonPage.labelStyleMarried);
                break;

            case "Divorced":
                LOGGER.info("No changes at UI level as discussed with Devs, no class is applied at a UI level ");
                flag= true;
                break;

            case "Married with icon on left":
                flag= ctaBbuttonPage.labelStyleLeft.isPresent();
                break;

            case "Text Only":
                flag= isElementDisplayed.test(ctaBbuttonPage.labelStyleText);
                break;
        }
        return flag;
    }
    @Step
    public boolean isCTAButtonColorPublishedSite(String buttonColor) {
        return isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.ctaButtonAttribute,buttonColor));
    }
    @Step
    public boolean isCTAButtonStyle(String buttonStyle) {
        return isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.ctaButtonAttribute,buttonStyle));
    }
    @Step
    public boolean isCTAButtonSize(String buttonSize) {
        return isElementDisplayed.test(getDynamicElement(ctaBbuttonPage.ctaButtonAttribute,buttonSize));
    }

}
