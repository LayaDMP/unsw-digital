package testautomation.ui.automation.actions.componentactions;

import org.openqa.selenium.By;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.TestimonialPage;

public class TestimonialActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    TestimonialPage testimonialPage;

    public void addQuotecopy(String quotecopy) {
        testimonialPage.QuoteCopy_Testimonial.waitUntilClickable().type(quotecopy);
    }
    public void addName(String name) {
        testimonialPage.Name_Testimonial.waitUntilClickable().type(name);
    }
    public void addRole(String role) {
        testimonialPage.Role_Testimonial.waitUntilClickable().type(role);
    }




    public void clickOnDoneButton() {
        waitforPageTodisplayWith(referenceSitePage.DoneButton);
        referenceSitePage.DoneButton.click();
    }

    public void clickOnTestimonialAddedOnPage() {
        waitforPageTodisplayWith(testimonialPage.testimonialAdded);
        testimonialPage.testimonialAdded.click();
    }

    public String publishedQuote(){
        return testimonialPage.publishedQuote.waitUntilVisible().getText();
    }
    public String publishedAuthorName(){
        return testimonialPage.publishedAuthorName.waitUntilVisible().getText();
    }
    public String publishedAuthorRole(){
        return testimonialPage.publishedAuthorRole.waitUntilVisible().getText();
    }

    public String publishedImage(){
        return testimonialPage.publishedImage.getAttribute("src");
    }

}
