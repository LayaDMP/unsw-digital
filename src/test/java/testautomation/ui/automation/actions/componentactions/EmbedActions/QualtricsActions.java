package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.QualtricsPage;

public class QualtricsActions extends BaseInteractions {
    QualtricsPage qualtricsPage;
    public void editQualtricsOption(String qForm, int qHt) {
        qualtricsPage.qualtricsURL.type(qForm);
        qualtricsPage.qulatricsHeight.type(Integer.toString(qHt));

    }

    public void selectQualtricsDropDown() {
        qualtricsPage.dropdownEmbeddable.waitUntilClickable().click();
        qualtricsPage.dropDownQualtrics.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return qualtricsPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedQualtricsFrame() {
        waitForPageToLoadWith(qualtricsPage.publishedQualtricsFrame);
        getDriver().switchTo().frame(qualtricsPage.publishedQualtricsFrame);
        return qualtricsPage.publishedQualtrics.waitUntilVisible().isVisible();

    }
}
