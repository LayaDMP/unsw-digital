package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ExperienceFragmentPage;

import java.time.Duration;

public class ExperienceFragmentActions extends BaseInteractions {

    ExperienceFragmentPage experienceFragmentPage;
    ReferenceSiteActions referenceSiteActions;

    public void navigateToTestAutomationFolderInExperienceFragment() {
        experienceFragmentPage.experienceFragments.waitUntilClickable().click();
        experienceFragmentPage.unswfolderExfragment.waitUntilClickable().click();
        experienceFragmentPage.testAutomationFolderExFragment.waitUntilClickable().click();

    }

    public void selectTemplateType(String templateType) {


                waitforPageTodisplayWith(experienceFragmentPage.UNSWCommonExperienceFragmentTemplate);
                experienceFragmentPage.UNSWCommonExperienceFragmentTemplate.click();
                waitforPageTodisplayWith( experienceFragmentPage.nextButtonUnderCreatePageExFragment);
                experienceFragmentPage.nextButtonUnderCreatePageExFragment.click();


        }


    public void enterTemplateTitleExFragment(String templateTitle) {
        waitforPageTodisplayWith(experienceFragmentPage.titleExperienceFragment);
        experienceFragmentPage.titleExperienceFragment.waitUntilClickable().click();
        experienceFragmentPage.titleExperienceFragment.type(templateTitle);

    }

    public void enterTemplateNameExFragment(String templateName) {
       waitforPageTodisplayWith(experienceFragmentPage.nameExperienceFragment);
        experienceFragmentPage.nameExperienceFragment.click();
        experienceFragmentPage.nameExperienceFragment.type(templateName);
    }

    public void enterTemplateDescriptionExFragment(String templateDescription) {
        waitforPageTodisplayWith(experienceFragmentPage.descriptionExperienceFragment);
        experienceFragmentPage.descriptionExperienceFragment.click();
        experienceFragmentPage.descriptionExperienceFragment.type(templateDescription);
    }

    public void clickOnCreateButtonExFragment() {

        experienceFragmentPage.createButtonExFragment.waitUntilClickable().click();
    }

    public void clickOnDoneButtonExFragment() {

        experienceFragmentPage.doneButtonCreateButtonExFragment.waitUntilClickable().click();

    }

    public void selectExperienceFragmentFromDropdown() {
        experienceFragmentPage.experienceFragmentDropdown.waitUntilClickable().click();
    }

    public void clickOnCreateButtonToCreateExFragment() {
        experienceFragmentPage.createButtonUnderCreatePageExFrag.waitUntilClickable().click();
    }

    public void clickOnTemplateTitleFolderUnderTestAutomation(String templateTitle) {
      getDynamicElement(experienceFragmentPage.folder,templateTitle);
      waitforPageTodisplayWith(experienceFragmentPage.folder);
      experienceFragmentPage.folder.click();

    }

    public void clickOnExperienceFragment(String templateName) {

        getDynamicElement(experienceFragmentPage.page,templateName);
        waitforPageTodisplayWith(experienceFragmentPage.page);
        experienceFragmentPage.page.click();
    }

    public void addUnswCommonGlobalHeaderComponent() {
        experienceFragmentPage.enterKeywordTextBox.type("global header");
       waitforPageTodisplayWith(experienceFragmentPage.unswcommonglobalheadercomponentDropdown);
        experienceFragmentPage.unswcommonglobalheadercomponentDropdown.click();
    }

    public void clickOnSideToggleBar() {
        waitforPageTodisplayWith(experienceFragmentPage.toggleSidePanel);
        experienceFragmentPage.toggleSidePanel.click();
    }

    public void clickOnAssetsFolderSideToggleBar() {
        waitforPageTodisplayWith(experienceFragmentPage.assetsOptionSidePanel);
        experienceFragmentPage.assetsOptionSidePanel.click();

    }

    public void clickOnGlobalHeaderComponentAdded() {
        waitforPageTodisplayWith(experienceFragmentPage.unswcommonglobalheadercomponentadded);
        experienceFragmentPage.unswcommonglobalheadercomponentadded.click();
    }

    public void clickOnContentTree() {
        waitforPageTodisplayWith(experienceFragmentPage.contentTreeOptionSidePanel);
        experienceFragmentPage.contentTreeOptionSidePanel.click();

    }

    public void clickOnUNSWCommonGlobalHeaderUnderContentTreeSideToggleBar() {
        waitforPageTodisplayWith(experienceFragmentPage.UNSWCommonGlobalHeaderUnderContentTreeOptionSidePanel);
        experienceFragmentPage.UNSWCommonGlobalHeaderUnderContentTreeOptionSidePanel.click();
    }

    public void clickOnConfigureButton() {
        waitforPageTodisplayWith(experienceFragmentPage.configureButtonExFragment);
        experienceFragmentPage.configureButtonExFragment.click();
    }

    public void enterInLogoLinkCommonGlobalHeaderComponent(String logoLink) {
        experienceFragmentPage.openSelectionDialogLogoLinkFieldGlobalHeaderComponent.waitUntilClickable().click();

        waitforPageTodisplayWith(experienceFragmentPage.dexReferenceSitePopup);
        experienceFragmentPage.dexReferenceSitePopup.click();

        waitforPageTodisplayWith(experienceFragmentPage.referenceSitePopup);
       waitforPageTodisplayWith(experienceFragmentPage.selectButtonPopup);
        experienceFragmentPage.selectButtonPopup.click();
    }

    public void enterInSearchPageLinkCommonGlobalHeaderComponent(String searchPageLink) {
        waitforPageTodisplayWith(experienceFragmentPage.searchPageLinkLinkFieldGlobalHeaderComponent);
        experienceFragmentPage.searchPageLinkLinkFieldGlobalHeaderComponent.click();
        experienceFragmentPage.searchPageLinkLinkFieldGlobalHeaderComponent.type(searchPageLink);

        click_on_sectionsTab_CommonGlobalHeader_component();
        experienceFragmentPage.logoTabGlobalHeaderComponent.waitUntilClickable().click();


    }

    public void enter_in_SearchLabel_CommonGlobalHeader_component(String searchLabel) {
        waitforPageTodisplayWith(experienceFragmentPage.searchLabelFieldGlobalHeaderComponent);
        experienceFragmentPage.searchLabelFieldGlobalHeaderComponent.click();
        experienceFragmentPage.searchLabelFieldGlobalHeaderComponent.type(searchLabel);
    }

    public void click_on_sectionsTab_CommonGlobalHeader_component() {

        experienceFragmentPage.sectionsTabGlobalHeaderComponent.click();
    }

    public void add_GlobalHeaderSections_CommonGlobalHeaderComponent() {
        waitforPageTodisplayWith(experienceFragmentPage.addButtonUnderSectionsTabGlobalHeaderComponent);
        experienceFragmentPage.addButtonUnderSectionsTabGlobalHeaderComponent.click();
        waitforPageTodisplayWith(experienceFragmentPage.globalHeaderSectionDropdownSectionsTabGlobalHeaderComponent);
        experienceFragmentPage.globalHeaderSectionDropdownSectionsTabGlobalHeaderComponent.click();
       waitABit(300);
    }

    public void enter_in_sectionTitleField(String sectionTitile1) {
        experienceFragmentPage.sectionsTitleGlobalHeaderSection.waitUntilClickable().setImplicitTimeout(Duration.ofSeconds(5));
        experienceFragmentPage.sectionsTitleGlobalHeaderSection.type(sectionTitile1);
    }

    public void click_on_GlobalHeaderSection(int sectionNumber) {
       getDynamicElement(experienceFragmentPage.globalHeaderSection, String.valueOf(sectionNumber));
        waitforPageTodisplayWith(experienceFragmentPage.globalHeaderSection);
        experienceFragmentPage.globalHeaderSection.click();
    }

    public void add_subSections_GlobalHeader() {
        experienceFragmentPage.addSubSectionSettings.waitUntilClickable().click();
        experienceFragmentPage.globalHeaderSubSection.waitUntilClickable().click();
        waitFor(1).seconds();
    }

    public void enter_in_sectionTitleLink(String sectionTitleLink) {
        experienceFragmentPage.sectionsTitleLinkGlobalHeaderSection.waitUntilClickable().click();
        experienceFragmentPage.sectionsTitleLinkGlobalHeaderSection.type(sectionTitleLink);
        waitFor(1).seconds();
        experienceFragmentPage.sectionsTitleGlobalHeaderSection.waitUntilClickable().click();
    }

    public void click_on_GlobalHeaderSubSection(int subsectionNumber) {

        getDynamicElement(experienceFragmentPage.globalHeaderSubSec,
                String.valueOf(subsectionNumber));
        waitforPageTodisplayWith(experienceFragmentPage.globalHeaderSubSec);
        experienceFragmentPage.globalHeaderSubSec.click();
    }

    public void enter_in_subsectionTitleField(String subSection_sectionTitile1) {

        experienceFragmentPage.subSectionsTitleGlobalHeaderSection.waitUntilClickable().setImplicitTimeout(Duration.ofSeconds(5));
        experienceFragmentPage.subSectionsTitleGlobalHeaderSection.type(subSection_sectionTitile1);
    }

    public void enter_in_subsectionTitleLink(String subSection_sectionTitileLink1) {
        experienceFragmentPage.subSectionLinkGlobalHeader.waitUntilClickable().setImplicitTimeout(Duration.ofSeconds(5));
        experienceFragmentPage.subSectionLinkGlobalHeader.type(subSection_sectionTitileLink1);
    }
    public void configureseigthSubSection(String subSection_sectionTitile8, String subSection_sectionTitileLink8) {
        configureSubsection(8,subSection_sectionTitile8,subSection_sectionTitileLink8);
    }
    public void configuresninthSubSection(String subSection_sectionTitile9, String subSection_sectionTitileLink9) {
        configureSubsection(9,subSection_sectionTitile9,subSection_sectionTitileLink9);
    }
    public void configureFirstSubSection(String subSection_sectionTitile1, String subSection_sectionTitileLink1) {
        configureSubsection(1,subSection_sectionTitile1,subSection_sectionTitileLink1);
    }

    public void configuresecondSubSection(String subSection_sectionTitile2, String subSection_sectionTitileLink2) {
        configureSubsection(2,subSection_sectionTitile2,subSection_sectionTitileLink2);
    }

    public void configuresthirdSubSection(String subSection_sectionTitile3, String subSection_sectionTitileLink3) {
        configureSubsection(3,subSection_sectionTitile3,subSection_sectionTitileLink3);
    }

    public void configuresfourthSubSection(String subSection_sectionTitile4, String subSection_sectionTitileLink4) {
        configureSubsection(4,subSection_sectionTitile4,subSection_sectionTitileLink4);
    }

    public void configuresfifthSubSection(String subSection_sectionTitile5, String subSection_sectionTitileLink5) {
        configureSubsection(5,subSection_sectionTitile5,subSection_sectionTitileLink5);
    }

    public void configuressixthSubSection(String subSection_sectionTitile6, String subSection_sectionTitileLink6) {
        configureSubsection(6,subSection_sectionTitile6,subSection_sectionTitileLink6);
    }

    public void configuresseventhSubSection(String subSection_sectionTitile7, String subSection_sectionTitileLink7) {
        configureSubsection(7,subSection_sectionTitile7,subSection_sectionTitileLink7);
    }
public void configureSubsection(int tile,String subSection_sectionTitile, String subSection_sectionTitileLink) {
    click_on_GlobalHeaderSubSection(tile);
    clickOnConfigureButton();
    enter_in_subsectionTitleField(subSection_sectionTitile);
    enter_in_subsectionTitleLink(subSection_sectionTitileLink);
}

    public void add_link_to_ExperienceFragmentPage(String experienceFragmentName, String experienceFragmentTitle) {
        referenceSiteActions.clickOnOpenSelectionDialogExperienceFragmentUNSWGlobalHeader();
        referenceSiteActions.navigateTillTestAutomationFolder();
        clickOnTemplateTitleFolderUnderTestAutomation(experienceFragmentTitle);
        clickOnExperienceFragment(experienceFragmentName);
        referenceSiteActions.clickOnSelectButtonForPopupwindow();

    }

    public void enter_in_SearchPageLink_CommonGlobalHeaderComponent(String searchPageLink) {
        enterInSearchPageLinkCommonGlobalHeaderComponent(searchPageLink);
    }

    public void enter_in_SearchLabel(String searchLabel) {
        enter_in_SearchLabel_CommonGlobalHeader_component(searchLabel);
    }

    public void click_on_SectionsTab_CommonGlobalHeaderComponent() {
        click_on_sectionsTab_CommonGlobalHeader_component();
    }


    public void add_number_of_GlobalHeaderSections_CommonGlobalHeaderComponent(int numberOfSections) {
        for (int i = 0; i < numberOfSections; i++) {
            add_GlobalHeaderSections_CommonGlobalHeaderComponent();
        }
    }

    public void click_on_contentTree_SideToggleBar() {
        clickOnContentTree();
    }

    public void configure_firs_GlobalHeaderSection(String sectionTitile1, int numberOfSubSections, String sectionTitleLink) {
        click_on_GlobalHeaderSection(1);
        clickOnConfigureButton();
        enter_in_sectionTitleField(sectionTitile1);
        enter_in_sectionTitleLink(sectionTitleLink);
        for (int i = 0; i < numberOfSubSections; i++) {
            add_subSections_GlobalHeader();
        }

    }

    public void configure_second_GlobalHeaderSection(String sectionTitile2, int numberOfSubSections, String sectionTitleLink) {
        click_on_GlobalHeaderSection(2);
        clickOnConfigureButton();
        enter_in_sectionTitleField(sectionTitile2);
        enter_in_sectionTitleLink(sectionTitleLink);
        for (int i = 0; i < numberOfSubSections; i++) {
            add_subSections_GlobalHeader();
        }
    }

    public void configure_third_GlobalHeaderSection(String sectionTitile3, int numberOfSubSections, String sectionTitleLink) {
        click_on_GlobalHeaderSection(3);
        clickOnConfigureButton();
        enter_in_sectionTitleField(sectionTitile3);
        enter_in_sectionTitleLink(sectionTitleLink);
        for (int i = 0; i < numberOfSubSections; i++) {
            add_subSections_GlobalHeader();
        }
    }

}
