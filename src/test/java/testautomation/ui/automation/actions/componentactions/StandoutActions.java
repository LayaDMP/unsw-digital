package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import org.openqa.selenium.WebElement;
import testautomation.ui.automation.pages.components.StandoutPage;

public class StandoutActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;
    StandoutPage standoutPage;

    public void editStandoutComponent(String standoutText){
        standoutPage.standoutText.waitUntilClickable().click();
        standoutPage.standoutText.type(standoutText);
    }

    public boolean publishedStandoutComponent(){return standoutPage.publishedStandout.isVisible();}

    public boolean publishedStandoutText(){return standoutPage.publishedStandoutText.isVisible();}
}

