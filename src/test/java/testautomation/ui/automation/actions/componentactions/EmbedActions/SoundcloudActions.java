package testautomation.ui.automation.actions.componentactions.EmbedActions;

import framework.BaseInteractions;
import testautomation.ui.automation.pages.components.Embed.SoundcloudPage;

public class SoundcloudActions extends BaseInteractions {
    SoundcloudPage soundcloudPage;

    public void editSoundCloudOption(String sURL,String bTitle,String bLink,String sLink,String sType) {

        soundcloudPage.soundCloudURL.type(sURL);
        soundcloudPage.soundBandLinkTitle.type(bTitle);
        soundcloudPage.soundBandLink.type(bLink);
        soundcloudPage.soundSongLink.type(sLink);
        soundcloudPage.soundSongLinkTitle.type(sType);

    }

    public void selectSoundCloudDropDown() {
        soundcloudPage.dropdownEmbeddable.waitUntilClickable().click();
        soundcloudPage.dropDownSoundCloud.waitUntilClickable().click();

    }

    public boolean publishedEmbedGrid() {
        waitForJSPageLoadComplete();
        return soundcloudPage.publishedEmbedGrid.isVisible();
    }

    public boolean publishedSoundCloudFrame() {
        waitForPageToLoadWith(soundcloudPage.publishedSoundCloudFrame);
        getDriver().switchTo().frame(soundcloudPage.publishedSoundCloudFrame);
        return soundcloudPage.publishedSoundCloud.waitUntilVisible().isVisible();

    }
}
