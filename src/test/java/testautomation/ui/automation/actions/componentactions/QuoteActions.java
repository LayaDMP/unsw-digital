package testautomation.ui.automation.actions.componentactions;

import framework.BaseInteractions;
import org.openqa.selenium.WebElement;
import testautomation.ui.automation.actions.commonactions.ReferenceSiteActions;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.components.QuotePage;

public class QuoteActions extends BaseInteractions {
    ReferenceSitePage referenceSitePage;
    ReferenceSiteActions referenceSiteActions;
    QuotePage quotePage;

    public void editQuoteComponent(String quoteText, String quoteAuthorName, String quotePositionTitle) {
        waitForJSPageLoadComplete();
        waitFor(quotePage.quoteTextingSpace).isVisible();
        addQuoteText(quoteText);
        addAuthorName(quoteAuthorName);
        addPositionTitle(quotePositionTitle);
        addQuoteImage();
    }

    public void addQuoteText(String quoteText) {
        quotePage.quoteTextingSpace.type(quoteText);
    }

    public void addAuthorName(String quoteAuthorName) {
        quotePage.quoteAuthorName.type(quoteAuthorName);
    }

    public void addPositionTitle(String quotePositionTitle) {
        quotePage.quotePositionTitle.type(quotePositionTitle);
    }

    public void addQuoteImage() {
        referenceSiteActions.dragAndDropImageToComponent();
    }

    public boolean publishedQuoteText() {
        return quotePage.publishedQuoteText.isVisible();
    }

    public boolean publishedQuoteAuthorName() {
        return quotePage.publishedQuoteAuthorName.isVisible();
    }

    public boolean publishedQuotePositionTitle() {
        return quotePage.publishedQuotePositionTitle.isVisible();
    }

    public boolean publishedQuoteIcon(){
        return quotePage.publishPageQuoteIcon.isVisible();
    }
    public boolean publishedQuoteImage(){
        return quotePage.publishedPageQuoteImage.isVisible();
    }

}
