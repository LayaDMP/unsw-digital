package testautomation.ui.automation.actions.commonactions;

import framework.BaseInteractions;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.WebDriver;

import testautomation.ui.automation.pages.commonpages.NavigationPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class NavigationActions extends BaseInteractions {

    NavigationPage navigationPage;

    private EnvironmentVariables environmentVariables;




    @Step
    public void navigateToPublishedSite() {
        waitForJSPageLoadComplete();
        String reference_site = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("webdriver.referencesite.url");
        WebDriver driver = getDriver();
        String currentUrl = driver.getCurrentUrl();
        String url = getUrlSuffix(currentUrl);
        String new_url = String.format("%s%s", reference_site, url);
        driver.get(new_url);
        waitABit(1000);
        waitForJSPageLoadComplete();
    }

@Step
    public void clickOnDEXReferenceSiteFolder() {
        waitforPageTodisplayWith(navigationPage.dexreferencesitefolder);
        navigationPage.dexreferencesitefolder.click();
    }

@Step
    public void clickOnReferenceSiteFolder() {
        navigationPage.referenceSiteSearchBoxXpath.waitUntilClickable().click();
    }

@Step
    public void clickOnTestAutomationFolder() {
        waitForJSPageLoadComplete();
        waitforPageTodisplayWith(navigationPage.testAutomationfolder);
        navigationPage.testAutomationfolder.waitUntilClickable().click();
    }

    @Step
    public void clickOnCreateButton() {
        waitForJSPageLoadComplete();
        clickWithJS(navigationPage.createButton);
    }

    @Step
    public void clickOnPageOption() {
        waitForJSPageLoadComplete();
        clickWithJS(navigationPage.pageOptionUnderCreateButton);
        /*waitforPageTodisplayWith(navigationPage.pageOptionUnderCreateButton);
        clickOn(navigationPage.pageOptionUnderCreateButton);*/
    }

    @Step
    public void deleteAllPagesUnderTestAutomationFolder(){

        waitABit(800);
        if (navigationPage.firstPage_underTestAutomationfolder.isCurrentlyVisible()) {
            navigationPage.firstPage_underTestAutomationfolder.waitUntilClickable().click();
            waitABit(300);
            if (navigationPage.selectAllButtonUnderNavigationPage.isCurrentlyVisible()) {
                navigationPage.selectAllButtonUnderNavigationPage.waitUntilClickable().click();
                waitABit(600);
            }
            try {
                navigationPage.moreOption.waitUntilClickable().click();
            }catch(Exception e){
                e.printStackTrace();
            }
            navigationPage.deleteButton.waitUntilClickable().click();
            navigationPage.deleteButtonConfirm.waitUntilClickable().click();
            waitABit(600);
            if (navigationPage.forceDelete.isCurrentlyVisible()) {
                navigationPage.forceDelete.waitUntilClickable().click();
            }
        }
        else{
            LOGGER.info("No Pages found under TestAutomation Folder");
        }

    }
    @Step
    public void deletePagesUnderTestAutomationFolder(){

        waitForJSPageLoadComplete();
        if (isOptionalElementDisplayed(navigationPage.firstPage_underTestAutomationfolder)) {
            waitABit(500);
            clickWithJS(navigationPage.firstPage_underTestAutomationfolder);
            if (isOptionalElementDisplayed(navigationPage.selectAllButtonUnderNavigationPage)) {
                navigationPage.selectAllButtonUnderNavigationPage.waitUntilClickable().click();
            }
            try {
                waitABit(500);
                clickWithJS(navigationPage.moreOption);
            }catch(Exception e){
                e.printStackTrace();
            }
            waitForJSPageLoadComplete();
            clickWithJS(navigationPage.deleteButton);
            navigationPage.deleteButtonConfirm.waitUntilClickable().click();
            waitABit(600);
            if (isOptionalElementDisplayed(navigationPage.forceDelete)) {
                navigationPage.forceDelete.waitUntilClickable().click();
            }
        }
        else{
            LOGGER.info("No Pages found under TestAutomation Folder");
        }
        waitForJSPageLoadComplete();
    }

    @Step
    public void editThePage() {
        waitABit(800);
        clickWithJS(navigationPage.EditButton);
    }

    public void switchToNewTab() {
        WebDriver driver = getDriver();
        String mainTab = driver.getWindowHandle();
        Set<String> tabsSet = driver.getWindowHandles();
        List tabsList = new ArrayList();
        tabsList.addAll(tabsSet);
        waitABit(300);
        driver.switchTo().window((String) tabsList.get(1));
    }


    public void switchToFirstTab() {
        ArrayList<String> windowHandles = new ArrayList(getDriver().getWindowHandles());
        getDriver().switchTo().window(windowHandles.get(1)).close();
        getDriver().switchTo().window(windowHandles.get(0));
        getDriver().navigate().refresh();
    }

    public static String getUrlSuffix(String currentUrl) {
        Scanner scanner = new Scanner(currentUrl);
        String suffix = scanner.findInLine("test-automation(.*)");
        String formEdit = suffix.substring(0, suffix.length() - 5);
        scanner.close();
        return formEdit;
    }

    public void refreshPageURL() {
        getDriver().navigate().refresh();
        waitFor(3).seconds();
    }
}
