package testautomation.ui.automation.actions.commonactions;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import framework.BaseInteractions;
import testautomation.ui.automation.pages.commonpages.Loginpage;

public class LoginActions extends BaseInteractions {
    private EnvironmentVariables environmentVariables;

    public static String navigateURL ="";
    Loginpage loginpage;

    public void tracyLogsInToTheAEMSite() {
        if(getDriver()!=null){
            getDriver().quit();
        }

        loginToAEM();
    }

    @Step
    public void loginToAEM() {

        navigateURL = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("webdriver.base.url");

        String Enter_Username = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("webdriver.admin.user");

        String Enter_Password = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("webdriver.admin.password");

        openUrl(navigateURL+"/sites.html/content/unsw-dex-reference/en/test-automation");
        waitforPageTodisplayWith(loginpage.SignInWithAdobeButton);
        loginpage.SignInWithAdobeButton.click();
        loginpage.Username.waitUntilClickable().click();
        loginpage.Username.type(Enter_Username);
        loginpage.Password.waitUntilClickable().click();
        loginpage.Password.type(Enter_Password);
        loginpage.SignInButton.click();
    }
}
