package testautomation.ui.automation.actions.commonactions;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import framework.BaseInteractions;
import testautomation.aem.ColumnTypes;
import testautomation.ui.automation.pages.commonpages.ReferenceSitePage;
import testautomation.ui.automation.pages.commonpages.TemplatesPage;

import java.time.Duration;
import java.util.*;
import java.util.function.Supplier;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static testautomation.ui.automation.pages.commonpages.ReferenceSitePage.IMAGENAMETOUSE;

public class ReferenceSiteActions extends BaseInteractions {

    NavigationActions sitesPage;

    ReferenceSitePage referenceSitePage;

    TemplatesPage templatesPage;



    private EnvironmentVariables environmentVariables;
    private final Map<ColumnTypes, Supplier<Boolean>> layoutMap=new HashMap<>();

    public void clickOnTogglePanel() {
        referenceSitePage.ToggleSidePanel.setImplicitTimeout(Duration.ofSeconds(10));
        referenceSitePage.ToggleSidePanel.waitUntilClickable().click();
        referenceSitePage.searchTextBoxUnderToggleSidePanel.waitUntilClickable().setImplicitTimeout(Duration.ofSeconds(5));
    }


    public void searchForImage(String imageName) {

        referenceSitePage.searchTextBoxUnderToggleSidePanel.typeAndEnter(imageName);
        waitforPageTodisplayWith(referenceSitePage.imageinsearchtextboxundertogglesidepanel);
        waitABit(300);
    }

    public void dragAndDropImageToComponent() {
        dragAndDrop(referenceSitePage.imageinsearchtextboxundertogglesidepanel,
                referenceSitePage.dragToLocationForImage);
    }

    public void clickOnDoneButton() {
        waitABit(500);
        referenceSitePage.DoneButton.click();
        waitABit(200);
    }

    public void clickonpageinformation() {
        waitForJSPageLoadComplete();
        clickWithJS(referenceSitePage.PageInformation);
    }

    public void clickOnPublishPage() {
        optionalDefaultWaitFor(referenceSitePage.PublishPage);
        clickWithJS(referenceSitePage.PublishPage);
        waitForJSPageLoadComplete();
        if (isOptionalElementDisplayed(referenceSitePage.publishButtonForUnpublishedItems)) {
            referenceSitePage.publishButtonForUnpublishedItems.waitUntilClickable().click();
            waitABit(600);
        } else {
            LOGGER.info("All items are published");
        }
        waitForJSPageLoadComplete();
    }

    public void clickOnPlusButton() {
        referenceSitePage.PlusButton.waitUntilClickable().click();
        waitABit(300);
        referenceSitePage.EnterKeywordTextBox.waitUntilClickable().click();
    }

    public void clickOnConfigureButton() {
        waitForJSPageLoadComplete();
        waitABit(800);
        clickWithJS(referenceSitePage.ConfigureButton);

    }

    public void clickOnComponent(String componentName){
        waitforPageTodisplayWith(getDynamicElement(referenceSitePage.addedComponent,componentName));
        clickWithJS(getDynamicElement(referenceSitePage.addedComponent,componentName));
    }

    public void selectFixedListDropdownOption() {
        waitforPageTodisplayWith(referenceSitePage.pageDropDownButton);
        referenceSitePage.pageDropDownButton.waitUntilClickable().click();
        waitforPageTodisplayWith(referenceSitePage.FixedPagesDropwdown);
        referenceSitePage.FixedPagesDropwdown.waitUntilClickable().click();
    }


    public void clickOnTestAutomationFolder() {

        referenceSitePage.TestAutomation_popup.waitUntilClickable().click();
    }

    public void clickOnTemplatePage(String templateName) {
        waitForPageToLoadWith(getDynamicElement(templatesPage.imagePageXpath,templateName));
        getDynamicElement(templatesPage.imagePageXpath,templateName).click();
    }

    public void clickOnSelectButtonForPopupwindow()
    {
        referenceSitePage.SelectButton_popup.waitUntilClickable().click();
    }



    public void clickOnOpenSelectionDialogExperienceFragmentUNSWGlobalHeader() {
        referenceSitePage.OpenSelectionDialog_ExperienceFragment_StandardPage.waitUntilClickable().click();

    }

    public void navigateTillTestAutomationFolder() {
        referenceSitePage.UNSWFolderExFragment.waitUntilClickable().click();
        referenceSitePage.testAutomationFolderExFragment.waitUntilClickable().click();
    }

    public void enterKeyword(String keyword) {
        referenceSitePage.EnterKeywordTextBox.waitUntilClickable().type(keyword);
        waitABit(200);

    }

    @Step
    public void addComponentFromDropdown() {
       referenceSitePage.selectDropdownComponent.waitUntilClickable().click();
       waitABit(300);
    }

    @Step
    public void clickOnAddedComponent(String title) {
        String titleCaps=StringUtils.capitalize(title);
        waitABit(1000);
        getDynamicElement(referenceSitePage.clickOnAddedComponent,titleCaps)
                .waitUntilClickable().click();
    }

    @Step
    public void clickOnDexReferenceLink() {
        referenceSitePage.DEXReferenceSiteFolder.waitUntilClickable().click();
        referenceSitePage.referenceSitePopupButtonComponent.waitUntilClickable().click();
    }

    public void verifyNavigatedURLFor(String readMoreURL) {

    }

    @Step
    public void clickOnTheSuppliedColumnLayout(String template) {
        String type=template.replaceAll("_","").toUpperCase().replaceAll(" ","");
        setTemplateDetails();
        layoutMap.get(ColumnTypes.valueOf(type)).get();
    }

    public Supplier<Boolean> oneColumnFullwidth =()-> {
        waitforPageTodisplayWith(referenceSitePage.oneColumnFullWidthLayout);
        referenceSitePage.oneColumnFullWidthLayout.click();
        return true;
    };
    public Supplier<Boolean> oneColumnLeft =()->{
        waitforPageTodisplayWith(referenceSitePage.oneColumnLeftLayout);
        referenceSitePage.oneColumnLeftLayout.click();
        return true;
    };
    public Supplier<Boolean> twoColumnFullwidth =()->{
        waitforPageTodisplayWith(referenceSitePage.OneColumn_TwoColumnTemplate);
        referenceSitePage.OneColumn_TwoColumnTemplate.click();
        return true;
    };
    public Supplier<Boolean> twoColumnLeft =()->{
        waitforPageTodisplayWith( referenceSitePage.twoColumnLeftLayout);
        referenceSitePage.twoColumnLeftLayout.click();
        return true;
    };
    public Supplier<Boolean> standardTemplatefullwidth =()->{
        waitforPageTodisplayWith(referenceSitePage.standardTemplateFullwidthLayout);
        referenceSitePage.standardTemplateFullwidthLayout.click();
        return true;
    };
    public Supplier<Boolean> standardTemplateLeft =()->{
        waitforPageTodisplayWith( referenceSitePage.standardTemplateLeftLaytout);
        referenceSitePage.standardTemplateLeftLaytout.click();
        return true;
    };
    public Supplier<Boolean> standardTemplateRight =()->{
        waitforPageTodisplayWith( referenceSitePage.standardTemplateRightLayout);
        referenceSitePage.standardTemplateRightLayout.click();
        return true;
    };

    public Supplier<Boolean> standardTemplateexperienceFragment=()->{
        waitforPageTodisplayWith(referenceSitePage.ExperienceFragmentRoot_ExperienceFragment);
        referenceSitePage.ExperienceFragmentRoot_ExperienceFragment.click();
        return true;
    };
    public Supplier<Boolean> experienceFragmentRoot=()->{
        waitforPageTodisplayWith(referenceSitePage.RootColumn_ExperienceFragment);
        referenceSitePage.RootColumn_ExperienceFragment.click();
        return true;
    };

    public Supplier<Boolean> degreefullwidth=()->{
        waitforPageTodisplayWith(referenceSitePage.degreeFullWidthLayout);
        referenceSitePage.degreeFullWidthLayout.click();
        return true;
    };


    public void setTemplateDetails(){
        layoutMap.put(ColumnTypes.ONECOLUMNFULLWIDTH, oneColumnFullwidth);
        layoutMap.put(ColumnTypes.ONECOLUMNLEFT, oneColumnLeft);
        layoutMap.put(ColumnTypes.TWOCOLUMNFULLWIDTH, twoColumnFullwidth);
        layoutMap.put(ColumnTypes.TWOCOLUMNLEFT, twoColumnLeft);
        layoutMap.put(ColumnTypes.STANDARDFULLWIDTHCOLUMN, standardTemplatefullwidth);
        layoutMap.put(ColumnTypes.STANDARDLEFTCOLUMN, standardTemplateLeft);
        layoutMap.put(ColumnTypes.STANDARDRIGHTCOLUMN, standardTemplateRight);
        layoutMap.put(ColumnTypes.STANDARDEXPERIENCEFRAGMENT,standardTemplateexperienceFragment);
        layoutMap.put(ColumnTypes.EXPERIENCEFRAGMENTROOT,experienceFragmentRoot);
        layoutMap.put(ColumnTypes.DEGREEFULLWIDTH,degreefullwidth);
    }

    @Step
    public void gotoManualSettingsTab(){
        referenceSitePage.componentTypeDropdown.click();
        referenceSitePage.componentOptionManual.click();
        referenceSitePage.manualSettingTab.click();
    }

    public void gotoContentFragmentSettingsTab(){
        referenceSitePage.componentTypeDropdown.click();
        referenceSitePage.componentOptionContentFragment.click();
        referenceSitePage.contentFragmentTab.click();
    }

    public void gotoGeneralSettingsTab(){
        referenceSitePage.generalTabSettings.click();
    }

    public void goToConfigOfComponent(String component){
        clickOnTogglePanel();
        searchForImage(IMAGENAMETOUSE);
        clickOnComponent(component);
        clickOnConfigureButton();
    }

}
