package testautomation.ui.automation.actions.commonactions;

import framework.BaseInteractions;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebElement;
import testautomation.ui.automation.pages.commonpages.TemplatesPage;

import java.util.Map;


public class TemplatesActions extends BaseInteractions {

    TemplatesPage templatesPage;

    // create the Page as per the values passed from the Feature file

    @Step
    public void createThePage(Map<String,String> pageDetails) {
        chooseTemplateType(pageDetails.get("templateType"));
        templatesPage.TitleTextBox.type(pageDetails.get("templateTitle"));
        templatesPage.NameTextBox.type(pageDetails.get("templateName"));
        templatesPage.PageTitleTextBox.type(pageDetails.get("templatePage"));
        templatesPage.NavigationTitleTextBox.type(pageDetails.get("templateNavigation"));
        templatesPage.CreateButton_underCreatePage.click();
        waitforPageTodisplayWith(templatesPage.DoneButton_CreateButton);
        templatesPage.DoneButton_CreateButton.click();
    }

    @Step
    public void chooseTemplateType(String templateType){
        String Type_of_Template = templateType;
        switch (Type_of_Template) {
            case "oneColumn":
                waitforPageTodisplayWith( templatesPage.OneColumnStarterTemplate);
                templatesPage.OneColumnStarterTemplate.click();
                templatesPage.NextButton_underCreatePage.click();
                break;
            case "twoColumn":
                waitforPageTodisplayWith(templatesPage.TwoColumnStarterTemplate);
                templatesPage.TwoColumnStarterTemplate.click();
                templatesPage.NextButton_underCreatePage.click();
                break;

            case "standardPage":
                waitforPageTodisplayWith( templatesPage.StandardPageTemplate);
                templatesPage.StandardPageTemplate.click();
                templatesPage.NextButton_underCreatePage.waitUntilClickable().click();

                break;

            case "degreePage":
                waitforPageTodisplayWith(templatesPage.DegreePageTemplate);
                templatesPage.DegreePageTemplate.click();
                templatesPage.NextButton_underCreatePage.waitUntilClickable().click();

            default:
                break;
        }

    }

    @Step
    public void clickOnTemplateImage(String templateName) {
        WebElementFacade imagePath=getDynamicElement(templatesPage.imagePageXpath,templateName);
        waitForJSPageLoadComplete();
        waitABit(1000);
        imagePath.waitUntilClickable().click();
    }

}
